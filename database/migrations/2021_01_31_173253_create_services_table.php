<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->id();
            $table->string('service_name',255)->nullable();
            $table->string('service_category',255)->nullable();
            $table->longText('details',255)->nullable(); 
            $table->decimal('prize', 10, 2)->default(0);
            $table->longText('description',255)->nullable(); 
            $table->string('service_image',255)->nullable();
            $table->string('thumpnail',255)->nullable();
            $table->string('date',255)->nullable();  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
