<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemporaryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temporary', function (Blueprint $table) {
            $table->id();
            $table->string('service',255)->nullable();
            $table->string('type',255)->nullable();
            $table->string('duration',255)->nullable();
            $table->integer('pin')->nullable(); 
            $table->string('city',255)->nullable();
            $table->string('district',255)->nullable();    
            $table->string('state',255)->nullable(); 
            $table->string('firstname',255)->nullable(); 
            $table->string('lastname',255)->nullable();
            $table->string('phone',255)->nullable();    
            $table->string('email',255)->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temporary');
    }
}
