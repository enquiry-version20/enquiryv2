<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEnquirysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enquirys', function (Blueprint $table) {
            $table->id();
            $table->string('name',255)->nullable();
            $table->integer('meterial_id')->nullable();
            $table->integer('service_id')->nullable();
            $table->longText('address',255)->nullable(); 
            $table->string('phone',255)->nullable();
            $table->string('email',255)->nullable();    
            $table->string('date',255)->nullable();              
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enquirys');
    }
}
