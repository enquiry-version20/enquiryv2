<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMeterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meterials', function (Blueprint $table) {
            $table->id();
            $table->string('material_name',255)->nullable();
            $table->string('material_category',255)->nullable();
            $table->string('material_type',255)->nullable();
            $table->longText('details',255)->nullable(); 
            $table->decimal('prize', 10, 2)->default(0);
            $table->longText('description',255)->nullable(); 
            $table->string('meterial_image',255)->nullable();
            $table->string('thumpnail',255)->nullable();
            $table->integer('stock')->nullable();
            $table->string('date',255)->nullable();              
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meterials');
    }
}
