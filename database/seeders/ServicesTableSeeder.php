<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Seeder;

class ServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            'parent_id' => '0',
            'child_id' => '0',
            'service_name' => 'General',
            'service_category' => '',
            'details' => 'Garage Door Repair. Carpentry. Termite Damage 
            <br/>Repairs. Tub and shower caulking. Doors. <br/>Fences. Weatherproofing&#8230;',
            'prize' => '0',
            'description'=>'Garage Door Repair. Carpentry. Termite Damage 
            <br/>Repairs. Tub and shower caulking. Doors. <br/>Fences. Weatherproofing&#8230;',
            'service_image'=>'/images/icons/general.png',
            'thumpnail'=>'/images/icons/general.png',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
        DB::table('services')->insert([
            'parent_id' => '0',
            'child_id' => '0',
            'service_name' => 'Flooring',
            'service_category' => '',
            'details' => 'Electrical. Cable TV. Internet. Network. Low<br/> Voltage. Door Bells. Home Theater. Sprinkler<br/> Systems. Phone Jacks. Computers.',
            'prize' => '0',
            'description'=>'Electrical. Cable TV. Internet. Network. Low<br/> Voltage. Door Bells. Home Theater. Sprinkler<br/> Systems. Phone Jacks. Computers.',
            'service_image'=>'/images/icons/floor.png',
            'thumpnail'=>'/images/icons/floor.png',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
        DB::table('services')->insert([
            'parent_id' => '0',
            'child_id' => '0',
            'service_name' => 'Painting',
            'service_category' => '',
            'details' => 'Kitchens. Baths. Back Splashes. Floors. <br/> Grout. Grout Repair. Grout sealing.<br/> Tile repair. Showers. Walls',
            'prize' => '0',
            'description'=>'Kitchens. Baths. Back Splashes. Floors. <br/> Grout. Grout Repair. Grout sealing.<br/> Tile repair. Showers. Walls',
            'service_image'=>'/images/icons/paint.jpg',
            'thumpnail'=>'/images/icons/paint.jpg',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
        DB::table('services')->insert([
            'parent_id' => '0',
            'child_id' => '0',
            'service_name' => 'Cleaning',
            'service_category' => '',
            'details' => 'Home Theater Installation. Flat Panel TV<br/> Installation. Projectors. Sound Systems.<br/> Surround Sound.',
            'prize' => '0',
            'description'=>'Home Theater Installation. Flat Panel TV<br/> Installation. Projectors. Sound Systems.<br/> Surround Sound.',
            'service_image'=>'/images/icons/cleaning.webp',
            'thumpnail'=>'/images/icons/cleaning.webp',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
        DB::table('services')->insert([
            'parent_id' => '0',
            'child_id' => '0',
            'service_name' => 'Roofing',
            'service_category' => '',
            'details' => 'Switches. Dimmers. Outlets. Recessed<br/> Lighting. Light Fixture Installation. Ceiling<br/> Fans. Ground Fault Outlet Installation.',
            'prize' => '0',
            'description'=>'Switches. Dimmers. Outlets. Recessed<br/> Lighting. Light Fixture Installation. Ceiling<br/> Fans. Ground Fault Outlet Installation.',
            'service_image'=>'/images/icons/roof.jpg',
            'thumpnail'=>'/images/icons/roof.jpg',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
        DB::table('services')->insert([
            'parent_id' => '0',
            'child_id' => '0',
            'service_name' => 'Plumbing',
            'service_category' => '',
            'details' => 'Leaks Repaired. Faucet Installation. Toilet<br/> Repair/Replacement. Faucet Leaks. Shut Off<br/> Valves. Kitchens. Toilet Repairs&#8230;',
            'prize' => '0',
            'description'=>'Leaks Repaired. Faucet Installation. Toilet<br/> Repair/Replacement. Faucet Leaks. Shut Off<br/> Valves. Kitchens. Toilet Repairs&#8230;',
            'service_image'=>'/images/icons/plump.jpg',
            'thumpnail'=>'/images/icons/plump.jpg',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
    }
}
