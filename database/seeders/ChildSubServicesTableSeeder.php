<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ChildSubServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('services')->insert([
            'parent_id' => '7',
            'child_id' => '0',
            'service_name' => 'immediate',
            'service_category' => 'general',
            'details' => 'HOME',
            'prize' => '0',
            'description'=>'home',
            'service_image'=>'',
            'thumpnail'=>'',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
        DB::table('services')->insert([
            'parent_id' => '7',
            'child_id' => '0',
            'service_name' => 'Within Two Weeks',
            'service_category' => 'general',
            'details' => 'Within Two Weeks',
            'prize' => '0',
            'description'=>'Within Two Weeks',
            'service_image'=>'',
            'thumpnail'=>'',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
       
        DB::table('services')->insert([
            'parent_id' => '7',
            'child_id' => '0',
            'service_name' => 'Within a month',
            'service_category' => 'general',
            'details' => 'Within a month',
            'prize' => '0',
            'description'=>'Within a month',
            'service_image'=>'',
            'thumpnail'=>'',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
        DB::table('services')->insert([
            'parent_id' => '7',
            'child_id' => '0',
            'service_name' => 'I am Flexible',
            'service_category' => 'general',
            'details' => 'I am Flexible',
            'prize' => '0',
            'description'=>'I am Flexible',
            'service_image'=>'',
            'thumpnail'=>'',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
    }
}
