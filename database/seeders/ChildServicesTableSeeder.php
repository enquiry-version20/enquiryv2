<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Seeder;

class ChildServicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('services')->insert([
            'parent_id' => '1',
            'child_id' => '0',
            'service_name' => 'HOME',
            'service_category' => 'general',
            'details' => 'HOME',
            'prize' => '0',
            'description'=>'home',
            'service_image'=>'/images/kaboompics.com_Old-building-350x252.jpg',
            'thumpnail'=>'/images/kaboompics.com_Old-building-350x252.jpg',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
        DB::table('services')->insert([
            'parent_id' => '1',
            'child_id' => '0',
            'service_name' => 'Residence',
            'service_category' => 'general',
            'details' => 'Residence',
            'prize' => '0',
            'description'=>'Residence',
            'service_image'=>'/images/projects-4-350x252.jpg',
            'thumpnail'=>'/images/projects-4-350x252.jpg',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
        DB::table('services')->insert([
            'parent_id' => '1',
            'child_id' => '0',
            'service_name' => 'Office',
            'service_category' => 'general',
            'details' => 'Office',
            'prize' => '0',
            'description'=>'Office',
            'service_image'=>'/images/kaboompics.com_Old-building-350x252.jpg',
            'thumpnail'=>'/images/kaboompics.com_Old-building-350x252.jpg',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);
        DB::table('services')->insert([
            'parent_id' => '1',
            'child_id' => '0',
            'service_name' => 'Others',
            'service_category' => 'general',
            'details' => 'Others',
            'prize' => '0',
            'description'=>'Others',
            'service_image'=>'/images/wallpapering.jpg',
            'thumpnail'=>'/images/wallpapering.jpg',
            'date'=>'02-09-2021',
            'created_at' => '2019-10-24 00:00:00',
            'updated_at' => '2019-10-24 00:00:00',
            
        ]);

    }
}
