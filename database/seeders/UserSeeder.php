<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@homeadvisor.com',
            'password' => bcrypt('secret'),
            'is_admin_user' => '0',            
            'created_at' => '2021-2-9 15:42:00',
            'updated_at' => '2021-2-9 15:42:00',
            
        ]);
    }
}
