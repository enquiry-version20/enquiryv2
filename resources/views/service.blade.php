@extends('layout.frontend.design')

@section('content')



			<div id="topOfPage" class="topTabsWrap">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">All Posts</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<span class="current">Service / Department Page</span>
					</div>
					<h3 class="pageTitle h3">Service / Department Page</h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
				<div class="content">
					<div class="itemscope" itemscope itemtype="http://schema.org/Article">
						<section class="post post_format_standard postAlter no_margin page type-page status-publish hentry">
							<article class="post_content">
								<div class="post_text_area" itemprop="articleBody">

									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<h3 class="sc_title sc_aligncenter sc_title_regular">Service Page</h3>
													<div class="sc_subsection bg_tint_none sc_aligncenter margin_bottom_small">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<p>It&#8217;s our department&#8217;s page template where you can place information about team of specialists, services provided,<br/>
																certificates and work schedule etc.</p>
															</div>
														</div>
													</div>
													<div class="sc_team no_padding">
														<div class="sc_columns columnsWrap">
															<div class="columns1_3" data-animation="animated fadeInUp">
																<div class="sc_team_item sc_team_item_1 odd first">
																	<div class="sc_team_item_avatar">
																		<img alt="03.jpg" src="images/03-370x370.jpg">
																		<div class="sc_team_item_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</div>
																	</div>
																	<div class="sc_team_item_info">
																		<h3 class="sc_team_item_title">
																			<a href="{{ url('/handyman')}}">Earl Stone</a>
																		</h3>
																		<div class="sc_team_item_position theme_accent2">Construction Worker</div>
																		<ul class="sc_team_item_socials">
																			<li>
																				<a href="#" class="social_icons icon-facebook" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-twitter" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-gplus" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-dribbble" target="_blank"> </a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="columns1_3" data-animation="animated fadeInUp">
																<div class="sc_team_item sc_team_item_2 even">
																	<div class="sc_team_item_avatar">
																		<img alt="01.jpg" src="images/01-370x370.jpg">
																		<div class="sc_team_item_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</div>
																	</div>
																	<div class="sc_team_item_info">
																		<h3 class="sc_team_item_title">
																			<a href="#">John Reed</a>
																		</h3>
																		<div class="sc_team_item_position theme_accent2">Interior Designer</div>
																		<ul class="sc_team_item_socials">
																			<li>
																				<a href="#" class="social_icons icon-facebook" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-twitter" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-gplus" target="_blank"> </a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="columns1_3" data-animation="animated fadeInUp">
																<div class="sc_team_item sc_team_item_3 odd">
																	<div class="sc_team_item_avatar">
																		<img alt="04.jpg" src="images/04-370x370.jpg">
																		<div class="sc_team_item_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</div>
																	</div>
																	<div class="sc_team_item_info">
																		<h3 class="sc_team_item_title">
																			<a href="#">Jessica Brown</a>
																		</h3>
																		<div class="sc_team_item_position theme_accent2">Exterior Designer</div>
																		<ul class="sc_team_item_socials">
																			<li>
																				<a href="#" class="social_icons icon-facebook" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-dribbble" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-gplus" target="_blank"> </a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>															
														</div>
														<div class="sc_columns columnsWrap">
															<div class="columns1_3" data-animation="animated fadeInUp">
																<div class="sc_team_item sc_team_item_1 odd first">
																	<div class="sc_team_item_avatar">
																		<img alt="03.jpg" src="images/03-370x370.jpg">
																		<div class="sc_team_item_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</div>
																	</div>
																	<div class="sc_team_item_info">
																		<h3 class="sc_team_item_title">
																			<a href="#">Earl Stone</a>
																		</h3>
																		<div class="sc_team_item_position theme_accent2">Construction Worker</div>
																		<ul class="sc_team_item_socials">
																			<li>
																				<a href="#" class="social_icons icon-facebook" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-twitter" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-gplus" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-dribbble" target="_blank"> </a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="columns1_3" data-animation="animated fadeInUp">
																<div class="sc_team_item sc_team_item_2 even">
																	<div class="sc_team_item_avatar">
																		<img alt="01.jpg" src="images/01-370x370.jpg">
																		<div class="sc_team_item_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</div>
																	</div>
																	<div class="sc_team_item_info">
																		<h3 class="sc_team_item_title">
																			<a href="#">John Reed</a>
																		</h3>
																		<div class="sc_team_item_position theme_accent2">Interior Designer</div>
																		<ul class="sc_team_item_socials">
																			<li>
																				<a href="#" class="social_icons icon-facebook" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-twitter" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-gplus" target="_blank"> </a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>
															<div class="columns1_3" data-animation="animated fadeInUp">
																<div class="sc_team_item sc_team_item_3 odd">
																	<div class="sc_team_item_avatar">
																		<img alt="04.jpg" src="images/04-370x370.jpg">
																		<div class="sc_team_item_description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</div>
																	</div>
																	<div class="sc_team_item_info">
																		<h3 class="sc_team_item_title">
																			<a href="#">Jessica Brown</a>
																		</h3>
																		<div class="sc_team_item_position theme_accent2">Exterior Designer</div>
																		<ul class="sc_team_item_socials">
																			<li>
																				<a href="#" class="social_icons icon-facebook" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-dribbble" target="_blank"> </a>
																			</li>
																			<li>
																				<a href="#" class="social_icons icon-gplus" target="_blank"> </a>
																			</li>
																		</ul>
																	</div>
																</div>
															</div>															
														</div>

													</div>
												</div>
												<div class="sc_line sc_line_style_solid no_margin"></div>
											</div>
										</div>
									</div>

									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<h1 class="sc_title sc_aligncenter sc_title_regular" data-animation="animated fadeInUp">Pricing tables</h1>
													<div class="sc_subsection bg_tint_none sc_aligncenter margin_bottom_small" data-animation="animated fadeInUp">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<p>Here&#8217;s what you get with M2 membership:</p>
															</div>
														</div>
													</div>
													<div class="sc_pricing_table columns_3 alignCenter" data-animation="animated fadeInUp">
														<div class="sc_pricing_columns sc_pricing_column_1">
															<ul>
																<li class="sc_pricing_data sc_pricing_title">
																	<div>Design Consulting</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<!-- <div class="sc_price_currency">Rs</div> -->
																			<div class="sc_price_money">500/</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">day</div>
																				<div class="sc_price_period">
																				</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Step-by-step instructions to your repair works</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Basic house maintenance and repair cunsultations</div>
																</li>
																<li class="sc_pricing_data sc_pricing_footer">
																	<div>
																		<div class="sc_button sc_button_style_dark sc_button_size_medium squareButton dark medium">
																			<a href="#" class="">Start free trial</a>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
														<div class="sc_pricing_columns sc_pricing_column_2">
															<ul>
																<li class="sc_pricing_data sc_pricing_title">
																	<div>Home Maintenance</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<!-- <div class="sc_price_currency">Rs</div> -->
																			<div class="sc_price_money">400/</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">day</div>
																				<div class="sc_price_period">
																				</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Step-by-step instructions to your repair works</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Basic house maintenance and repair cunsultations</div>
																</li>
																<li class="sc_pricing_data sc_pricing_footer">
																	<div>
																		<div class="sc_button sc_button_style_dark sc_button_size_medium squareButton dark medium">
																			<a href="#" class="">Sign Up</a>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
														<div class="sc_pricing_columns sc_pricing_column_3">
															<ul>
																<li class="sc_pricing_data sc_pricing_title">
																	<div>Repair Deal</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<!-- <div class="sc_price_currency">Rs</div> -->
																			<div class="sc_price_money">550/</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">day</div>
																				<div class="sc_price_period">
																				</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Step-by-step instructions to your repair works</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Basic house maintenance and repair cunsultations</div>
																</li>
																<li class="sc_pricing_data sc_pricing_footer">
																	<div>
																		<div class="sc_button sc_button_style_dark sc_button_size_medium squareButton dark medium">
																			<a href="#" class="">Sign Up</a>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_section dark image_bg2 color_bg1">
													<div class="sc_content main">
														<h1 class="sc_title sc_title_regular sc_aligncenter" data-animation="animated fadeInUp">Client Stories</h1>
														<div class="sc_testimonials sc_testimonials_style_2 no_margin sc_testimonials_padding sc_testimonials_controls_top" data-animation="animated fadeInUp">
															<div class="sc_slider sc_slider_swiper sc_slider_controls sc_slider_controls_top sc_slider_nopagination sc_slider_autoheight swiper-slider-container">
																<ul class="sc_testimonials_items slides swiper-wrapper">
																	<li class="sc_testimonials_item swiper-slide" style="">
																		<div class="sc_testimonials_item_content">
																			<div class="sc_testimonials_item_quote">
																				<div class="sc_testimonials_item_text">A friend recommended M2, and I couldn&#8217;t be happier&#8230;it was fast, easy, and most importantly, it allowed me to focus on my business while leaving the construction matters to the professionals.</div>
																			</div>
																			<div class="sc_testimonials_item_author">
																				<div class="sc_testimonials_item_avatar">
																					<img alt="01.jpg" src="images/01-50x50.jpg">
																				</div>
																				<div class="sc_testimonials_item_name">John Doe</div>
																				<div class="sc_testimonials_item_position">SEO, Some Co.</div>
																			</div>
																		</div>
																	</li>
																	<li class="sc_testimonials_item swiper-slide" style="">
																		<div class="sc_testimonials_item_content">
																			<div class="sc_testimonials_item_quote">
																				<div class="sc_testimonials_item_text">Best purchase i made on ThemeForest. Great Theme!</div>
																			</div>
																			<div class="sc_testimonials_item_author">
																				<div class="sc_testimonials_item_avatar">
																					<img alt="02.jpg" src="images/02-50x50.jpg">
																				</div>
																				<div class="sc_testimonials_item_name">Anna Smith</div>
																				<div class="sc_testimonials_item_position">Purchaser</div>
																			</div>
																		</div>
																	</li>
																	<li class="sc_testimonials_item swiper-slide" style="">
																		<div class="sc_testimonials_item_content">
																			<div class="sc_testimonials_item_quote">
																				<div class="sc_testimonials_item_text">Hi! I’ve just check and it works perfectly! Thank you very much for your kindness and for all the work you’ve done to solve all issues. I’ll write about your fantastic support wherever I can. Thanks again.</div>
																			</div>
																			<div class="sc_testimonials_item_author">
																				<div class="sc_testimonials_item_avatar">
																					<img alt="3.jpg" src="images/3-50x50.jpg">
																				</div>
																				<div class="sc_testimonials_item_name">Anna Black</div>
																				<div class="sc_testimonials_item_position">Purchaser</div>
																			</div>
																		</div>
																	</li>
																	<li class="sc_testimonials_item swiper-slide" style="">
																		<div class="sc_testimonials_item_content">
																			<div class="sc_testimonials_item_quote">
																				<div class="sc_testimonials_item_text">Hello All, Firstly, thank you so much for the great template! I have spent around 10hrs looking for a good template and yours was the best!</div>
																			</div>
																			<div class="sc_testimonials_item_author">
																				<div class="sc_testimonials_item_avatar">
																					<img alt="2.jpg" src="images/2-50x50.jpg">
																				</div>
																				<div class="sc_testimonials_item_name">Mike Doe</div>
																				<div class="sc_testimonials_item_position">Purchaser</div>
																			</div>
																		</div>
																	</li>
																	<li class="sc_testimonials_item swiper-slide" style="">
																		<div class="sc_testimonials_item_content">
																			<div class="sc_testimonials_item_quote">
																				<div class="sc_testimonials_item_text">Great theme &amp; fast Support.</div>
																			</div>
																			<div class="sc_testimonials_item_author">
																				<div class="sc_testimonials_item_avatar">
																					<img alt="1.jpg" src="images/1-50x50.jpg">
																				</div>
																				<div class="sc_testimonials_item_name">Nicole Smith</div>
																				<div class="sc_testimonials_item_position">Purchaser</div>
																			</div>
																		</div>
																	</li>
																</ul>
															</div>
															<ul class="flex-direction-nav">
																<li>
																	<a class="flex-prev" href="#">
																	</a>
																</li>
																<li>
																	<a class="flex-next" href="#">
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tagsWrap"></div>
								</div>  
							</article>  
						</section>  
					</div>    
				 
				</div>  
			</div>

@endsection			