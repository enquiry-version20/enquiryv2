

@extends('layout.frontend.design')
@section('content')


			<div id="topOfPage" class="topTabsWrap" style="border:4px solid white;">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">Cleaning</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">Location</a>
					</div>
					<h3 class="pageTitle h3">Cleaning / Location</h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar" style="border:4px solid white;">
				<div class="content">
					<div class="itemscope" itemscope itemtype="http://schema.org/Article">
						<section class="post post_format_standard postAlter no_margin page type-page status-publish hentry">
							<article class="post_content">
								<div class="post_text_area" itemprop="articleBody">

									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_section grey_section">
													<div class="sc_content main" data-animation="animated fadeInUp">
														<h3 class="sc_undertitle sc_title_regular" style="padding: 0">Where is your project located?</h3><hr>
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<p><br/>
															 </p>
															</div>
														</div>
														<div class="sc_contact_form sc_contact_form_contact_3 margin_top_small">
															<form data-formtype="contact_3" method="post" action="include/sendmail.php">
																<div class="columnsWrap sc_columns sc_columns_count_4 no_padding">
																	<div class="columns1_4 sc_column_item sc_column_item_1 odd first">
																		<input id="sc_contact_form_username" type="text" name="pin" placeholder="Pin Code">
																		<input id="sc_contact_form_phone" type="text" name="city" placeholder="City">
																	</div>
																	<div class="columns1_4 sc_column_item sc_column_item_2 even">
																		<input id="sc_contact_form_location" type="text" name="district" placeholder="District">
																		<input id="sc_contact_form_subj" type="text" name="state" placeholder="State">
																	</div>



																		<div class="sc_contact_form_button" style="float:none;">
																			<div class="sc_button sc_button_style_dark sc_button_size_huge squareButton dark huge">
																				<a href="{{ url('/clean/contact')}}" class="sc_contact_form_submit">Submit</a>
																			</div>
																		</div>

																</div>
																<div class="result sc_infobox"></div>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="tagsWrap"></div>
								</div>  
							</article>  
						</section>  
					</div>    
				 
				</div>  
			</div>

@endsection			