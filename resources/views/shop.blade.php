
@extends('layout.frontend.design')

@section('content')

			<div id="mainslider_4" class="sliderHomeBullets staticSlider slider_engine_revo slider_alias_rev-hr4">
				<div id="rev_slider_4_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
					
					<div id="rev_slider_4_1" class="rev_slider fullwidthabanner">
						<ul>
							<li data-transition="random" data-slotamount="7" data-masterspeed="300" data-thumb="images/slider/shop_sl2_bg-320x200.jpg" data-saveperformance="off" data-title="Slide">
								<img src="images/slider/shop_sl2_bg.jpg" alt="shop_sl2_bg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
								<div class="tp-caption slider4-sl1-price lfb tp-resizeme" data-x="right" data-hoffset="-135" data-y="36" data-speed="2500" data-start="1000" data-easing="Bounce.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">
									<div>$58.<sup>99</sup></div>
								</div>
								<div class="tp-caption slider4-sl1-cat lfl tp-resizeme" data-x="160" data-y="96" data-speed="1500" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">Tools & Materials
								</div>
								<div class="tp-caption slider4-sl1-header lfl tp-resizeme" data-x="160" data-y="132" data-speed="1500" data-start="750" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">Basic Tools for the Newbie
								</div>
								<div class="tp-caption slider4-sl1-text lfl tp-resizeme" data-x="160" data-y="200" data-speed="1500" data-start="1000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">The ten tools you need to fix all the <br/>
									broken stuff in your home.
								</div>
								<div class="tp-caption no_style lfl tp-resizeme  slider4-sl1-btn" data-x="160" data-y="280" data-speed="1500" data-start="1250" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">
									<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
										<a href="#" class="">purchase now</a>
									</div>
								</div>
							</li>
							
							<li data-transition="random" data-slotamount="7" data-masterspeed="300" data-thumb="images/slider/shop_sl1_bg-320x200.jpg" data-saveperformance="off" data-title="Slide">
								
								<img src="images/slider/shop_sl1_bg.jpg" alt="shop_sl1_bg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
								
								
								<div class="tp-caption slider4-sl1-price randomrotate tp-resizeme" data-x="right" data-hoffset="-135" data-y="36" data-speed="2500" data-start="1000" data-easing="Bounce.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">
									<div>$37.<sup>99</sup>
									</div>
								</div>
								
								<div class="tp-caption slider4-sl1-cat lfb tp-resizeme" data-x="160" data-y="96" data-speed="1500" data-start="500" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">Interior Paint
								</div>
								
								<div class="tp-caption slider4-sl1-header lfb tp-resizeme" data-x="160" data-y="132" data-speed="1500" data-start="750" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">Delux Matt Emulsion<br/>
									Paint Blue
								</div>
								
								<div class="tp-caption slider4-sl1-text lfb tp-resizeme" data-x="160" data-y="240" data-speed="1500" data-start="1000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">Paint of quality enhanced with cutting <br/>
									edge technology.
								</div>
								
								<div class="tp-caption no_style sfb tp-resizeme  slider4-sl1-btn" data-x="160" data-y="320" data-speed="1500" data-start="1250" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">
									<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
										<a href="#" class="">purchase now</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
				<div class="main" role="main">
					<div class="content">
						<div class="itemscope" itemscope itemtype="http://schema.org/Article">
							<section class="post postAlter no_margin page">
								<article class="post_content">
									<div class="post_text_area" itemprop="articleBody">
										<div class="vc_row wpb_row vc_row-fluid">
											<div class="wpb_column vc_column_container vc_col-sm-12">
												<div class="wpb_wrapper">
													<div class="sc_section bg_tint_dark darkgrey_section woo_section">
														<div class="sc_content main">
															<h2 class="sc_title sc_title_regular sc_aligncenter">Categories</h2>
															<div class="woocommerce columns-5">
																<ul class="products">
																	<li class="product-category product first">
																		<a href="features_shop_category.html">
																			<img src="images/2.jpg" alt="Basins &amp; Sinks"/>
																			<h3>
																			Basins &amp; Sinks <mark class="count">(2)</mark>
																			</h3>
																		</a>
																	</li>
																	<li class="product-category product">
																		<a href="features_shop_category.html">
																			<img src="images/1.jpg" alt="Building Mixtures"/>
																			<h3>
																			Building Mixtures <mark class="count">(3)</mark>
																			</h3>
																		</a>
																	</li>
																	<li class="product-category product">
																		<a href="features_shop_category.html">
																			<img src="images/3.jpg" alt="Faucets"/>
																			<h3>
																			Faucets <mark class="count">(3)</mark>
																			</h3>
																		</a>
																	</li>
																	<li class="product-category product">
																		<a href="features_shop_category.html">
																			<img src="images/7.jpg" alt="Lighting"/>
																			<h3>
																			Lighting <mark class="count">(3)</mark>
																			</h3>
																		</a>
																	</li>
																	<li class="product-category product last">
																		<a href="features_shop_category.html">
																			<img src="images/8.jpg" alt="Other"/>
																			<h3>
																			Other <mark class="count">(3)</mark>
																			</h3>
																		</a>
																	</li>
																	<li class="product-category product first">
																		<a href="features_shop_category.html">
																			<img src="images/9.jpg" alt="Electrical"/>
																			<h3>
																			Electrical <mark class="count">(3)</mark>
																			</h3>
																		</a>
																	</li>
																	<li class="product-category product">
																		<a href="features_shop_category.html">
																			<img src="images/10.jpg" alt="Home Security"/>
																			<h3>
																			Home Security <mark class="count">(3)</mark>
																			</h3>
																		</a>
																	</li>
																	<li class="product-category product">
																		<a href="features_shop_category.html">
																			<img src="images/4.jpg" alt="Painting"/>
																			<h3>
																			Painting <mark class="count">(3)</mark>
																			</h3>
																		</a>
																	</li>
																	<li class="product-category product">
																		<a href="features_shop_category.html">
																			<img src="images/5.jpg" alt="Tools"/>
																			<h3>
																			Tools <mark class="count">(3)</mark>
																			</h3>
																		</a>
																	</li>
																	<li class="product-category product last">
																		<a href="features_shop_category.html">
																			<img src="images/6.jpg" alt="Windows"/>
																			<h3>
																			Windows <mark class="count">(2)</mark>
																			</h3>
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="vc_row wpb_row vc_row-fluid">
											<div class="wpb_column vc_column_container vc_col-sm-12">
												<div class="wpb_wrapper">
													<div class="sc_content main no_padding_bottom">
														<h2 class="sc_title sc_title_regular sc_aligncenter margin_bottom_small">Recent products</h2>
														<div class="woocommerce columns-4">
															<ul class="products">
																<li class="first product">
																	<a href="features_shop_product.html">
																		<img src="images/drill-300x300.jpg" class="attachment-shop_catalog" alt="drill"/>
																		<h3>Electric Drill</h3>
																		<span class="price">
																			<span class="amount">&pound;250.00</span>
																		</span>
																	</a>
																	<a href="#" rel="nofollow" class="button add_to_cart_button product_type_simple">Add to cart</a>
																</li>
																<li class="product">
																	<a href="features_shop_product.html">
																		<img src="images/Australian-Cement-1-300x300.jpg" class="attachment-shop_catalog" alt="Print"/>
																		<h3>Australian Cement</h3>
																		<span class="price">
																			<span class="amount">&pound;19.45</span>
																		</span>
																	</a>
																	<a href="#" rel="nofollow" class="button add_to_cart_button product_type_simple">Add to cart</a>
																</li>
																<li class="product">
																	<a href="features_shop_product.html">
																		<img src="images/High-Tech-Faucet-1-300x300.jpg" class="attachment-shop_catalog" alt="High Tech Faucet-1"/>
																		<h3>High Tech Faucet</h3>
																		<div class="star-rating" title="Rated 5.00 out of 5">
																			<span style="width:100%">
																			<strong class="rating">5.00</strong> out of 5</span>
																		</div>
																		<span class="price">
																			<span class="amount">&pound;112.80</span>
																		</span>
																	</a>
																	<a href="#" rel="nofollow" class="button add_to_cart_button product_type_simple">Add to cart</a>
																</li>
																<li class="last product">
																	<a href="features_shop_product.html">
																		<span class="onsale">Sale!</span>
																		<img src="images/Tracking-Light-1-300x300.jpg" class="attachment-shop_catalog" alt="Tracking Light -1"/>
																		<h3>Tracking Light</h3>
																		<div class="star-rating" title="Rated 5.00 out of 5">
																			<span style="width:100%">
																			<strong class="rating">5.00</strong> out of 5</span>
																		</div>
																		<span class="price">
																			<del>
																			<span class="amount">&pound;144.90</span>
																			</del>
																			<ins>
																			<span class="amount">&pound;102.50</span>
																			</ins>
																		</span>
																	</a>
																	<a href="#" rel="nofollow" class="button add_to_cart_button product_type_simple">Add to cart</a>
																</li>
															</ul>
														</div>
													</div>
													<div class="sc_line sc_line_style_solid sc_line_style_solid_2 margin_top_big margin_bottom_large sc_aligncenter">
													</div>
												</div>
											</div>
										</div>
										<div class="vc_row wpb_row vc_row-fluid">
											<div class="wpb_column vc_column_container vc_col-sm-12">
												<div class="wpb_wrapper">
													<h2 class="sc_title sc_title_regular sc_aligncenter margin_bottom_small">Top sellers</h2>
													<div class="woocommerce columns-4">
														<ul class="products">
															<li class="first product">
																<a href="features_shop_product.html">
																	<img src="images/Australian-Cement-1-300x300.jpg" class="attachment-shop_catalog" alt="Print"/>
																	<h3>Australian Cement</h3>
																	<span class="price">
																		<span class="amount">&pound;19.45</span>
																	</span>
																</a>
																<a href="#" rel="nofollow" class="button add_to_cart_button product_type_simple">Add to cart</a>
															</li>
															<li class="product">
																<a href="features_shop_product.html">
																	<img src="images/drill-300x300.jpg" class="attachment-shop_catalog" alt="drill"/>
																	<h3>Electric Drill</h3>
																	<span class="price">
																		<span class="amount">&pound;250.00</span>
																	</span>
																</a>
																<a href="#" rel="nofollow" class="button add_to_cart_button product_type_simple">Add to cart</a>
															</li>
															<li class="product">
																<a href="features_shop_product.html">
																	<img src="images/Spotlights-1-300x300.jpg" class="attachment-shop_catalog" alt="Spotlights -1"/>
																	<h3>Spotlights</h3>
																	<span class="price">
																		<span class="amount">&pound;149.50</span>
																	</span>
																</a>
																<a href="#" rel="nofollow" class="button add_to_cart_button product_type_simple">Add to cart</a>
															</li>
															<li class="last product">
																<a href="features_shop_product.html">
																	<img src="images/Led-Light-2-300x300.jpg" class="attachment-shop_catalog" alt="Led Light - 2"/>
																	<h3>Led Light</h3>
																	<span class="price">
																		<span class="amount">&pound;55.50</span>
																	</span>
																</a>
																<a href="#" rel="nofollow" class="button add_to_cart_button product_type_simple">Add to cart</a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
										<div class="vc_row wpb_row vc_row-fluid">
											<div class="wpb_column vc_column_container vc_col-sm-12">
												<div class="wpb_wrapper">
													<div class="sc_content main partner_section">
														<div class="sc_section scrollbar_hide bg_tint_none sc_scroll_controls sc_scroll_controls_horizontal sc_scroll_controls_type_side" data-animation="animated fadeInUp">
															<div class="sc_scroll sc_scroll_horizontal swiper-slider-container scroll-container">
															<div class="sc_scroll_wrapper swiper-wrapper">
																<div class="sc_scroll_slide swiper-slide">
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-1-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-2-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-3-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-4-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-5-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-1-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-2-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-3-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-4-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-5-150x143.jpg" alt=""/>
																	</figure>
																</div>
															</div>
																<div class="sc_scroll_bar sc_scroll_bar_horizontal"></div>
															</div>
															<ul class="flex-direction-nav">
																<li>
																	<a class="flex-prev" href="#"> </a>
																</li>
																<li>
																	<a class="flex-next" href="#"> </a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tagsWrap"></div>
									</div>
								</article>
							</section>
						</div>
					</div>
				</div>
			</div>
@endsection
