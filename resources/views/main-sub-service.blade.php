@extends('layout.frontend.design')

@section('content')

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

			<div id="topOfPage" class="topTabsWrap" style="width: 100%;">
				<div class="main" style="width: 92%;">
					<div class="speedBar">
						<a class="home" href="{{ url('/')}}"></a>
						<span class="breadcrumbs_delimiter">  </span>
						<a class="all" href="{{ url('/')}}">{{$main_service_parent_name}}</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="{{ url('/main-services/'.$service_parent_id)}}">{{$service_name}}</a>
					</div>
					<h3 class="pageTitle h3"><a class="all" href="{{ url('/')}}">{{$main_service_parent_name}} </a>/ <a class="all" href="{{ url('/main-services/'.$service_parent_id)}}">{{$service_name}}</a></h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12" style="padding:50px;">
											<div class="wpb_wrapper" 
											style=" border: 2px solid lightgrey;border-radius: 8px;background: #F1F1F1;">
												<div class="sc_content">
													<div class="sc_section sc_aligncenter" data-animation="animated fadeInUp">
														<h3 style="margin-top:50px;padding:1px;">When do you need to start your project?</h3>
														<div>
														<!-- <form action="{{ url('/contact-form/'.$parent_id)}}" method="post" class="form-section" >
														{{ csrf_field() }} -->
														<div style="padding-left: 30px;padding-right: 30px;">
														<div class=" sc_blogger sc_blogger_horizontal style_portfolio3 portfolioWrap">

															<section class="duration portfolio isotope folio3col searchable-container" data-columns="4">
																@if(count($main_sub_services)>0)
																	@foreach($main_sub_services as $key=>$main_sub_service)
																<article class="items isotopeElement hover_Shift odd flt_252" style="    width: 265px;">
												                    <div class="info-block block-info clearfix">
												                        <div data-toggle="buttons" class="btn-group bizmoduleselect" style="width: 100%;    text-align: -webkit-center;">
												                        
												                            <label class="btn btn-default" style="width: 80%;">
												                                <div class="bizcontent">
												                                    <input type="radio" name="service_sub_cat_name" class="service_sub_cat_name" autocomplete="off" value="{{$main_sub_service->id}}" {{ ($main_sub_service->id==$service_id)? "checked" : "" }}  >
												                                    <span class="glyphicon glyphicon-ok glyphicon-lg"></span>
												                                    <h5>{{$main_sub_service->service_name}}</h5>
												                                </div>
												                            </label>
												                    
												                        </div>
												                    </div>
																</article>
																@endforeach
																@endif
																

															</section>
													</div>
													</div>
														<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium" style="    margin-bottom:63px;">
															<a href="{{ url('/main-services/'.$service_parent_id)}}" class="">Previous</a>
														</div>
														<input type="hidden" name="parent_id" id="parent_id" value="{{$parent_id}}"/>
														<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium" style="display: none;">
														<button type="submit">Next</button>	
														</div>
													<!-- </form>	 -->
												
												</div>
												</div>
											</div>
										</div>
									</div>															
			</div>

<style type="text/css">
.searchable-container{margin:20px 0 0 0}
.searchable-container label.btn-default:hover{background-color:#F1F1F1;color:#FFF;border:3px solid #FF9900;}
.searchable-container label.btn-default{width:90%;border:2px solid #a4a3b0; box-shadow:5px 8px 8px 0 #ccc;border-radius: 5px;cursor: pointer;}
.searchable-container label .bizcontent{width:100%; margin-top:17px;}
.searchable-container .btn-group{width:90%}
.searchable-container .btn span.glyphicon{
    opacity: 0;
}
.searchable-container .btn.active span.glyphicon {
    opacity: 1;
}
.btn-default{
	height: 126px;
	width: 100%;
}	
.searchable-container h5{
font-size: 20px;
}
.searchable-container label{
line-height: normal;
}
.searchable-container label .bizcontent{
	margin-top: 34px;
}
.searchable-container .items{
	margin: 0;
}
.duration .item input[type=radio]:checked + label{
	background:#003872;
	border: 1px solid #003872;
}
.duration .item input[type=radio]:active{
	background:#003872;
	border: 1px solid #003872;
}
</style>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript">

$('.service_sub_cat_name').click(function() {

var radio_val=$("input[name='service_sub_cat_name']:checked").val();
 
  var url = '{{ url("/contact-form") }}/'+radio_val;
  window.location.href =url;

})
</script>


@endsection			