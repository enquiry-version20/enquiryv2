@extends('layout.frontend.design')

@section('content')

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

			<div id="topOfPage" class="topTabsWrap">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">Cleaning</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">Home</a>
					</div>
					<h3 class="pageTitle h3">Cleaning / Home</h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<div class="sc_section sc_aligncenter" data-animation="animated fadeInUp">
														<h3>When do you need to start your project?</h3>
														<form>
														<div class="sc_blogger sc_blogger_horizontal style_portfolio3 portfolioWrap">

															<section class="portfolio isotope folio3col searchable-container" data-columns="4">
																<article class="items isotopeElement hover_Shift odd flt_252">
												                    <div class="info-block block-info clearfix">
												                        <div class="square-box pull-left">
												                            <span class="glyphicon glyphicon-tags glyphicon-lg"></span>
												                        </div>
												                        <div data-toggle="buttons" class="btn-group bizmoduleselect">
												                            <label class="btn btn-default">
												                                <div class="bizcontent">
												                                    <input type="checkbox" name="var_id[]" autocomplete="off" value="">
												                                    <span class="glyphicon glyphicon-ok glyphicon-lg"></span>
												                                    <h5>immediate</h5>
												                                </div>
												                            </label>
												                        </div>
												                    </div>
																</article>
																<article class="items isotopeElement hover_Shift even flt_253">
												                    <div class="info-block block-info clearfix">
												                        <div class="square-box pull-left">
												                            <span class="glyphicon glyphicon-tags glyphicon-lg"></span>
												                        </div>
												                        <div data-toggle="buttons" class="btn-group bizmoduleselect">
												                            <label class="btn btn-default">
												                                <div class="bizcontent">
												                                    <input type="checkbox" name="var_id[]" autocomplete="off" value="">
												                                    <span class="glyphicon glyphicon-ok glyphicon-lg"></span>
												                                    <h5>Within Two Weeks</h5>
												                                </div>
												                            </label>
												                        </div>
												                    </div>
																</article>
																<article class="items isotopeElement hover_Shift odd flt_253 flt_252">
                    												<div class="info-block block-info clearfix">
												                        <div class="square-box pull-left">
												                            <span class="glyphicon glyphicon-tags glyphicon-lg"></span>
												                        </div>
												                        <div data-toggle="buttons" class="btn-group bizmoduleselect">
												                            <label class="btn btn-default">
												                                <div class="bizcontent">
												                                    <input type="checkbox" name="var_id[]" autocomplete="off" value="">
												                                    <span class="glyphicon glyphicon-ok glyphicon-lg"></span>
												                                    <h5>Within a month</h5>
												                                </div>
												                            </label>
												                        </div>
												                    </div>
																</article>
																<article class="items isotopeElement hover_Shift even flt_252">
                    												<div class="info-block block-info clearfix">
												                        <div class="square-box pull-left">
												                            <span class="glyphicon glyphicon-tags glyphicon-lg"></span>
												                        </div>
												                        <div data-toggle="buttons" class="btn-group bizmoduleselect">
												                            <label class="btn btn-default">
												                                <div class="bizcontent">
												                                    <input type="checkbox" name="var_id[]" autocomplete="off" value="">
												                                    <span class="glyphicon glyphicon-ok glyphicon-lg"></span>
												                                    <h5>I'm Flexible</h5>
												                                </div>
												                            </label>
												                        </div>
												                    </div>
																</article>

															</section>
													</div>
														<!-- <div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
															<a href="{{ url('/clean/location')}}" class="">Next</a>
														</div> -->
													</form>	
												</div>
											</div>
										</div>
									</div>															
			</div>

<style type="text/css">
.searchable-container{margin:20px 0 0 0}
.searchable-container label.btn-default:hover{background-color:#F1F1F1;color:#FFF;border:3px solid #FF9900;}
.searchable-container label.btn-default{width:90%;border:2px solid #a4a3b0; box-shadow:5px 8px 8px 0 #ccc;border-radius: 5px;cursor: pointer;}
.searchable-container label .bizcontent{width:100%; margin-top:17px;}
.searchable-container .btn-group{width:90%}
.searchable-container .btn span.glyphicon{
    opacity: 0;
}
.searchable-container .btn.active span.glyphicon {
    opacity: 1;
}
.btn-default{
	height: 126px;
	width: 100%;
}	
.searchable-container h5{
font-size: 20px;
}
.searchable-container label{
line-height: normal;
}
.searchable-container label .bizcontent{
	margin-top: 34px;
}
.searchable-container .items{
	margin: 0;
}
</style>




@endsection			