
@extends('layout.frontend.design')
@section('content')


			<div id="topOfPage" class="topTabsWrap">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">All Posts</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<span class="current">Protected: Protected Page</span>
					</div>
					<h3 class="pageTitle h3">Protected: Protected Page</h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
				<div class="content">
					<div class="itemscope" itemscope itemtype="http://schema.org/Article">
						<section class="post postAlter no_margin page post-password-required">
							<article class="post_content">
								<div class="post_text_area" itemprop="articleBody">
									<p>There is no excerpt because this is a protected post.</p>
									<form action="#" class="post-password-form" method="post">
										<p>This content is password protected. To view it please enter your password below:</p>
										<p>
											<label for="pwbox-3034">Password: 
											<input name="post_password" id="pwbox-3034" type="password" size="20"/></label>
											<input type="submit" name="Submit" value="Submit"/>
										</p>
									</form>
								</div>
							</article>
						</section>  
					</div>    
				 
				</div>  
			</div>


@endsection		