
@extends('layout.frontend.design')
@section('content')

			<div id="topOfPage" class="topTabsWrap">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">All Posts</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<span class="current">1 column</span>
					</div>
					<h3 class="pageTitle h3">1 column</h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
				<div class="main" role="main">
					<div class="content">
						<div class="itemscope" itemscope itemtype="http://schema.org/Article">
							<section class="itemPage post postAlter no_margin page">
								<article class="post_content">
									<div class="post_text_area" itemprop="articleBody">
										<div class="sc_blogger sc_blogger_horizontal style_classic1 masonryWrap">
											<div class="isotopeFiltr">
												<ul>
													<li class="squareButton active">
														<a href="#" data-filter="*">All</a>
													</li>
													<li class="squareButton">
														<a href="#" data-filter=".flt_252">living rooms</a>
													</li>
													<li class="squareButton">
														<a href="#" data-filter=".flt_251">buildings</a>
													</li>
													<li class="squareButton">
														<a href="#" data-filter=".flt_250">exterior design</a>
													</li>
													<li class="squareButton">
														<a href="#" data-filter=".flt_253">kitchens</a>
													</li>
												</ul>
											</div>
											<section class="masonry isotope" data-columns="1">
												<article class="isotopeElement post_format_standard odd flt_252">
													<div class="isotopePadding">
														<div class="thumb" data-image="images/projects-1.jpg" data-title="Living Room Staircases">
															<img alt="Living Room Staircases" src="images/projects-1-1150x647.jpg">
															<div class="link-wrapper">
																<a href="#">View</a>
															</div>
														</div>
														<h4>
															<a href="#">Living Room Staircases</a>
														</h4>
														<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci... </p>
														<div class="masonryInfo">
															Posted 
															<a href="#" class="post_date">February 25, 2015</a>
														</div>
														<div class="masonryMore">
															<ul>
																<li class="squareButton dark">
																	<a title="More" href="#">More</a>
																</li>
																<li class="squareButton light ico">
																	<a class="icon-eye" title="Views - 615" href="#">615</a>
																</li>
																<li class="squareButton light ico">
																	<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
																</li>
																<li class="squareButton light ico likeButton like" data-postid="3639" data-likes="4" data-title-like="Like" data-title-dislike="Dislike">
																	<a class="icon-heart-1" title="Like - 4" href="#">
																		<span class="likePost">4</span>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</article>

												<article class="isotopeElement post_format_standard even flt_252">
													<div class="isotopePadding">
														<div class="thumb" data-image="images/projects-5.jpg" data-title="Interior Design Ideas">
															<img alt="Interior Design Ideas" src="images/projects-5-1150x647.jpg">
															<div class="link-wrapper">
																<a href="#">View</a>
															</div>
														</div>
														<h4>
														<a href="#">Interior Design Ideas</a>
														</h4>
														<p>
														Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci... </p>
														<div class="masonryInfo">
															Posted
															<a href="#" class="post_date">February 25, 2015</a>
														</div>
														<div class="masonryMore">
															<ul>
																<li class="squareButton dark">
																	<a title="More" href="#">More</a>
																</li>
																<li class="squareButton light ico">
																	<a class="icon-eye" title="Views - 324" href="#">324</a>
																</li>
																<li class="squareButton light ico">
																	<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
																</li>
																<li class="squareButton light ico likeButton like" data-postid="3636" data-likes="5" data-title-like="Like" data-title-dislike="Dislike">
																	<a class="icon-heart-1" title="Like - 5" href="#">
																		<span class="likePost">5</span>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</article>

												<article class="isotopeElement post_format_standard odd flt_251 flt_250">
													<div class="isotopePadding">
														<div class="thumb" data-image="images/kaboompics.com_Words-on-the-wall.jpg" data-title="Bedroom Design Ideas">
															<img alt="Bedroom Design Ideas" src="images/kaboompics.com_Words-on-the-wall-1150x647.jpg">
															<div class="link-wrapper">
																<a href="#">View</a>
															</div>
														</div>
														<h4>
														<a href="#">Bedroom Design Ideas</a>
														</h4>
														<p>
														Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci... </p>
														<div class="masonryInfo">
															Posted
															<a href="#" class="post_date">February 25, 2015</a>
														</div>
														<div class="masonryMore">
															<ul>
																<li class="squareButton dark">
																	<a title="More" href="#">More</a>
																</li>
																<li class="squareButton light ico">
																	<a class="icon-eye" title="Views - 346" href="#">346</a>
																</li>
																<li class="squareButton light ico">
																	<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
																</li>
																<li class="squareButton light ico likeButton like" data-postid="3627" data-likes="1" data-title-like="Like" data-title-dislike="Dislike">
																	<a class="icon-heart-1" title="Like - 1" href="#">
																		<span class="likePost">1</span>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</article>

												<article class="isotopeElement post_format_standard even flt_253">
													<div class="isotopePadding">
														<div class="thumb" data-image="images/projects-2.jpg" data-title="Kitchen Design Ideas">
															<img alt="Kitchen Design Ideas" src="images/projects-2-1150x647.jpg">
															<div class="link-wrapper">
																<a href="#">View</a>
															</div>
														</div>
														<h4>
														<a href="#">Kitchen Design Ideas</a>
														</h4>
														<p>
														Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci... </p>
														<div class="masonryInfo">
															Posted
															<a href="#" class="post_date">February 25, 2015</a>
														</div>
														<div class="masonryMore">
															<ul>
																<li class="squareButton dark">
																	<a title="More" href="#">More</a>
																</li>
																<li class="squareButton light ico">
																	<a class="icon-eye" title="Views - 576" href="#">576</a>
																</li>
																<li class="squareButton light ico">
																	<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
																</li>
																<li class="squareButton light ico likeButton like" data-postid="3638" data-likes="5" data-title-like="Like" data-title-dislike="Dislike">
																	<a class="icon-heart-1" title="Like - 5" href="#">
																		<span class="likePost">5</span>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</article>

												<article class="isotopeElement post_format_standard odd flt_253 flt_252">
													<div class="isotopePadding">
														<div class="thumb" data-image="images/projects-4.jpg" data-title="10 Design Lessons">
															<img alt="10 Design Lessons" src="images/projects-4-1150x647.jpg">
															<div class="link-wrapper">
																<a href="#">View</a>
															</div>
														</div>
														<h4>
														<a href="#">10 Design Lessons</a>
														</h4>
														<p>
														Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci... </p>
														<div class="masonryInfo">
															Posted
															<a href="#" class="post_date">February 25, 2015</a>
														</div>
														<div class="masonryMore">
															<ul>
																<li class="squareButton dark">
																	<a title="More" href="#">More</a>
																</li>
																<li class="squareButton light ico">
																	<a class="icon-eye" title="Views - 425" href="#">425</a>
																</li>
																<li class="squareButton light ico">
																	<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
																</li>
																<li class="squareButton light ico likeButton like" data-postid="3637" data-likes="1" data-title-like="Like" data-title-dislike="Dislike">
																	<a class="icon-heart-1" title="Like - 1" href="#">
																		<span class="likePost">1</span>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</article>

												<article class="isotopeElement post_format_standard even flt_252">
													<div class="isotopePadding">
														<div class="thumb" data-image="images/kaboompics.com_Old-building.jpg" data-title="Exterior Design Ideas">
															<img alt="Exterior Design Ideas" src="images/kaboompics.com_Old-building-1150x647.jpg">
															<div class="link-wrapper">
																<a href="#">View</a>
															</div>
														</div>
														<h4>
														<a href="#">Exterior Design Ideas</a>
														</h4>
														<p>
														Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci... </p>
														<div class="masonryInfo">
															Posted
															<a href="#" class="post_date">February 25, 2015</a>
														</div>
														<div class="masonryMore">
															<ul>
																<li class="squareButton dark">
																	<a title="More" href="#">More</a>
																</li>
																<li class="squareButton light ico">
																	<a class="icon-eye" title="Views - 228" href="#">228</a>
																</li>
																<li class="squareButton light ico">
																	<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
																</li>
																<li class="squareButton light ico likeButton like" data-postid="3635" data-likes="0" data-title-like="Like" data-title-dislike="Dislike">
																	<a class="icon-heart-1" title="Like - 0" href="#">
																		<span class="likePost">0</span>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</article>
											</section>
										</div>
									</div>
								</article>
							</section>
						</div>
					</div>  
				</div>
			</div>

@endsection			