@extends('layout.frontend.design')

@section('content')

				<div id="mainslider_2" class="sliderHomeBullets  slider_engine_revo slider_alias_rev-hr1">
					<div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullscreen-container">
						<div id="rev_slider_2_1" class="rev_slider fullscreenbanner">
							<ul>
								<li data-transition="random-static" data-slotamount="7" data-masterspeed="300" data-thumb="images/slider/slide1_1-320x200.jpg" data-saveperformance="off" data-title="Slide">
									<img src="{{ url('/images/slider/slide1_1.jpg')}}" alt="slide1_1" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
									<div class="tp-caption hrsl1btn lfr tp-resizeme" data-x="right" data-hoffset="-150" data-y="center" data-voffset="150" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300" >
										<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
											<a href="#" class="">View Project</a>
										</div>
									</div>
									<div class="tp-caption hrsl11text lfr tp-resizeme" data-x="right" data-hoffset="-150" data-y="center" data-voffset="0" data-speed="1250" data-start="1250" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">Learn how our design-build<br/>remodeling process makes<br/>your life easier.
									</div>
									<div class="tp-caption hrsl11head lfr tp-resizeme" data-x="right" data-hoffset="-150" data-y="center" data-voffset="-90" data-speed="1000" data-start="750" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">Design & Remodeling
									</div>
								</li>
								<li data-transition="random-static" data-slotamount="7" data-masterspeed="300" data-thumb="images/slider/slide1_2-320x200.jpg" data-saveperformance="off" data-title="Slide">
									<img src="images/slider/slide1_2.jpg" alt="slide1_2" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
									<div class="tp-caption hrsl1btnregister lfb tp-resizeme" data-x="right" data-hoffset="-150" data-y="center" data-voffset="150" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">
										<div class="sc_button sc_button_style_global sc_button_size_medium registered squareButton global medium">
											<a href="#user-popUp" class=" user-popup-link">Register Now</a>
										</div>
									</div>
									<div class="tp-caption hrsl11text lfb tp-resizeme" data-x="right" data-hoffset="-150" data-y="center" data-voffset="-22" data-speed="1250" data-start="1250" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">Transforming Homes<br/>and Building Clients For Life
									</div>
									<div class="tp-caption hrsl11head lfb tp-resizeme" data-x="right" data-hoffset="-150" data-y="center" data-voffset="-90" data-speed="1000" data-start="750" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">Design & Remodeling
									</div>
								</li>
								<li data-transition="random-static" data-slotamount="7" data-masterspeed="300" data-thumb="images/slider/slide1_3-320x200.jpg" data-saveperformance="off" data-title="Slide">
									<img src="images/slider/slide1_3.jpg" alt="slide1_3" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
									<div class="tp-caption hrsl1btnregister lfl tp-resizeme" data-x="right" data-hoffset="-150" data-y="center" data-voffset="150" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">
										<div class="sc_button sc_button_style_global sc_button_size_medium registered squareButton global medium">
											<a href="#user-popUp" class=" user-popup-link">Register Now</a>
										</div>
									</div>
									<div class="tp-caption hrsl11text lfl tp-resizeme" data-x="right" data-hoffset="-150" data-y="center" data-voffset="0" data-speed="1250" data-start="1250" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">Register Now<br/>for our FREE Design and<br/>Remodeling seminar!
									</div>
									<div class="tp-caption hrsl11head lfl tp-resizeme" data-x="right" data-hoffset="-150" data-y="center" data-voffset="-90" data-speed="1000" data-start="750" data-easing="Power3.easeInOut" data-splitin="none" data-splitout="none" data-elementdelay="0.1" data-endelementdelay="0.1" data-endspeed="300">Design & Remodeling
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
				<div class="content">
					<div class="itemscope" itemscope itemtype="http://schema.org/Article">
						<section class="post postAlter no_margin page type-page status-publish hentry">
							<article class="post_content">
								<div class="post_text_area" itemprop="articleBody">
									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<h1 class="sc_title sc_aligncenter sc_title_regular" data-animation="animated fadeInUp">About</h1>
													<h3 class="sc_undertitle sc_aligncenter sc_title_regular margin_bottom_big" data-animation="animated fadeInUp">REMODELING AND HOME DESIGN</h3>
													<div class="sc_blogger no_margin sc_blogger_horizontal style_image style_image_large" data-animation="animated fadeInUp">
														<div class="columnsWrap">
															<div class="columns1_3 column_item_1 odd first">
																<article class="sc_blogger_item">
																	<h4 class="sc_blogger_title sc_title">
																	<a href="#">We’re Passionate</a>
																	</h4>
																	<div class="sc_blogger_content">
																	We turn your home into the coziest place on earth. Many year experience and hundreds of projects have made our team a professional on the design area... </div>
																</article>
															</div>
															<div class="columns1_3 column_item_2 even">
																<article class="sc_blogger_item">
																	<h4 class="sc_blogger_title sc_title">
																	<a href="#">We’re Professional</a>
																	</h4>
																	<div class="sc_blogger_content">
																	We are attentive to our customers, and we work hard to make sure you receive all answers. We stand behind our promise to deliver timely and quality service... </div>
																</article>
															</div>
															<div class="columns1_3 column_item_3 odd columns_last">
																<article class="sc_blogger_item sc_blogger_item_last">
																	<h4 class="sc_blogger_title sc_title">
																	<a href="#">We’re Creative</a>
																	</h4>
																	<div class="sc_blogger_content">
																	Our company does its work with the customer in mind. That is why your are getting the best service in the country to fulfill your most sophisticated... </div>
																</article>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_section sc_aligncenter darkgrey_section">
													<div class="sc_content main" data-animation="animated fadeInUp">
														<h3 class="sc_undertitle sc_aligncenter sc_title_regular margin_bottom_big">CLIENTS / PARTNERS / AWARDS</h3>
														<div class="sc_section" data-animation="animated fadeInUp">
															<div class="sc_blogger sc_blogger_horizontal style_portfolio3 portfolioWrap">
																<section class="portfolio isotope folio3col no_margin no_padding" data-columns="3">
																	<article class="isotopeElement post_format_standard hover_Dir odd flt_">
																		<div class="hover hoverDirShow">
																			<div class="thumb">
																				<img alt="Certificate of Achievement" src="{{ url('/images/001-350x252.png')}}">
																			</div>
																			<div class="folioShowBlock">
																				<div class="folioContentAfter">
																					<h4>
																					<a href="#">Certificate of Achievement</a>
																					</h4>
																					<p>
																					Nullam vitae dui tellus. Sed eu vestibulum sapien, nec molestie nisi. Morbi vitae ex... </p>
																				</div>
																			</div>
																		</div>
																	</article>
																	<article class="isotopeElement post_format_standard hover_Dir even flt_">
																		<div class="hover hoverDirShow">
																			<div class="thumb">
																				<img alt="Jorgen" src="{{ url('/images/005-350x252.png')}}">
																			</div>
																			<div class="folioShowBlock">
																				<div class="folioContentAfter">
																					<h4>
																					<a href="#">Jorgen</a>
																					</h4>
																					<p>
																					Nullam vitae dui tellus. Sed eu vestibulum sapien, nec molestie nisi. Morbi vitae ex... </p>
																				</div>
																			</div>
																		</div>
																	</article>
																	<article class="isotopeElement post_format_standard hover_Dir odd flt_">
																		<div class="hover hoverDirShow">
																			<div class="thumb">
																				<img alt="Okolo Sport" 
																				src="{{ url('/images/004-350x252.png')}}">
																			</div>
																			<div class="folioShowBlock">
																				<div class="folioContentAfter">
																					<h4>
																					<a href="#">Okolo Sport</a>
																					</h4>
																					<p>
																					Nullam vitae dui tellus. Sed eu vestibulum sapien, nec molestie nisi. Morbi vitae ex... </p>
																				</div>
																			</div>
																		</div>
																	</article>
																	<article class="isotopeElement post_format_standard hover_Dir even flt_">
																		<div class="hover hoverDirShow">
																			<div class="thumb">
																				<img alt="JG" src="{{ url('/images/003-350x252.png')}}">
																			</div>
																			<div class="folioShowBlock">
																				<div class="folioContentAfter">
																					<h4>
																					<a href="#">JG</a>
																					</h4>
																					<p>
																					Nullam vitae dui tellus. Sed eu vestibulum sapien, nec molestie nisi. Morbi vitae ex... </p>
																				</div>
																			</div>
																		</div>
																	</article>
																	<article class="isotopeElement post_format_standard hover_Dir odd flt_">
																		<div class="hover hoverDirShow">
																			<div class="thumb">
																				<img alt="TATES" src="{{ url('/images/002-350x252.png')}}">
																			</div>
																			<div class="folioShowBlock">
																				<div class="folioContentAfter">
																					<h4>
																					<a href="#">TATES</a>
																					</h4>
																					<p>
																					Nullam vitae dui tellus. Sed eu vestibulum sapien, nec molestie nisi. Morbi vitae ex... </p>
																				</div>
																			</div>
																		</div>
																	</article>
																	<article class="isotopeElement post_format_standard hover_Dir even last flt_">
																		<div class="hover hoverDirShow">
																			<div class="thumb">
																				<img alt="BBB" src="{{ url('/images/006-350x252.png')}}">
																			</div>
																			<div class="folioShowBlock">
																				<div class="folioContentAfter">
																					<h4>
																					<a href="#">BBB</a>
																					</h4>
																					<p>
																					Nullam vitae dui tellus. Sed eu vestibulum sapien, nec molestie nisi. Morbi vitae ex... </p>
																				</div>
																			</div>
																		</div>
																	</article>
																</section>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_section dark image_bg">
													<div class="sc_content main" data-animation="animated fadeInUp">
														<h1 class="sc_title sc_title_regular sc_aligncenter">Client Stories</h1>
														<div class="sc_testimonials sc_testimonials_style_2 sc_testimonials_padding sc_testimonials_controls_top margin_bottom_none">
															<div class="sc_slider sc_slider_swiper sc_slider_controls sc_slider_controls_top sc_slider_nopagination sc_slider_autoheight swiper-slider-container">
																<ul class="sc_testimonials_items slides swiper-wrapper">
																	<li class="sc_testimonials_item swiper-slide">
																		<div class="sc_testimonials_item_content">
																			<div class="sc_testimonials_item_quote">
																				<div class="sc_testimonials_item_text">A friend recommended M2, and I couldn&#8217;t be happier&#8230;it was fast, easy, and most importantly, it allowed me to focus on my business while leaving the construction matters to the professionals.</div>
																			</div>
																			<div class="sc_testimonials_item_author">
																				<div class="sc_testimonials_item_avatar">
																					<img alt="01.jpg" src="{{ url('/images/01-50x50.jpg')}}">
																				</div>
																				<div class="sc_testimonials_item_name">John Doe</div>
																				<div class="sc_testimonials_item_position">SEO, Some Co.</div>
																			</div>
																		</div>
																	</li>
																	<li class="sc_testimonials_item swiper-slide">
																		<div class="sc_testimonials_item_content">
																			<div class="sc_testimonials_item_quote">
																				<div class="sc_testimonials_item_text">Best purchase i made on ThemeForest. Great Theme!</div>
																			</div>
																			<div class="sc_testimonials_item_author">
																				<div class="sc_testimonials_item_avatar">
																					<img alt="02.jpg" src="{{ url('/images/02-50x50.jpg')}}">
																				</div>
																				<div class="sc_testimonials_item_name">Anna Smith</div>
																				<div class="sc_testimonials_item_position">Purchaser</div>
																			</div>
																		</div>
																	</li>
																	<li class="sc_testimonials_item swiper-slide">
																		<div class="sc_testimonials_item_content">
																			<div class="sc_testimonials_item_quote">
																				<div class="sc_testimonials_item_text">Hi! I’ve just check and it works perfectly! Thank you very much for your kindness and for all the work you’ve done to solve all issues. I’ll write about your fantastic support wherever I can. Thanks again.</div>
																			</div>
																			<div class="sc_testimonials_item_author">
																				<div class="sc_testimonials_item_avatar">
																					<img alt="3.jpg" src="{{ url('/images/3-50x50.jpg')}}">
																				</div>
																				<div class="sc_testimonials_item_name">Anna Black</div>
																				<div class="sc_testimonials_item_position">Purchaser</div>
																			</div>
																		</div>
																	</li>
																	<li class="sc_testimonials_item swiper-slide">
																		<div class="sc_testimonials_item_content">
																			<div class="sc_testimonials_item_quote">
																				<div class="sc_testimonials_item_text">Hello All, Firstly, thank you so much for the great template! I have spent around 10hrs looking for a good template and yours was the best!</div>
																			</div>
																			<div class="sc_testimonials_item_author">
																				<div class="sc_testimonials_item_avatar">
																					<img alt="2.jpg" src="{{ url('/images/2-50x50.jpg')}}">
																				</div>
																				<div class="sc_testimonials_item_name">Mike Doe</div>
																				<div class="sc_testimonials_item_position">Purchaser</div>
																			</div>
																		</div>
																	</li>
																	<li class="sc_testimonials_item swiper-slide">
																		<div class="sc_testimonials_item_content">
																			<div class="sc_testimonials_item_quote">
																				<div class="sc_testimonials_item_text">Great theme &amp; fast Support.</div>
																			</div>
																			<div class="sc_testimonials_item_author">
																				<div class="sc_testimonials_item_avatar">
																					<img alt="1.jpg" src="{{ url('/images/1-50x50.jpg')}}">
																				</div>
																				<div class="sc_testimonials_item_name">Nicole Smith</div>
																				<div class="sc_testimonials_item_position">Purchaser</div>
																			</div>
																		</div>
																	</li>
																</ul>
															</div>
															<ul class="flex-direction-nav">
																<li>
																	<a class="flex-prev" href="#"> </a>
																</li>
																<li>
																	<a class="flex-next" href="#"> </a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="vc_row wpb_row vc_row-fluid light_section">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<h1 class="sc_title sc_title_regular sc_aligncenter margin_bottom_mini" data-animation="animated fadeInUp">Projects</h1>
													<div class="sc_section sc_aligncenter" data-animation="animated fadeInUp">
														<div class="sc_blogger sc_blogger_horizontal style_portfolio3 portfolioWrap">
															<div class="isotopeFiltr">
																<ul>
																	<li class="squareButton active">
																		<a href="#" data-filter="*">All</a>
																	</li>
																	<li class="squareButton">
																		<a href="#" data-filter=".flt_252">living rooms</a>
																	</li>
																	<li class="squareButton">
																		<a href="#" data-filter=".flt_253">kitchens</a>
																	</li>
																	<li class="squareButton">
																		<a href="#" data-filter=".flt_251">buildings</a>
																	</li>
																	<li class="squareButton">
																		<a href="#" data-filter=".flt_250">exterior design</a>
																	</li>
																</ul>
															</div>
															<section class="portfolio isotope folio3col" data-columns="3">
																<article class="isotopeElement hover_Shift odd flt_252">
																	<div class="ih-item colored square effect6 scale_up">
																		<a href="#">
																			<div class="img">
																				<img alt="Living Room Staircases" src="images/projects-1-350x252.jpg">
																			</div>
																			<div class="info">
																				<div class="info-back">
																					<h4>Living Room Staircases</h4>
																					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod... </p>
																					<div class="link-wrapper">View</div>
																				</div>
																			</div>
																		</a>
																	</div>
																</article>
																<article class="isotopeElement hover_Shift even flt_253">
																	<div class="ih-item colored square effect6 scale_up">
																		<a href="#">
																			<div class="img">
																				<img alt="Kitchen Design Ideas" src="images/projects-2-350x252.jpg">
																			</div>
																			<div class="info">
																				<div class="info-back">
																					<h4>Kitchen Design Ideas</h4>
																					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod... </p>
																					<div class="link-wrapper">View</div>
																				</div>
																			</div>
																		</a>
																	</div>
																</article>
																<article class="isotopeElement hover_Shift odd flt_253 flt_252">
																	<div class="ih-item colored square effect6 scale_up">
																		<a href="#">
																			<div class="img">
																				<img alt="10 Design Lessons" src="images/projects-4-350x252.jpg">
																			</div>
																			<div class="info">
																				<div class="info-back">
																					<h4>10 Design Lessons</h4>
																					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod... </p>
																					<div class="link-wrapper">View</div>
																				</div>
																			</div>
																		</a>
																	</div>
																</article>
																<article class="isotopeElement hover_Shift even flt_252">
																	<div class="ih-item colored square effect6 scale_up">
																		<a href="#">
																			<div class="img">
																				<img alt="Exterior Design Ideas" src="images/kaboompics.com_Old-building-350x252.jpg">
																			</div>
																			<div class="info">
																				<div class="info-back">
																					<h4>Exterior Design Ideas</h4>
																					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod... </p>
																					<div class="link-wrapper">View</div>
																				</div>
																			</div>
																		</a>
																	</div>
																</article>
																<article class="isotopeElement hover_Shift odd flt_252">
																	<div class="ih-item colored square effect6 scale_up">
																		<a href="#">
																			<div class="img">
																				<img alt="Interior Design Ideas" src="images/projects-5-350x252.jpg">
																			</div>
																			<div class="info">
																				<div class="info-back">
																					<h4>Interior Design Ideas</h4>
																					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod... </p>
																					<div class="link-wrapper">View</div>
																				</div>
																			</div>
																		</a>
																	</div>
																</article>
																<article class="isotopeElement hover_Shift even last flt_251 flt_250">
																	<div class="ih-item colored square effect6 scale_up">
																		<a href="#">
																			<div class="img">
																				<img alt="Bedroom Design Ideas" src="images/kaboompics.com_Words-on-the-wall-350x252.jpg">
																			</div>
																			<div class="info">
																				<div class="info-back">
																					<h4>Bedroom Design Ideas</h4>
																					<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod... </p>
																					<div class="link-wrapper">View</div>
																				</div>
																			</div>
																		</a>
																	</div>
																</article>
															</section>
														</div>
														<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
															<a href="#" class="">VIEW MORE</a>
														</div>
														<div class="sc_line sc_line_style_solid_2 sc_aligncenter margin_bottom_none margin_top_large"></div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="vc_row wpb_row vc_row-fluid light_section">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_section">
													<div class="sc_content main">
														<div class="columnsWrap sc_columns no_margin sc_columns_count_2">
															<div class="columns1_2 sc_column_item sc_column_item_1 odd first" data-animation="animated fadeInUp">
																<h3 class="sc_undertitle sc_title_regular">WHY CHOOSE US</h3>
																<div class="wpb_text_column wpb_content_element ">
																	<div class="wpb_wrapper">
																		<p>We turn your home into the coziest place on earth. Many year experience and hundreds of projects have made our team a professional on this area. We are attentive to our customers and we stand behind our promise to deliver timely and quality service. Our company does its work with the customer in mind. That is why your are getting the best service in the country to fulfill your most sophisticated demands. </p>
																	</div>
																</div>
															</div>
															<div class="columns1_2 sc_column_item sc_column_item_2 even" data-animation="animated fadeInUp">
																<h3 class="sc_undertitle sc_title_regular">SKILLS</h3>
																<div class="sc_skills sc_skills_bar no_padding sc_skills_horizontal" data-type="bar" data-subtitle="Skills" data-dir="horizontal">
																	<div class="sc_skills_info">Electrical</div>
																	<div class="sc_skills_info">95%</div>
																	<div class="sc_skills_item sc_skills_style_1 odd first">
																		<div class="sc_skills_count">
																			<div class="sc_skills_total" data-start="0" data-stop="95" data-step="1" data-max="100" data-speed="17" data-duration="1615" data-ed="">0</div>
																		</div>
																	</div>
																	<div class="sc_skills_info">Cabinet Repairs</div>
																	<div class="sc_skills_info">90%</div>
																	<div class="sc_skills_item sc_skills_style_1 even">
																		<div class="sc_skills_count">
																			<div class="sc_skills_total" data-start="0" data-stop="90" data-step="1" data-max="100" data-speed="30" data-duration="2700" data-ed="">0</div>
																		</div>
																	</div>
																	<div class="sc_skills_info">Plumbing Repairs</div>
																	<div class="sc_skills_info">99%</div>
																	<div class="sc_skills_item sc_skills_style_1 odd">
																		<div class="sc_skills_count">
																			<div class="sc_skills_total" data-start="0" data-stop="99" data-step="1" data-max="100" data-speed="31" data-duration="3069" data-ed="">0</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_section grey_section">
													<div class="sc_content main" data-animation="animated fadeInUp">
														<h3 class="sc_undertitle sc_title_regular">REQUEST FREE QUOTE</h3>
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<p>Please fill in the form below to request a free quote and one of our experts will contact you to arrange a meeting. This visit<br/>
																will serve to evaluate your needs and give you a personalized and competitive quotation. </p>
															</div>
														</div>
														<div class="sc_contact_form sc_contact_form_contact_3 margin_top_small">
															<form data-formtype="contact_3" method="post" action="include/sendmail.php">
																<div class="columnsWrap sc_columns sc_columns_count_4 no_padding">
																	<div class="columns1_4 sc_column_item sc_column_item_1 odd first">
																		<input id="sc_contact_form_username" type="text" name="username" placeholder="Name">
																		<input id="sc_contact_form_phone" type="text" name="phone" placeholder="Phone (Ex.0123456789)">
																	</div>
																	<div class="columns1_4 sc_column_item sc_column_item_2 even">
																		<input id="sc_contact_form_email" type="text" name="email" placeholder="E-mail">
																		<input id="sc_contact_form_subj" type="text" name="subject" placeholder="Subject">
																	</div>
																	<div class="columns2_4 sc_column_item sc_column_item_3 odd span_2">
																		<textarea id="sc_contact_form_message" class="textAreaSize" name="message" placeholder="Message"></textarea>
																		<div class="sc_contact_form_button">
																			<div class="sc_button sc_button_style_dark sc_button_size_huge squareButton dark huge">
																				<a href="#" class="sc_contact_form_submit">Submit</a>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="result sc_infobox"></div>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tagsWrap"></div>
								</div>  
							</article>  
						</section>  
					</div>    
				 
				</div>  
			</div>

@endsection			