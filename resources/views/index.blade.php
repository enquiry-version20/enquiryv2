@extends('layout.frontend.design')

@section('content')

<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
		<!-- <form  method="post" action="{{ url('/contact-form-submit')}}" id="frmSearch"> -->


			<div class="container-fluid" >
				<div class="row bg-image" >				
			    <img src="/images/slider/slider3_2.png" class="img-responsive" 
				style="width:100%;object-fit: cover;min-height: 286px;">
						
						<div class="wrapper" >
							<input type="text" class="input" id="search-posts" placeholder="What can we help with?" >
							<br/>
							<div class="searchbtn" >
								<a href="" id="searchbtn"><span>Search</span></a>
							</div>
							<div style="position: absolute;">
							<div class="searchlist">
								<div class="col-md-12 form-control-comment" id="serviceList">
								</div>
							</div>	
							</div>		
						</div>
						
						
				</div>
				
			</div>
		<!-- </form> -->

			<div class="mainWrap without_sidebar">
				<div class="content">
					<div class="itemscope" itemscope itemtype="http://schema.org/Article">
						<section class="post postAlter no_margin page type-page status-publish hentry">
							<article class="post_content">
								<div class="post_text_area" itemprop="articleBody">
									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<h1 class="sc_title sc_aligncenter sc_title_regular" data-animation="animated fadeInUp">Services</h1>
													<h3 class="sc_undertitle sc_aligncenter sc_title_regular margin_bottom_small" data-animation="animated fadeInUp">Remodeling and Home Design</h3>
													<div class="sc_section sc_aligncenter">
														<div class="columnsWrap sc_columns sc_columns_count_3 margin_bottom_big" data-animation="animated fadeInUp" >
															@if(count($main_services)>0)
															@foreach($main_services as $main_service)
															<div class="columns1_3 sc_column_item sc_column_item_1 odd first">
																<a href="{{ url('/main-services/'.$main_service->id)}}">
																	<div class="sc_section section_desc">
																		<div class="sc_title_icon_animated">
																			<div class="sc_title_icon sc_title_bg ">

																				<img src="{{url($main_service->service_image)}}">

																			</div>
																			<h5 class="sc_title sc_title_iconed">{{$main_service->service_name}}</h5>
																		</div>
																	</div>
																	<div class="sc_section ">
																		<div class="wpb_text_column wpb_content_element ">
																			<div class="wpb_wrapper">
																				<p>{!! $main_service->description !!} </p>
																			</div>
																		</div>
																	</div>
																</a>
															</div>
															@endforeach
															@endif

														</div>
														<div class="columnsWrap sc_columns sc_columns_count_3" data-animation="animated fadeInUp">

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="vc_row wpb_row vc_row-fluid grey_section">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<h3 class="sc_undertitle sc_title_regular" 
													style="text-align: center;margin-bottom: 35px;font-size: 24px;">WHY CHOOSE US</h3>
													<div class="sc_section sc_aligncenter">
														<div class="columnsWrap sc_columns sc_columns_count_3 margin_bottom_big" data-animation="animated fadeInUp">
															<div class="columns1_3 sc_column_item sc_column_item_1 odd first">
																	<div class="sc_section">
																		<div class="sc_title_icon_animated choose">
																			<div class="sc_title_icon sc_title_top sc_size_medium icon-cancel-circle sc_title_bg sc_bg_animated">
																			</div>
																			<p class="sc_title sc_title_iconed" style="margin-left:7px;margin-right:7px">Help to connect professional near your dream project</p>
																		</div>
																	</div>
															</div>
															<div class="columns1_3 sc_column_item sc_column_item_2 even">
																	<div class="sc_section">
																		<div class="sc_title_icon_animated choose">
																			<div class="sc_title_icon sc_title_top sc_size_medium icon-dribbble-1 sc_title_bg sc_bg_animated">
																			</div>
																			<p class="sc_title sc_title_iconed" style="margin-left:7px;margin-right:7px">Helps to compare quotations from experts</p>
																		</div>
																	</div>
															</div>
															<div class="columns1_3 sc_column_item sc_column_item_3 odd">
																	<div class="sc_section">
																		<div class="sc_title_icon_animated choose">
																			<div class="sc_title_icon sc_title_top sc_size_medium icon-search sc_title_bg sc_bg_animated">
																			</div>
																			<p class="sc_title sc_title_iconed" style="margin-left:7px;margin-right:7px">Book Online</p>
																		</div>
																	</div>
															</div>
														</div>
														<div class="columnsWrap sc_columns sc_columns_count_3" data-animation="animated fadeInUp">
															<div class="columns1_3 sc_column_item sc_column_item_1 odd first">
																	<div class="sc_section">
																		<div class="sc_title_icon_animated choose">
																			<div class="sc_title_icon sc_title_top sc_size_medium icon-rss sc_title_bg sc_bg_animated">
																			</div>
																			<p class="sc_title sc_title_iconed" style="margin-left:7px;margin-right:7px">Save Time and Money</p>
																		</div>
																	</div>
															</div>
															<div class="columns1_3 sc_column_item sc_column_item_2 even">
																	<div class="sc_section">
																		<div class="sc_title_icon_animated choose">
																			<div class="sc_title_icon sc_title_top sc_size_medium icon-monitor sc_title_bg sc_bg_animated">
																			</div>
																			<p class="sc_title sc_title_iconed" style="margin-left:7px;margin-right:7px">Get Advise from Experts</p>
																		</div>
																	</div>
															</div>
															<div class="columns1_3 sc_column_item sc_column_item_3 odd">
																	<div class="sc_section">
																		<div class="sc_title_icon_animated choose">
																			<div class="sc_title_icon sc_title_top sc_size_medium icon-right-open sc_title_bg sc_bg_animated">
																			</div>
																			<p class="sc_title sc_title_iconed" style="margin-left:7px;margin-right:7px">Schedule your work</p>
																		</div>
																	</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_section sc_aligncenter darkgrey_section">
													<div class="sc_content main" data-animation="animated fadeInUp">
														<h3 class="sc_undertitle sc_aligncenter sc_title_regular">Newsletter</h3>
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<p>Yes, I am interested in receiving the free Transformations newsletter which has remodeling news for my lifestyle and my point of<br/>
																view. I prefer to receive the newsletter. </p>
															</div>
														</div>
														<div class="sc_button sc_button_style_global sc_button_size_small squareButton global small">
															<a href="#form_newsletter" class=" user-popup-link">Subscribe</a>
														</div>
														<div id="form_newsletter" class="sc_popup sc_popup_light mfp-with-anim mfp-hide">
															<div class="sc_emailer inputSubmitAnimation sFocus rad4 opened">
																<form>
																	<input type="text" class="sInput" name="email" value="" placeholder="Please, enter you email address.">
																</form>
																<a href="#" class="sc_emailer_button searchIcon aIco mail" title="Submit" data-group="E-mail collector group">
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_section grey_section">
													<div class="sc_content main" data-animation="animated fadeInUp">
														<h3 class="sc_undertitle sc_title_regular">REQUEST FREE QUOTE</h3>
														@if (session()->has('success'))
														<div class="alert alert-success">
														    @if(is_array(session('success')))
														        <ul>
														            @foreach (session('success') as $message)
														                <li>{{ $message }}</li>
														            @endforeach
														        </ul>
														    @else
														        {{ session('success') }}
														    @endif
														</div>
														@endif
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<p>Please fill in the form below to request a free quote and one of our experts will contact you to arrange a meeting. This visit<br/>
																will serve to evaluate your needs and give you a personalized and competitive quotation. </p>
															</div>
														</div>
														<div class="sc_contact_form sc_contact_form_contact_3 margin_top_small">
															<form data-formtype="contact_3" method="post" action="{{ url('/contact/email')}}" id="frmcontact">
																{{ csrf_field() }}
																<div class="columnsWrap sc_columns sc_columns_count_4 no_padding">
																	<div class="columns1_4 sc_column_item sc_column_item_1 odd first">
																		<input id="sc_contact_form_username" type="text" name="name" placeholder="Name" required>
																		<input id="sc_contact_form_phone" type="text" name="phone" placeholder="Phone (Ex.0123456789)" required>
																	</div>
																	<div class="columns1_4 sc_column_item sc_column_item_2 even">
																		<input id="sc_contact_form_email" type="text" name="email" placeholder="E-mail" required>
																		<input id="sc_contact_form_subj" type="text" name="subject" placeholder="Subject" required>
																	</div>


																	<div class="columns2_4 sc_column_item sc_column_item_3 odd span_2">
																		<textarea id="sc_contact_form_message" class="textAreaSize" name="message" placeholder="Message"></textarea>
																		<div class="sc_contact_form_button">
																			<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
																		<button type="submit">Submit</button>	
																		</div>
																		</div>
																	</div>

																</div>
																<div class="result sc_infobox"></div>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="vc_row wpb_row vc_row-fluid" id="partners">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main partner_section">

													<div class="sc_section scrollbar_hide bg_tint_none sc_scroll_controls sc_scroll_controls_horizontal sc_scroll_controls_type_side" data-animation="animated fadeInUp">
														<h3 class="sc_undertitle sc_title_regular" 
													style="text-align: center;">PARTNERS</h3>
														<div class="sc_scroll sc_scroll_horizontal swiper-slider-container scroll-container">
															<div class="sc_scroll_wrapper swiper-wrapper">
																<div class="sc_scroll_slide swiper-slide">
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-1-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-2-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-3-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-4-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-5-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-1-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-2-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-3-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-4-150x143.jpg" alt=""/>
																	</figure>
																	<figure class="sc_image  sc_image_align_left sc_image_shape_square">
																		<img src="images/partner-5-150x143.jpg" alt=""/>
																	</figure>
																</div>
															</div>
															<div class="sc_scroll_bar sc_scroll_bar_horizontal"></div>
														</div>
														<ul class="flex-direction-nav">
															<li>
																<a class="flex-prev" href="#"></a>
															</li>
															<li>
																<a class="flex-next" href="#"></a>
															</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tagsWrap"></div>
								</div>  
							</article>  
						</section>  
					</div>    
				 
				</div>  
			</div>
<style type="text/css">

  .topWrap .logo{
		display: block;
	}
	.menuTopWrap{
		display: block;
	}
	.section_desc{
		    padding-top: 10%;
	}
	.topWrap{
		display: block;
	}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript">
	$(window).scroll(function() {
	  if ($(this).scrollTop() > 0) {
	    $('.mainmenu_area').fadeOut();
	  } else {
	    $('.mainmenu_area').fadeIn();
	  }
	});		
	</script>

	<script type="text/javascript">
		$('body').on('keyup','#search-posts',function(){
			var searchQuest = $(this).val();
			$.ajax({
				method:'POST',
				url:'{{url("/searching")}}',
				
				data:{
					'_token':'{{csrf_token()}}',
					searchQuest:searchQuest
				},

				success:function(data){
					
					$('#serviceList').fadeIn();  
						if(data==1){
						$('#serviceList').html('');
					}else{
							$('#serviceList').html(data);
						}

				}
			})
		});
	
$(document).on('click', 'li', function(){  

    var result=$(this).text();
	var href = $(this).attr('id');
	$("#searchbtn").attr("href", href)
    $('#search-posts').val(result);  
  	$("html, body").animate({ scrollTop: 220 }, "fast");
	$('#serviceList').html('');
   
}); 

$('body').on('click', function(){
      //do some code here i.e
     
	  var textval= $('#search-posts').val();  
	  if(!textval){
		$('#serviceList').html('');

	  }
    });
// $(document).on('focusout', '#search-posts', function(){ 

// 	var result=$(this).val();
// 	var lival=$('li').val();
	
// 	if(result==''){
// 		console.log('hii');
// 		$('#serviceList').html('');
// 		$("#searchbtn").attr("href", '#');
// 	}
	

// }); 


// var last_position = 0;
// $(document).ready(function () {
//     $("#search-posts").on("focusout", function() {
		
// 		console.log('hii');
// 	$('#serviceList').html('');
// 	$("#searchbtn").attr("href", '#');
//     });
// });
</script>


@endsection
