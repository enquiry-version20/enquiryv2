@extends('layout.frontend.design')
@section('content')
			<div id="topOfPage" class="topTabsWrap">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">All Posts</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<span class="current">Blog with sidebar</span>
					</div>
					<h3 class="pageTitle h3">Blog with sidebar</h3>
				</div>
			</div>

			<div class="mainWrap with_sidebar sideBarRight">
				<div class="main" role="main">
					<div class="content">
						<article class="postCenter hrShadow odd post margin_bottom_middle no_padding">
							<h2 class="post_title">
							<a href="#">Luxury Vinyl Tile Installation</a>
							</h2>
							<div class="sc_section columns3_3 post_thumb thumb">
								<a href="#">
									<img alt="Luxury Vinyl Tile Installation" src="images/Depositphotos_61632697_original-714x402.jpg">
								</a>
							</div>
							<div class="postStandard">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat</p>
							</div>
							<div class="postSharing">
								<ul>
									<li class="squareButton dark">
										<a title="More" href="#">More</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-eye" title="Views - 339" href="#">339</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-comment-3" title="Comments - 2" href="#comments">2</a>
									</li>
									<li class="squareButton light ico likeButton like" data-postid="791" data-likes="4" data-title-like="Like" data-title-dislike="Dislike">
										<a class="icon-heart-1" title="Like - 4" href="#">
											<span class="likePost">4</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="post_info infoPost">
								Posted
								<a href="#" class="post_date">July 21, 2014</a>
								<span class="separator">|</span>
								by
								<a href="#" class="post_author">John Doe</a>
								<span class="separator">|</span>
								<span class="post_cats">
									in
									<a class="cat_link" href="#">Post formats fullwidth</a>
								</span>
							</div>
						</article>  
					 
						<article class="postCenter hrShadow even post">
							<h2 class="post_title">
							<a href="#">Theme is easy to use with any type of displays</a>
							</h2>
							<div class="sc_section  _columns1_2 sc_aligncenter columns2_3">
								<div class="sc_border sc_border_light">
									<div class="sc_zoom margin_bottom_small">
										<img src="images/projects-2.jpg" data-zoom-image="images/projects-2.jpg" alt=""/>
									</div>
								</div>
							</div>
							<div class="postStandard">
								<div class="sc_section">
									<div class="sc_title_image sc_title_left sc_size_medium">
										<img src="images/icons/lens_icon_retina.png" alt=""/>
									</div>
									<h3 class="sc_title sc_title_iconed sc_left">Retina Ready</h3>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
								</div>
								<div class="sc_section margin_top_small">
									<div class="sc_title_image sc_title_left sc_size_medium">
										<img src="images/icons/hand_icon_retina.png" alt=""/>
									</div>
									<h3 class="sc_title sc_title_iconed sc_left">Tablet friendly</h3>
									Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
								</div>
							</div>
							<div class="postSharing">
								<ul>
									<li class="squareButton dark">
										<a title="More" href="#">More</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-eye" title="Views - 69" href="#">69</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
									</li>
									<li class="squareButton light ico likeButton like" data-postid="764" data-likes="2" data-title-like="Like" data-title-dislike="Dislike">
										<a class="icon-heart-1" title="Like - 2" href="#">
											<span class="likePost">2</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="post_info infoPost">
								Posted
								<a href="#" class="post_date">July 20, 2014</a>
								<span class="separator">|</span>
								by
								<a href="#" class="post_author">John Doe</a>
								<span class="separator">|</span>
								<span class="post_cats">
									in
									<a class="cat_link" href="#">Post formats fullwidth</a>
								</span>
							</div>
						</article>

						<article class="postCenter hrShadow odd post">
							<h2 class="post_title">
							<a href="#">Video Post Example</a>
							</h2>
							<div class="sc_section  columns2_3 sc_aligncenter margin_bottom_small">
								<div class="sc_video_player" data-width="100%" data-height="295">
									<div class="sc_video_player_title">
									</div>
									<div class="sc_video_frame sc_video_play_button" data-video="&lt;iframe class=&quot;video_frame&quot; src=&quot;https://youtube.com/embed/RB2yVfR6jbI?autoplay=1&quot; width=&quot;100%&quot; height=&quot;295&quot; frameborder=&quot;0&quot; webkitAllowFullScreen=&quot;webkitAllowFullScreen&quot; mozallowfullscreen=&quot;mozallowfullscreen&quot; allowFullScreen=&quot;allowFullScreen&quot;&gt;&lt;/iframe&gt;">
										<img alt="" src="images/slide3_3-663x295.jpg">
									</div>
								</div>
							</div>
							<div class="postVideo">
								<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								<p>Integer et porttitor massa. Donec eu commodo massa. Nullam dignissim, magna et porttitor fermentum, est massa euismod odio, at pharetra ipsum mi eu felis. Aenean sit amet ante non augue egestas ultricies vel nec urna. Fusce posuere nulla tristique nibh convallis suscipit. Phasellus interdum justo dolor, eget imperdiet arcu ullamcorper nec. </p>
							</div>
							<div class="postSharing">
								<ul>
									<li class="squareButton dark">
										<a title="More" href="#">More</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-eye" title="Views - 140" href="#">140</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
									</li>
									<li class="squareButton light ico likeButton like" data-postid="806" data-likes="0" data-title-like="Like" data-title-dislike="Dislike">
										<a class="icon-heart-1" title="Like - 0" href="#">
											<span class="likePost">0</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="post_info infoPost">
								Posted
								<a href="#" class="post_date">July 20, 2014</a>
								<span class="separator">|</span>
								by
								<a href="#" class="post_author">John Doe</a>
								<span class="separator">|</span>
								<span class="post_cats">
									in
									<a class="cat_link" href="#">Post formats fullwidth</a>
								</span>
							</div>
						</article>

						<article class="postCenter hrShadow even post">
							<h2 class="post_title">
							<a href="#">Audio Post Example With Cover Image</a>
							</h2>
							<div class="sc_section  columns2_3 sc_aligncenter">
								<figure class="sc_image  sc_image_shape_square margin_bottom_mini">
									<img src="images/Depositphotos_5486592_original.jpg" alt=""/>
								</figure>
								<div class="audio_container">
									<audio src="sounds/laura.mp3" class="sc_audio" controls="controls">
									</audio>
								</div>
							</div>
							<div class="postAudio">
								<p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#8217;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.<br/>
								There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#8217;t look even slightly believable.</p>
							</div>
							<div class="postSharing">
								<ul>
									<li class="squareButton dark">
										<a title="More" href="#">More</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-eye" title="Views - 82" href="#">82</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
									</li>
									<li class="squareButton light ico likeButton like" data-postid="808" data-likes="1" data-title-like="Like" data-title-dislike="Dislike">
										<a class="icon-heart-1" title="Like - 1" href="#">
											<span class="likePost">1</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="post_info infoPost">
								Posted
								<a href="#" class="post_date">July 19, 2014</a>
								<span class="separator">|</span>
								by
								<a href="#" class="post_author">John Doe</a>
								<span class="separator">|</span>
								<span class="post_cats">
									in
									<a class="cat_link" href="#">Post formats fullwidth</a>
								</span>
							</div>
						</article>

						<article class="postCenter hrShadow odd post">
							<h2 class="post_title">
							<a href="#">Standard Gallery</a>
							</h2>
							<div class="sc_section columns3_3 post_thumb thumb">
								<div class="sc_slider sc_slider_swiper swiper-slider-container sc_slider_controls sc_aligncenter" data-old-width="714" data-old-height="402" data-interval="9024">
									<ul class="slides swiper-wrapper">
										<li class="swiper-slide"><img src="images/kaboompics.com_Great-architects-book-wooden-shelf-714x402.jpg" alt=""></li>
										<li class="swiper-slide"><img src="images/Depositphotos_5486592_original-714x402.jpg" alt=""></li>
										<li class="swiper-slide"><img src="images/Depositphotos_2136700_original-714x402.jpg" alt=""></li>
										<li class="swiper-slide"><img src="images/Depositphotos_30042823_original-714x402.jpg" alt=""></li>
										<li class="swiper-slide"><img src="images/kaboompics.com_Wooden-decorations-714x402.jpg" alt=""></li>
									</ul>
									<ul class="flex-direction-nav">
										<li>
											<a class="flex-prev" href="#"></a>
										</li>
										<li>
											<a class="flex-next" href="#"></a>
										</li>
									</ul>
								</div>
							</div>
							<div class="postGallery">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							</div>
							<div class="postSharing">
								<ul>
									<li class="squareButton dark">
										<a title="More" href="#">More</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-eye" title="Views - 55" href="#">55</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
									</li>
									<li class="squareButton light ico likeButton like" data-postid="810" data-likes="0" data-title-like="Like" data-title-dislike="Dislike">
										<a class="icon-heart-1" title="Like - 0" href="#">
											<span class="likePost">0</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="post_info infoPost">
								Posted
								<a href="#" class="post_date">July 18, 2014</a>
								<span class="separator">|</span>
								by
								<a href="#" class="post_author">John Doe</a>
								<span class="separator">|</span>
								<span class="post_cats">
									in
									<a class="cat_link" href="#">Post formats fullwidth</a>
								</span>
							</div>
						</article>

						<article class="postCenter hrShadow even post">
							<h2 class="post_title">
							<a href="#">Theme Styled Gallery</a>
							</h2>
							<div class="sc_section columns3_3 post_thumb thumb">
								<div class="sc_slider sc_slider_swiper swiper-slider-container sc_slider_controls sc_aligncenter" data-old-width="714" data-old-height="402" data-interval="8506">
									<ul class="slides swiper-wrapper">
										<li class="swiper-slide"><img src="images/Depositphotos_5486592_original-714x402.jpg" alt=""></li>
										<li class="swiper-slide"><img src="images/Depositphotos_3766330_original-714x402.jpg" alt=""></li>
										<li class="swiper-slide"><img src="images/kaboompics.com_Great-architects-book-wooden-shelf-714x402.jpg" alt=""></li>
										<li class="swiper-slide"><img src="images/kaboompics.com_Old-building-714x402.jpg" alt=""></li>
										<li class="swiper-slide"><img src="images/Depositphotos_4641223_original-714x402.jpg" alt=""></li>
									</ul>
									<ul class="flex-direction-nav">
										<li>
											<a class="flex-prev" href="#"></a>
										</li>
										<li>
											<a class="flex-next" href="#"></a>
										</li>
									</ul>
								</div>
							</div>
							<div class="postGallery">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							</div>
							<div class="postSharing">
								<ul>
									<li class="squareButton dark">
										<a title="More" href="#">More</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-eye" title="Views - 76" href="#">76</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
									</li>
									<li class="squareButton light ico likeButton like" data-postid="813" data-likes="0" data-title-like="Like" data-title-dislike="Dislike">
										<a class="icon-heart-1" title="Like - 0" href="#">
											<span class="likePost">0</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="post_info infoPost">
								Posted
								<a href="#" class="post_date">July 17, 2014</a>
								<span class="separator">|</span>
								by
								<a href="#" class="post_author">John Doe</a>
								<span class="separator">|</span>
								<span class="post_cats">
									in
									<a class="cat_link" href="#">Post formats fullwidth</a>
								</span>
							</div>
						</article>

						<article class="postCenter hrShadow odd post">
							<h2 class="post_title">
							<a href="#">Theme Slider instead of Gallery</a>
							</h2>
							<div class="sc_section columns3_3 post_thumb thumb">
								<div class="sc_slider sc_slider_swiper swiper-slider-container sc_slider_controls sc_aligncenter" data-old-width="714" data-old-height="402" data-interval="7969">
									<ul class="slides swiper-wrapper">
										<li class="swiper-slide"><img src="images/Depositphotos_30042823_original-714x402.jpg" alt=""></li>
										<li class="swiper-slide"><img src="images/Depositphotos_4641223_original-714x402.jpg" alt=""></li>
										<li class="swiper-slide"><img src="images/projects-5-714x402.jpg" alt=""></li>
										<li class="swiper-slide"><img src="images/projects-6-714x402.jpg" alt=""></li>
										<li class="swiper-slide"><img src="images/Depositphotos_2136700_original-714x402.jpg" alt=""></li>
									</ul>
									<ul class="flex-direction-nav">
										<li>
											<a class="flex-prev" href="#"></a>
										</li>
										<li>
											<a class="flex-next" href="#"></a>
										</li>
									</ul>
								</div>
							</div>
							<div class="postGallery">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
							</div>
							<div class="postSharing">
								<ul>
									<li class="squareButton dark">
										<a title="More" href="#">More</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-eye" title="Views - 135" href="#">135</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
									</li>
									<li class="squareButton light ico likeButton like" data-postid="816" data-likes="1" data-title-like="Like" data-title-dislike="Dislike">
										<a class="icon-heart-1" title="Like - 1" href="#">
											<span class="likePost">1</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="post_info infoPost">
								Posted
								<a href="#" class="post_date">July 16, 2014</a>
								<span class="separator">|</span>
								by
								<a href="#" class="post_author">John Doe</a>
								<span class="separator">|</span>
								<span class="post_cats">
									in
									<a class="cat_link" href="#">Post formats fullwidth</a>
								</span>
							</div>
						</article>

						<article class="post_format_quote postCenter hrShadow even post format-quote tag-quote-post post_format-post-format-quote">
							<div class="postQuote">
								<blockquote cite="#" class="sc_quote">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									<p class="sc_quote_title">
										<a href="#">Lisa Kudrow</a>
									</p>
								</blockquote>
							</div>
							<div class="postSharing">
								<ul>
									<li class="squareButton dark">
										<a title="More" href="#">More</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-eye" title="Views - 7" href="#">7</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
									</li>
									<li class="squareButton light ico likeButton like" data-postid="820" data-likes="0" data-title-like="Like" data-title-dislike="Dislike">
										<a class="icon-heart-1" title="Like - 0" href="#">
											<span class="likePost">0</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="post_info infoPost">
								Posted
								<a href="#" class="post_date">July 15, 2014</a>
								<span class="separator">|</span>
								by
								<a href="#" class="post_author">John Doe</a>
								<span class="separator">|</span>
								<span class="post_cats">
									in
									<a class="cat_link" href="#">Post formats fullwidth</a>
								</span>
							</div>
						</article>

						<article class="post_format_link postCenter hrShadow odd post format-link tag-link-post post_format-post-format-link">
							<div class="postLink">
								<p>
									<a href="http://themeforest.net/user/axiomthemes/portfolio">http://themeforest.net/user/axiomthemes/portfolio</a>
								</p>
							</div>
							<div class="postSharing">
								<ul>
									<li class="squareButton dark">
										<a title="More" href="http://themeforest.net/user/axiomthemes/portfolio">More</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-eye" title="Views - 6" href="http://themeforest.net/user/axiomthemes/portfolio">6</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
									</li>
									<li class="squareButton light ico likeButton like" data-postid="824" data-likes="0" data-title-like="Like" data-title-dislike="Dislike">
										<a class="icon-heart-1" title="Like - 0" href="#">
											<span class="likePost">0</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="post_info infoPost">
								Posted
								<a href="http://themeforest.net/user/axiomthemes/portfolio" class="post_date">July 14, 2014</a>
								<span class="separator">|</span>
								by
								<a href="#" class="post_author">John Doe</a>
								<span class="separator">|</span>
								<span class="post_cats">
									in
									<a class="cat_link" href="#">Post formats fullwidth</a>
								</span>
							</div>
						</article>

						<article class="post_format_status postCenter hrShadow even post format-status tag-status-post post_format-post-format-status">
							<div class="postStatus">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporincididunt ut labore et dolore. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>
							<div class="postSharing">
								<ul>
									<li class="squareButton dark">
										<a title="More" href="#">More</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-eye" title="Views - 9" href="#">9</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-comment-3" title="Comments - 1" href="#comments">1</a>
									</li>
									<li class="squareButton light ico likeButton like" data-postid="822" data-likes="0" data-title-like="Like" data-title-dislike="Dislike">
										<a class="icon-heart-1" title="Like - 0" href="#">
											<span class="likePost">0</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="post_info infoPost">
								Posted
								<a href="#" class="post_date">July 13, 2014</a>
								<span class="separator">|</span>
								by
								<a href="#" class="post_author">John Doe</a>
								<span class="separator">|</span>
								<span class="post_cats">
									in
									<a class="cat_link" href="#">Post formats fullwidth</a>
								</span>
							</div>
						</article>

						<article class="postCenter hrShadow odd post">
							<div class="postAside">
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporincididunt ut labore et dolore. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>
							<div class="postSharing">
								<ul>
									<li class="squareButton dark">
										<a title="More" href="#">More</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-eye" title="Views - 7" href="#">7</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
									</li>
									<li class="squareButton light ico likeButton like" data-postid="826" data-likes="0" data-title-like="Like" data-title-dislike="Dislike">
										<a class="icon-heart-1" title="Like - 0" href="#">
											<span class="likePost">0</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="post_info infoPost">
								Posted
								<a href="#" class="post_date">July 12, 2014</a>
								<span class="separator">|</span>
								by
								<a href="#" class="post_author">John Doe</a>
								<span class="separator">|</span>
								<span class="post_cats">
									in
									<a class="cat_link" href="#">Post formats fullwidth</a>
								</span>
							</div>
						</article>

						<article class="postCenter hrShadow even last post">
							<div class="postChat">
								<div class="sc_chat">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temporincididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
									<p class="sc_quote_title">
										<a href="#">Mike</a>
									</p>
								</div>
								<div class="sc_chat">
									<p>Vivamus nec quam nec elit porta dictum. Praesent dignissim eget ligula vel posuere. Proin sed mauris faucibus, euismod erat a, placerat odio.</p>
									<p class="sc_quote_title">
										<a href="#">Lisa</a>
									</p>
								</div>
							</div>
							<div class="postSharing">
								<ul>
									<li class="squareButton dark">
										<a title="More" href="#">More</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-eye" title="Views - 9" href="#">9</a>
									</li>
									<li class="squareButton light ico">
										<a class="icon-comment-3" title="Comments - 0" href="#comments">0</a>
									</li>
									<li class="squareButton light ico likeButton like" data-postid="828" data-likes="0" data-title-like="Like" data-title-dislike="Dislike">
										<a class="icon-heart-1" title="Like - 0" href="#">
											<span class="likePost">0</span>
										</a>
									</li>
								</ul>
							</div>
							<div class="post_info infoPost">
								Posted
								<a href="#" class="post_date">July 11, 2014</a>
								<span class="separator">|</span>
								by
								<a href="#" class="post_author">John Doe</a>
								<span class="separator">|</span>
								<span class="post_cats">
									in
									<a class="cat_link" href="#">Post formats fullwidth</a>
								</span>
							</div>
						</article>
					</div>  
					
					<div id="sidebar_main" class="widget_area sidebar_main sidebar" role="complementary">
						<aside class="widgetWrap hrShadow widget widget_archive">
							<h3 class="title">Archive</h3>
							<ul>
								<li>
									<a href='#'>February 2015</a>
								</li>
								<li>
									<a href='#'>September 2014</a>
								</li>
								<li>
									<a href='#'>August 2014</a>
								</li>
								<li>
									<a href='#'>July 2014</a>
								</li>
								<li>
									<a href='#'>June 2014</a>
								</li>
								<li>
									<a href='#'>May 2014</a>
								</li>
								<li>
									<a href='#'>April 2014</a>
								</li>
								<li>
									<a href='#'>January 2012</a>
								</li>
							</ul>
						</aside>

						<aside class="widgetWrap hrShadow widget widget_calendar">
							<h3 class="title">Calendar</h3>
							<div id="calendar_wrap">
								<table class="sc_calendar">
									<thead>
										<tr>
											<th class="prevMonth">
												<div class="left roundButton">
													<a href="#" data-type="post" data-year="2015" data-month="2" title="View posts for February 2015"></a>
												</div>
											</th>
											<th class="curMonth" colspan="5">
												<a href="#" title="View posts for November 2015">November <span>2015</span> </a>
											</th>
											<th class="nextMonth">&nbsp;</th>
										</tr>
										<tr>
											<th scope="col" title="Monday">M</th>
											<th scope="col" title="Tuesday">T</th>
											<th scope="col" title="Wednesday">W</th>
											<th scope="col" title="Thursday">T</th>
											<th scope="col" title="Friday">F</th>
											<th scope="col" title="Saturday">S</th>
											<th scope="col" title="Sunday">S</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td colspan="6" class="pad">&nbsp;</td>
											<td>1</td>
										</tr>
										<tr>
											<td>2</td>
											<td class="today">3</td>
											<td>4</td>
											<td>5</td>
											<td>6</td>
											<td>7</td>
											<td>8</td>
										</tr>
										<tr>
											<td>9</td>
											<td>10</td>
											<td>11</td>
											<td>12</td>
											<td>13</td>
											<td>14</td>
											<td>15</td>	
										</tr>
										<tr>
											<td>16</td>
											<td>17</td>
											<td>18</td>
											<td>19</td>
											<td>20</td>
											<td>21</td>
											<td>22</td>
										</tr>
										<tr>
											<td>23</td>
											<td>24</td>
											<td>25</td>
											<td>26</td>
											<td>27</td>
											<td>28</td>
											<td>29</td>
										</tr>
										<tr>
											<td>30</td>
											<td class="pad" colspan="6">&nbsp;</td>
										</tr>
									</tbody>
								</table>
							</div>
						</aside>

						<aside class="widgetWrap hrShadow widget widget_categories">
							<h3 class="title">Categories</h3>
							<ul>
								<li class="cat-item">
									<a href="#">Certificates</a> (5)
								</li>
								<li class="cat-item">
									<a href="#">House Maintenance</a> (1)
								</li>
								<li class="cat-item">
									<a href="#">Main Page</a> (3)
								</li>
								<li class="cat-item">
									<a href="#">Post formats &amp; All widgets</a> (12)
									<ul class='children'>
										<li class="cat-item">
											<a href="#">Post formats fullwidth</a> (12)
										</li>
									</ul>
								</li>
								<li class="cat-item">
									<a href="#">Posts slider</a> (5)
								</li>
								<li class="cat-item">
									<a href="#">Projects</a> (6)
								</li>
								<li class="cat-item">
									<a href="#">Skin Default</a> (15)
								</li>
								<li class="cat-item">
									<a href="#">Timeline example</a> (16)
								</li>
								<li class="cat-item">
									<a href="#">Uncategorized</a> (19)
								</li>
							</ul>
						</aside>

						<aside class="widgetWrap hrShadow widget widget_meta">
							<h3 class="title">Meta</h3>
							<ul>
								<li>
									<a href="#">Log in</a>
								</li>
								<li>
									<a href="#">Entries RSS</a>
								</li>
								<li>
									<a href="#">Comments RSS</a>
								</li>
							</ul>
						</aside>

						<aside class="widgetWrap hrShadow widget widget_pages">
							<h3 class="title">Pages</h3>
							<ul>
								<li class="page_item">
									<a href="projects_1_column.html">1 column</a>
								</li>
								<li class="page_item">
									<a href="projects_2_columns.html">2 columns</a>
								</li>
								<li class="page_item">
									<a href="projects_3_columns.html">3 columns</a>
								</li>
								<li class="page_item">
									<a href="features_pages_about_us.html">About Us</a>
								</li>
								<li class="page_item">
									<a href="appointment.html">Appointments</a>
								</li>
								<li class="page_item">
									<a href="blog_without_sidebar.html">Blog streampage</a>
								</li>
								<li class="page_item">
									<a href="contacts.html">Contact Us</a>
								</li>
								<li class="page_item">
									<a href="features_pages_faq.html">FAQ</a>
								</li>
								<li class="page_item">
									<a href="projects_4_columns.html">Four columns</a>
								</li>
								<li class="page_item">
									<a href="index.html">Home 1 (HR)</a>
								</li>
								<li class="page_item">
									<a href="index2.html">Home 2 (HR)</a>
								</li>
								<li class="page_item">
									<a href="index3.html">Home 3 (HR)</a>
								</li>
								<li class="page_item">
									<a href="features_shop.html">Homepage E-Commerce</a>
								</li>
								<li class="page_item">
									<a href="features_pages_handyman.html">Our Staff Member’s Page</a>
								</li>
								<li class="page_item">
									<a href="features_pages_page_404.html">Page 404</a>
								</li>
								<li class="page_item">
									<a href="pricing.html">Pricing Tables</a>
								</li>
								<li class="page_item">
									<a href="features_pages_protected_page.html">Protected Page</a>
								</li>
								<li class="page_item">
									<a href="features_pages_service.html">Service / Department Page</a>
								</li>
								<li class="page_item">
									<a href="features_typography.html">Typography</a>
								</li>
								<li class="page_item">
									<a href="features_pages_under_construction.html">Under Construction</a>
								</li>
							</ul>
						</aside>

						<aside class="widgetWrap hrShadow widget widget_recent_comments">
							<h3 class="title">Recent Comments</h3>
							<ul id="recentcomments">
								<li class="recentcomments">
									<span class="comment-author-link">John Doe</span> on <a href="#">Kitchen Design Ideas</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">
										<a href='#' rel='external nofollow' class='url'>Team | House Repair</a>
									</span> 
									on 
									<a href="#">Our Staff Member's Page</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">admin</span> 
									on 
									<a href="#">Luxury Vinyl Tile Installation</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">admin</span>
									on 
									<a href="#">Deck Rescue: Renew Your Deck</a>
								</li>
								<li class="recentcomments">
									<span class="comment-author-link">John Doe</span>
									on 
									<a href="#">High Tech Faucet</a>
								</li>
							</ul>
						</aside>

						<aside class="widgetWrap hrShadow widget widget_recent_entries">
							<h3 class="title">Recent posts</h3>
							<ul>
								<li>
									<a href="#">Living Room Staircases</a>
									<span class="post-date">February 25, 2015</span>
								</li>
								<li>
									<a href="#">Kitchen Design Ideas</a>
									<span class="post-date">February 25, 2015</span>
								</li>
								<li>
									<a href="#">10 Design Lessons</a>
									<span class="post-date">February 25, 2015</span>
								</li>
								<li>
									<a href="#">Exterior Design Ideas</a>
									<span class="post-date">February 25, 2015</span>
								</li>
								<li>
									<a href="#">Interior Design Ideas</a>
									<span class="post-date">February 25, 2015</span>
								</li>
							</ul>
						</aside>

						<aside class="widgetWrap hrShadow widget widget_search">
							<h3 class="title">Search</h3>
							<form role="search" method="get" class="search-form" action="#">
								<input type="text" class="search-field" placeholder="Search &hellip;" value="" name="s" title="Search for:"/>
								<span class="search-button squareButton light ico">
									<a class="search-field icon-search" href="#"></a>
								</span>
							</form>
						</aside>

						<aside class="widgetWrap hrShadow widget widget_tag_cloud">
							<h3 class="title">Tags</h3>
							<div class="tagcloud">
								<a href='#' title='1 topic'>aside post</a>
								<a href='#' title='1 topic'>audio post</a>
								<a href='#'  title='1 topic'>buildings</a>
								<a href='#' title='3 topics'>business</a>
								<a href='#' title='4 topics'>carpentry</a>
								<a href='#' title='1 topic'>chat post</a>
								<a href='#' title='1 topic'>clear</a>
								<a href='#' title='4 topics'>creative</a>
								<a href='#' title='4 topics'>design</a>
								<a href='#'  title='1 topic'>events</a>
								<a href='#' title='6 topics'>exterior</a>
								<a href='#'  title='1 topic'>exterior design</a>
								<a href='#' title='2 topics'>insurance</a>
								<a href='#' title='1 topic'>interior</a>
								<a href='#'  title='2 topics'>kitchens</a>
								<a href='#' title='1 topic'>link post</a>
								<a href='#'  title='4 topics'>living rooms</a>
								<a href='#'  title='2 topics'>plumbing</a>
								<a href='#' title='12 topics'>post formats</a>
								<a href='#' title='5 topics'>posts slider</a>
								<a href='#'  title='5 topics'>press releases</a>
								<a href='#' title='1 topic'>print</a>
								<a href='#'  title='8 topics'>print publications</a>
								<a href='#' title='1 topic'>quote post</a>
								<a href='#'  title='3 topics'>repair</a>
								<a href='#' title='31 topics'>shortcodes</a>
								<a href='#'  title='2 topics'>social</a>
								<a href='#' title='1 topic'>status post</a>
								<a href='#' title='2 topics'>toys</a>
								<a href='#'  title='3 topics'>Trial</a>
								<a href='#' title='2 topics'>typography</a>
								<a href='#'  title='11 topics'>video</a>
								<a href='#' title='1 topic'>video post</a>
								<a href='#' title='3 topics'>gallery</a>
							</div>
						</aside>

						<aside class="widgetWrap widget widget_text">
							<h3 class="title">Custom text</h3>
							<div class="textwidget">
								<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text.</p>
							</div>
						</aside>

					</div>

				</div>
			</div>

@endsection