@extends('layout.frontend.design')

@section('content')



			<div id="topOfPage" class="topTabsWrap">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">Painting</a>
						<span class="breadcrumbs_delimiter"> / </span>
					</div>
					<h3 class="pageTitle h3">Service / Painting</h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<div class="sc_section sc_aligncenter" data-animation="animated fadeInUp">
														<h3>What type of Painting work do you need?</h3>
														<div class="sc_blogger sc_blogger_horizontal style_portfolio3 portfolioWrap">				
															<section class="portfolio isotope folio3col" data-columns="3">
																<article class="isotopeElement hover_Shift odd flt_252">
																	<div class="ih-item colored square effect6 scale_up">
																		<a href="{{ url('/painting/type')}}">
																			<h3 style="padding: 0;font-size:16px;font-weight: bold;">Exterior Painting</h3 >
																			<div class="img">
																				<img alt="Living Room Staircases" src="{{ url('/images/kaboompics.com_Old-building-350x252.jpg')}}">

																			</div>
																			<div class="info">
																				<div class="info-back">
																				<div class="link-wrapper">Exterior Painting</div>
																				</div>
																			</div>
																		</a>
																	</div>
																</article>
																<article class="isotopeElement hover_Shift even flt_253">
																	<div class="ih-item colored square effect6 scale_up">
																		<a href="#">
																			<h3 style="padding: 0;font-size:16px;font-weight: bold;">Interior Painting</h3 >
																			<div class="img">
																				<img alt="Kitchen Design Ideas" src="{{ url('/images/projects-4-350x252.jpg')}}">
																			</div>
																			<div class="info">
																				<div class="info-back">
																					<div class="link-wrapper">Interior Painting</div>
																				</div>
																			</div>
																		</a>
																	</div>
																</article>
																<article class="isotopeElement hover_Shift odd flt_253 flt_252">
																	<div class="ih-item colored square effect6 scale_up">
																		<a href="#">
																			<h3 style="padding: 0;font-size:16px;font-weight: bold;">Other</h3 >
																			<div class="img">
																				<img alt="10 Design Lessons" src="{{ url('/images/kaboompics.com_Old-building-350x252.jpg')}}">
																			</div>
																			<div class="info">
																				<div class="info-back">
																					<div class="link-wrapper">Other</div>
																				</div>
																			</div>
																		</a>
																	</div>
																</article>

															</section>

													</div>
												</div>
											</div>
										</div>
									</div>															
			</div>

@endsection			