@extends('layout.backend.admin_design')
@section('content')

         <!-- Content body -->
        <div class="content-body">
            <!-- Content -->
            <div class="content ">


    <div class="row">
        <div class="col-lg-12 col-md-12">

                    <div class="card">
                        <div class="card-body">
                        <!--     <h6 class="card-title">PROFILE</h6> -->
                            <div class="row">
                                <div class="col-lg-4 col-md-12">
                                    <div class="card border" style="border: 1px solid;box-shadow: 0px 2px 8px 0px rgba(80, 102, 225, 0.45);border-color: antiquewhite;">
                                        <div class="card-body p-3">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <a href="{{url('admin/banners')}}"><h6 class="mb-0">Banner</h6></a>

                                                </div>
                                                <a href="#" class="ml-auto" title="Message"
                                                   data-toggle="tooltip">
                                                    <p class="number"></p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
<!--                                 <div class="col-lg-4 col-md-12">
                                  <div class="card border" style="border: 1px solid;box-shadow: 0px 2px 8px 0px rgba(80, 102, 225, 0.45);border-color: antiquewhite;">
                                        <div class="card-body p-3">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <a href="{{url('admin/thoughts')}}"><h6 class="mb-0">Meterial</h6></a>
                                                </div>
                                                <a href="#" class="ml-auto" title="Message"
                                                   data-toggle="tooltip">
                                                    <p class="number"></p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div> -->
                                <div class="col-lg-4 col-md-12">
                                  <div class="card border" style="border: 1px solid;box-shadow: 0px 2px 8px 0px rgba(80, 102, 225, 0.45);border-color: antiquewhite;">
                                        <div class="card-body p-3">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <a href="#"><h6 class="mb-0">Areas of Interests</h6></a>
                                                </div>
                                                <a href="#" class="ml-auto" title="Message"
                                                   data-toggle="tooltip">
                                                    <p class="number"></p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                  <div class="card border" style="border: 1px solid;box-shadow: 0px 2px 8px 0px rgba(80, 102, 225, 0.45);border-color: antiquewhite;">
                                        <div class="card-body p-3">
                                            <div class="d-flex align-items-center">

                                                <div>
                                                    <a href="{{url('/admin/service')}}"><h6 class="mb-0">Service</h6></a>
                                                </div>
                                                <a href="#" class="ml-auto" title="Message"
                                                   data-toggle="tooltip">
                                                    <p class="number"></p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                  <div class="card border" style="border: 1px solid;box-shadow: 0px 2px 8px 0px rgba(80, 102, 225, 0.45);border-color: antiquewhite;">
                                        <div class="card-body p-3">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <a href="#"><h6 class="mb-0">Calender</h6></a>
                                                </div>
                                                <a href="#" class="ml-auto" title="Message"
                                                   data-toggle="tooltip">
                                                    <p class="number"></p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-12">
                                  <div class="card border" style="border: 1px solid;box-shadow: 0px 2px 8px 0px rgba(80, 102, 225, 0.45);border-color: antiquewhite;">
                                        <div class="card-body p-3">
                                            <div class="d-flex align-items-center">
                                                <div>
                                                    <a href="{{url('/admin/enquiry')}}"><h6 class="mb-0">Enquiry</h6></a>
                                                </div>
                                                <a href="#" class="ml-auto" title="Message"
                                                   data-toggle="tooltip">
                                                    <p class="number"></p>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>






                            </div>
                        </div>
                    </div>

        </div>

    </div>
            </div>  

@endsection
