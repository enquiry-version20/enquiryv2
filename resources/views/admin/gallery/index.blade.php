@extends('layout.backend.admin_design')
@section('content')

        <div class="content-body">
            <!-- Content -->
            <div class="content ">
    <div class="page-header d-md-flex justify-content-between">
        <div>
            <h3>Gallery</h3>
            <nav aria-label="breadcrumb" class="d-flex align-items-start">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/home')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/gallery')}}">Gallery</a>
                    </li> 
                    <li class="breadcrumb-item">
                        <a href="#">View</a>
                    </li>                 
                </ol>
            </nav>
        </div>
        <div class="mt-2 mt-md-0">

            <div class="dropdown ml-2">
                <a href="{{url('/admin/create-gallery')}}" class="btn btn-primary ">Add Gallery</a>

            </div>
        </div>
    </div>


    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="user-list" class="table table-lg" data-page-length='35'>
                            
                            <thead>
                            <tr>
                                <th>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="user-list-select-all">
                                        <label class="custom-control-label" for="user-list-select-all"></label>
                                    </div>
                                </th>
                                <th>SI</th>
                                <th>Image</th>
                                <th>Heading</th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>

                                <th class="text-right">Action</th>
                            </tr>
                            </thead>
                                <tbody>

                            <?php $number=1 ?>        
                            @foreach($gallery as $value)          
                            <tr>
                                <td></td>
                                <td>{{$number}}</td>
                                <td>
                                <figure class="avatar avatar-sm">
                                    <img src="{{url('uploads/gallery/'.$value->image)}}" class="rounded-circle" alt="image">
                                </figure>                                     
                                </td>
                                <td>
                                {{ mb_strimwidth($value->heading,0,60,"...")}}
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>                              
                                </td>
                                
                               
                                <td class="text-right">                                  

                                <a href="{{ url('/admin/delete-gallery/'.$value->id) }}" onclick="return confirm('Are you sure?')"><button type="button" class="btn btn-secondary btn-floating">
                                  <i class="ti-trash"></i>
                                </button> </a>
                                </td>
                            </tr>

                            <?php $number++; ?>
                            @endforeach
                                </tfoot>
                            </table>
            </div>
        </div>
    </div>

            </div>
            <!-- ./ Content -->
            <!-- ./ Content -->


@endsection