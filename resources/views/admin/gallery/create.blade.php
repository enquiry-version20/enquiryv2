@extends('layout.backend.admin_design')
@section('content')

        <!-- Content body -->
        <div class="content-body">
            <!-- Content -->
            <div class="content ">
                
    <div class="page-header d-md-flex justify-content-between">
        <div>
            <h3>Gallery</h3>
            <nav aria-label="breadcrumb" class="d-flex align-items-start">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/home')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/gallery')}}">Gallery</a>
                    </li> 
                    <li class="breadcrumb-item">
                        <a href="#">Add</a>
                    </li>                 
                </ol>
            </nav>
        </div>
        <div class="mt-2 mt-md-0">

        </div>
    </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
    <div class="row">
        <div class="col-md-12">
            <div class="row">

                <div class="col-md-12">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                             aria-labelledby="v-pills-home-tab">


                            <div class="card">
                                <div class="card-body">
                                        <form action="{{url('/admin/store-gallery')}}" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                                          {{ csrf_field() }}
                                          <div class="form-group row">
                                          <div class="form-group col-md-10">
                                            <label for="validationCustom01" class="">Heading</label>
                                            <input type="text" class="form-control" name="heading" id="validationCustom06" required>
                                          </div>
                                          </div>

                                          <div class="form-group row">
                                          <div class="form-group col-md-10">
                                            <div class="custom-file">
                                            <label class="" for="validationCustom12">Gallery Image</label>
                                            <input type="file" class="custom-file-input" id="customFile" name="image" required>
                                              <label class="custom-file-label" for="customFile">Choose file</label>
                                            </div>  
                                          </div>
                                          
                                          </div>

                                          <div class="form-group row">
                                            <div class="col-sm-10">
                                              <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                          </div>

                                        </form>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

            </div>

            <!-- ./ Content -->
            <!-- ./ Content -->


<script type="text/javascript">
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();    
</script>


@endsection