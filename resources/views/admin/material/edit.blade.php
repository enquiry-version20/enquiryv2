@extends('layout.backend.admin_design')
@section('content')

       <div class="content-body">
            <!-- Content -->
            <div class="content ">
                
    <div class="page-header d-md-flex justify-content-between">
        <div>
            <h3>Meterials</h3>
            <nav aria-label="breadcrumb" class="d-flex align-items-start">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/home')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/material')}}">Meterials</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">Edit</a>
                    </li>                    
                </ol>
            </nav>
        </div>
        <div class="mt-2 mt-md-0">


        </div>
    </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
    <div class="row">
        <div class="col-md-12">
            <div class="row">

                <div class="col-md-12">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel"
                             aria-labelledby="v-pills-home-tab">


                            <div class="card">
                                <div class="card-body">
                                        <form action="{{url('/admin/update-material/'.$material->id)}}" method="post" enctype="multipart/form-data" class="needs-validation" novalidate>
                                        {{ csrf_field() }}
                                          <div class="form-group row">
                                          <div class="form-group col-md-6">
                                            <label for="validationCustom01" class="">Material Name</label>
                                            <input type="text" class="form-control" name="material_name" id="validationCustom06" value="{{$material->material_name}}" required>
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label class="" for="validationCustom08">Material Category</label>
                                            <input type="text" class="form-control" name="material_category" id="validationCustom06" value="{{$material->material_category}}" required>
                                          </div>                                            
                                          </div>

                                          <div class="form-group row">
                                          <div class="form-group col-md-6">
                                            <label for="validationCustom01" class="">Material Type</label>
                                            <input type="text" class="form-control" name="material_type" id="validationCustom06" value="{{$material->material_type}}" required>
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="validationCustom01" class="">Prize</label>
                                            <input type="text" class="form-control" name="prize" id="validationCustom06" value="{{$material->prize}}" required>
                                          </div>                                           
                                          </div>

                                          <div class="form-group row">
                                          <div class="form-group col-md-6">
                                            <label for="validationCustom01" class="">Date</label>
                                            <input type="date" class="form-control" name="date" id="validationCustom06" value="{{$material->date}}" required>
                                          </div>
                                          <div class="form-group col-md-6">
                                            <label for="validationCustom01" class="">Stock</label>
                                            <input type="number" class="form-control" name="stock" id="validationCustom06" value="{{$material->stock}}" required>
                                          </div>                                           
                                          </div>

                                          <div class="form-group row">
                                          <div class="form-group col-md-6">
                                            <label class="" for="validationCustom08">Details</label>
                                              <textarea class="form-control" id="editor1" name="details">
                                              {{$material->details}}
                                              </textarea>
                                          </div>  
                                          <div class="form-group col-md-6">
                                            <label class="" for="validationCustom08">Description</label>
                                              <textarea class="form-control" id="editor2" name="description">
                                              {{$material->description}}
                                              </textarea>
                                          </div>                                     
                                          </div>

                                          <div class="form-group row">
                                          <div class="form-group col-md-6">
                                            <div class="custom-file">
                                            <label class="" for="validationCustom12">Material Image</label>
                                            <input type="file" class="custom-file-input" id="customFile" name="meterial_image" value="{{$material->meterial_image}}">
                                              <label class="custom-file-label" for="customFile">Choose file</label>
                                              
                                            </div>  
                                          </div>
                                          <div class="form-group col-md-6">
                                            <div class="custom-file">
                                            <label class="" for="validationCustom12">Thumpnail</label>
                                            <input type="file" class="custom-file-input" id="customFile" name="thumpnail" value="{{$material->thumpnail}}">
                                              <label class="custom-file-label" for="customFile">Choose file</label>
                                              
                                            </div>  
                                          </div>                                          
                                          </div>
                                          @if(is_null($material->meterial_image)&&($material->thumpnail))
                                          <div>No Image </div>
                                          @else
                                          <div class="">
                                              <img src="{{url('/'.$material->meterial_image)}}" style="margin-bottom: 10px;" alt="" width="122" height="72">
                                          </div>
                                          <div class="">
                                              <img src="{{url('/'.$material->thumpnail)}}" style="margin-bottom: 10px;" alt="" width="122" height="72">
                                          </div>

                                          @endif
                                          <div class="form-group row">
                                            <div class="col-sm-10">
                                              <input type="hidden" name="id" id="id" value="{{$material->id}}"/>
                                              <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                          </div>

                                        </form>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

            </div>
            <!-- ./ Content -->
            <!-- ./ Content -->

<script src="{{url('backend/vendors/ckeditor5/ckeditor.js')}}"></script>   
   
<script type="text/javascript">
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();    
</script>


    <script>
        ClassicEditor
            .create( document.querySelector( '#editor1' ), {
                toolbar: [ 'heading', '|', 'bold', 'italic', 'link', 'bulletedList', 'numberedList', 'blockQuote' ],
                heading: {
                    options: [
                        { model: 'paragraph', title: 'Paragraph', class: 'ck-heading_paragraph' },
                        { model: 'heading1', view: 'h1', title: 'Heading 1', class: 'ck-heading_heading1' },
                        { model: 'heading2', view: 'h2', title: 'Heading 2', class: 'ck-heading_heading2' }
                    ]
                }
            })
    </script>


@endsection