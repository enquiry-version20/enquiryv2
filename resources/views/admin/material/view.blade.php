@extends('layout.backend.admin_design')
@section('content')

        <!-- Content body -->
<div class="content-body">
<!-- Content -->
<div class="content ">
    <div class="page-header d-md-flex justify-content-between">
        <div>
            <h3>Meterials</h3>
            <nav aria-label="breadcrumb" class="d-flex align-items-start">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/home')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">Material Details</a>
                    </li>
                </ol>
            </nav>
        </div>

    </div>



        <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table  table-hover">
                          
                            <tbody>
                            <tr class="text-center bg-primary">
                            <td class="text-left">Material Details</td><td ></td>  <td ></td>                           
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Material Name</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span></td>
                                <td class="text-left">{{ucfirst($material->material_name)}}</td>                          
                            </tr>

                            <tr class="text-center">
                                <td class="text-left">Material Category</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span></td>
                                <td class="text-left">{{ucfirst($material->material_category)}}
                                </td>                             
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Material Type</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating"> - </span></td>
                                <td class="text-left">{{ucfirst($material->material_type)}}</td>                             
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Details</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span></td>
                                <td class="text-left">{{ucfirst(strip_tags($material->details))}} 
                            </td>                             
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Prize</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating"> - </span></td>
                                <td class="text-left">{{$material->prize}}</td>                             
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Description</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span></td>
                                <td class="text-left">{{ucfirst(strip_tags($material->description))}}</td>                             
                            </tr>         
                            <tr class="text-center">
                                <td class="text-left">Stock</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span>
                                </td>
                                <td class="text-left">{{$material->stock}}</td>             
                            </tr> 

                            <tr class="text-center">
                                <td class="text-left">Image</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span>
                                </td>
                                <td class="text-left"><img src="{{url('/'.$material->meterial_image)}}" style="margin-bottom: 10px;" alt="" width="122" height="72"></td>             
                            </tr>                          

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>                     
            <!-- ./ Content -->
 @endsection