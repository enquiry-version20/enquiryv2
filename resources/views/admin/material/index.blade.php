@extends('layout.backend.admin_design')
@section('content')

        <div class="content-body">
            <!-- Content -->
            <div class="content ">
    <div class="page-header d-md-flex justify-content-between">
        <div>
            <h4>Services</h4>
            <nav aria-label="breadcrumb" class="d-flex align-items-start">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/home')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/service')}}">Services</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">View</a>
                    </li>                    
                </ol>
            </nav>
        </div>
        <div class="mt-2 mt-md-0">
            <div class="dropdown ml-2">
                <a href="{{url('/admin/create-service')}}" class="btn btn-primary" >Add Service</a>
            </div>
        </div>
    </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif

    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="user-list" class="table table-lg" data-page-length='35'>
                            
                            <thead>
                            <tr>
                                <th>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="user-list-select-all">
                                        <label class="custom-control-label" for="user-list-select-all"></label>
                                    </div>
                                </th>
                                <th>SI</th>
                                <th>Image</th>
                                <th></th>
                                <th>Name</th>
                                <th></th>
                                <th>Description</th>
                                <th></th>
                                

                                <th class="text-right">Action</th>
                            </tr>
                            </thead>
                                <tbody>

                            <?php $number=1 ?>        
                            @foreach($meterial as $value)          
                            <tr>
                                <td></td>
                                <td>{{$number}}</td>
                                <td>
                                @if(is_null($value->meterial_image) )
                                <span>No image</span>
                                @else
                                <figure class="avatar avatar-sm">
                                    <img src="{{url('/'.$value->meterial_image)}}" class="rounded-circle" alt="image">
                                </figure>  
                                @endif
                                </td>
                                <td>
                                
                                </td>
                                <td>{{$value->service_name}} </td>
                                <td></td>
                                <td>{{$value->description}}                             
                                </td>
                                <td></td>
                                
                               
                                <td class="text-right">  
                                <a href="{{url('/admin/view-material/'.$value->id)}}" ><button type="button" class="btn btn-primary btn-floating">
                                  <i class="ti-eye"></i>
                                </button> </a>                                                                
                                <a href="{{ url('/admin/edit-material/'.$value->id) }}" ><button type="button" class="btn btn-success btn-floating">
                                  <i class="ti-pencil"></i>
                                </button> </a>    
                                <a href="{{ url('/admin/delete-material/'.$value->id) }}" onclick="return confirm('Are you sure?')"><button type="button" class="btn btn-secondary btn-floating">
                                  <i class="ti-trash"></i>
                                </button> </a>
                                </td>
                            </tr>

                            <?php $number++; ?>
                            @endforeach
                                </tfoot>
                            </table>
            </div>
        </div>
    </div>

            </div>
            <!-- ./ Content -->
            <!-- ./ Content -->


@endsection