@extends('layouts.adminLayout.admin_design')
@section('content')

<div class="container mt-8">
  <!-- ========= -->
  <div class="container mt--8 pb-5">
    <div class="row justify-content-center">
      <div class="col-lg-5 col-md-7">
        <div class="card  shadow border-0">

          <div class="card-body px-lg-5 py-lg-5">
            <div class="text-center text-muted mb-4">
              <img src="{{ url('assets/adminlayout/img/logo.png')}}"
                class="d-inline-block align-top img-responsive wd110" alt="">
              <h6>Reset Password</h6>
            </div>
            <br>
            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
            @endif
            <form action="{{ url('/admin/reset-password-submit') }}" method="post" name="frmreset" id="frmreset">
              {{ csrf_field() }}
              <div class="form-group">
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-key blu-o" aria-hidden="true"></i></span>
                  </div>
                  <input class="form-control" name="password" id="password" required="true" placeholder="Password"
                    type="password">
                </div>
              </div>
              <div class="form-group">
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-key blu-o" aria-hidden="true"></i></span>
                  </div>
                  <input class="form-control" name="confim_password" id="confim_password" required="true"
                    placeholder=" Confirm Password" type="password">
                  <br />
                </div>
              </div>
              <div>
                <span id="labelpassword" style="color: red;"></span>
              </div>
              <div class="text-center">
                <button type="submit" class="btn btn-primary my-4 w-5 blu b-rad log" name="btnreset">Reset
                  password</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- ========    -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
  $(function () {

    $("#frmreset").submit(function () {

      var password = $('#password').val();
      var confim_password = $('#confim_password').val();

      if (password != confim_password) {

        $('#labelpassword').html("Password mismatch");
        $('#confim_password').text('');
        return false;

      }
    });

  });
</script>
@endsection