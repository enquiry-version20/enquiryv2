<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Build Advisor</title>

  <link href="{{ url('assets/backend/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js">
  <link rel="stylesheet" type="text/css"
    href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <link href="{{url('assets/backend/css/style.css')}}" rel="stylesheet">

</head>

<body style="background: linear-gradient(87deg, #005F8B 0, #825ee4 100%) !important;">


  <!--  head & nav  closed -->
  <!-- content -->



  <div class="container" style="margin-top: 40px;">
    <!-- ========= -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card  shadow border-0">

            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                <img src="{{ url('/images/logo/logo.png')}}" style=" width:15%;">
                <!-- <p><b class="blu-o" style="font-size: 14px;">Center For Excellence in Smart City Technologies</b></p> -->
                <br>
                <br>
                <h6><b style="text-transform: uppercase;letter-spacing: 6px;">login-In</b></h6>
              </div>
              <form method="POST" action="{{ url('/admin/authenticate') }}">
                @csrf
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-envelope blu-o" aria-hidden="true"></i></span>
                  </div>
                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                    value="{{ old('email') }}" placeholder="Email" required="true" autocomplete="email"
                    style="width: 200px;" autofocus>
                  @error('email')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                <div class="input-group input-group-alternative mt-3">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="fa fa-key blu-o" aria-hidden="true"></i></span>
                  </div>
                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                    name="password" style="width: 200px;" required="true" placeholder="Password"
                    autocomplete="current-password">
                  @error('password')
                  <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                </div>
                <!-- <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" 
                        class="form-control @error('email') is-invalid @enderror" 
                        name="email" value="{{ old('email') }}" placeholder="Email"
                        required="true" autocomplete="email" style="width: 200px;" autofocus>

                        @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div> -->
                <!-- 
  
                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" 
                        name="password" style="width: 200px;" required="true" placeholder="Password" autocomplete="current-password">
                        
                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div> -->

                <div class="form-group row">
                  <div class="col-md-6 offset-md-4 m-0">
                    <div class="form-check">
                      <!-- <input class="form-check-input" type="checkbox" name="remember" id="remember"
                        {{ old('remember') ? 'checked' : '' }}> -->

                      <label class="form-check-label" for="remember">
                        <!-- {{ __('Remember Me') }} -->
                      </label>
                    </div>
                  </div>
                </div>

                <div class="form-group row mb-0">
                  <center>
                    <div class="col-md-12 " style="margin-left:82px !important;">
                      <button type="submit" class="btn btn-primary   blu b-rad ">
                        {{ __('Login') }}
                      </button>
                      <br>
                      @if (Route::has('password.request'))
                      <a class="btn btn-link" href="{{ route('password.request') }}">
                        <!-- {{ __('Forgot Your Password?') }} -->
                      </a>
                      @endif
                    </div>
                  </center>

                </div>
              </form>
              <!-- <div class="col-12">
              <a href="#" class="text-light neon-o" style="margin-left: 106px;"><small>Forgot password?</small></a>
            </div> -->


            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- ========    -->
  </div>
  <!-- Bootstrap core JavaScript -->
  <script src="{{ url('assets/backend/vendor/jquery/jquery.min.js')}}"></script>
  <script src="{{ url('assets/backend/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

</body>

</html>