@extends('layout.backend.admin_design')
@section('content')

        <div class="content-body">
            <!-- Content -->
            <div class="content ">
    <div class="page-header d-md-flex justify-content-between">
        <div>
            <h4>Services</h4>
            <nav aria-label="breadcrumb" class="d-flex align-items-start">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/home')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/service')}}">Services</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">View</a>
                    </li>                    
                </ol>
            </nav>
        </div>
        <div class="mt-2 mt-md-0">

            <div class="dropdown ml-2">
                <a href="{{url('/admin/create-service')}}" class="btn btn-primary" >Add Service</a>

            </div>
        </div>
    </div>
  @if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
    </ul>
  </div>
  @endif
  @if(Session::has('flash_message_error'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{!! session('flash_message_error') !!}</strong>
  </div>
  @endif
  @if(Session::has('flash_message_success'))
  <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <strong>{!! session('flash_message_success') !!}</strong>
  </div>
  @endif
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table id="user-list" class="table table-lg" data-page-length='35'>
                            
                            <thead>
                            <tr>
                                <th>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="user-list-select-all">
                                        <label class="custom-control-label" for="user-list-select-all"></label>
                                    </div>
                                </th>
                                <th>SI</th>
                                <th>Image</th>
                                <th></th>
                                <th>Name</th>
                                <th></th>
                                <th>Details</th>
                                <th></th>
                                

                                <th class="text-right">Action</th>
                            </tr>
                            </thead>
                                <tbody>

                            <?php $number=1 ?>        
                            @foreach($service as $value)          
                            <tr>
                                <td></td>
                                <td>{{$number}}</td>
                                <td>
                                @if(is_null($value->service_image) )
                                <span>No image</span>
                                @else
                                <figure class="avatar avatar-sm">
                                    <img src="{{url('/'.$value->service_image)}}" class="rounded-circle" alt="image">
                                </figure>  
                                @endif
                                </td>
                                <td>
                                
                                </td>
                                <td>{{$value->service_name}} </td>
                                <td></td>
                                <!-- <td>{{mb_strimwidth(strip_tags($value->description),0,25,"...")}}                              
                                </td> -->
                               <td>{{$value->details}} </td>
                                <td></td>
                                
                               
                                <td class="text-right">  
                                <a href="{{url('/admin/view-service/'.$value->id)}}" ><button type="button" class="btn btn-primary btn-floating">
                                  <i class="ti-eye"></i>
                                </button> </a>                                                                
                                <a href="{{ url('/admin/service-category/'.$value->id) }}" ><button type="button" class="btn btn-success btn-floating" alt="Add Category">
                                  <i class="ti-plus"></i>
                                </button> </a>    
                                <a href="{{ url('/admin/service-edit/'.$value->id) }}" ><button type="button" class="btn btn-secondary btn-floating" alt="Add Category">
                                  <i class="ti-pencil"></i>
                                </button> </a> 
                                <a href="{{ url('/admin/delete-service/'.$value->id) }}" onclick="return confirm('Are you sure?')"><button type="button" class="btn btn-danger btn-floating">
                                  <i class="ti-trash"></i>
                                </button> </a>
                                </td>
                            </tr>

                            <?php $number++; ?>
                            @endforeach
                                </tfoot>
                            </table>
            </div>
        </div>
    </div>

            </div>
            <!-- ./ Content -->
            <!-- ./ Content -->


@endsection