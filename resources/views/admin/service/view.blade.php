@extends('layout.backend.admin_design')
@section('content')

        <!-- Content body -->
<div class="content-body">
<!-- Content -->
<div class="content ">
    <div class="page-header d-md-flex justify-content-between">
        <div>
            <h4>Services</h4>
            <nav aria-label="breadcrumb" class="d-flex align-items-start">
                <ol class="breadcrumb">
                    <!-- <li class="breadcrumb-item">
                        <a href="{{url('/admin/home')}}">Home</a>
                    </li> -->
                    <li class="breadcrumb-item">
                        <!-- <a href="{{url('/admin/service')}}">Services</a> -->
                        @if($service->details)
                        {{$service->details}}
                        @else
                        {{$service->service_name}}
                        @endif
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">View</a>
                    </li>                    
                </ol>
            </nav>
        </div>

    </div>



        <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table  table-hover">
                          
                            <tbody>
                            <tr class="text-center bg-primary">
                            <td class="text-left">Service Details</td><td ></td>  <td ></td>                           
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Service Name</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span></td>
                                <td class="text-left">{{ucfirst($service->service_name)}}</td>                          
                            </tr>
                            @if(!empty($parent_service))
                            <tr class="text-center">
                                <td class="text-left">Parent Service</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span></td>
                                <td class="text-left">{{ucfirst($parent_service->service_name)}}</td>                          
                            </tr>
                            @endif
                            <tr class="text-center">
                                <td class="text-left">Description</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating"> - </span></td>
                                <td class="text-left">{{ucfirst(strip_tags($service->description))}}</td>                             
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Image</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span>
                                </td>
                                <td class="text-left"><img src="{{url('/'.$service->service_image)}}" style="margin-bottom: 10px;" alt="" width="122" height="72"></td>             
                            </tr>                          

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>                     
            <!-- ./ Content -->
 @endsection