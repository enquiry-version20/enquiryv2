@extends('layout.backend.admin_design')
@section('content')

        <!-- Content body -->
<div class="content-body">
<!-- Content -->
<div class="content ">
    <div class="page-header d-md-flex justify-content-between">
        <div>
            <h4>Enquiry</h4>
            <nav aria-label="breadcrumb" class="d-flex align-items-start">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/home')}}">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="{{url('/admin/enquiry')}}">Enquiry</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href="#">View</a>
                    </li>                    
                </ol>
            </nav>
        </div>

    </div>



        <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table  table-hover">
                          
                            <tbody>
                            <tr class="text-center bg-primary">
                            <td class="text-left">Enquiry Details</td><td ></td>  <td ></td>                           
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Name</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span></td>
                                <td class="text-left">{{ucwords($service_one->full_name)}}</td>                          
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Email</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span></td>
                                <td class="text-left">{{$service_one->email}}</td>                          
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Phone</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span></td>
                                <td class="text-left">{{$service_one->phone_no}}</td>                          
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Pincode</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span></td>
                                <td class="text-left">{{$service_one->pincode}}</td>                          
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">City / District / State</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span></td>
                                <td class="text-left">{{ucfirst($service_one->city)}} / {{ucfirst($service_one->district)}} / {{ucfirst($service_one->state)}}</td>                          
                            </tr>
                            <tr class="text-center">
                                <td class="text-left">Service Requests</td>
                                <td><span class="icon-block mr-3 bg-primary icon-block-xs icon-block-floating text-left"> - </span></td>
                                <td class="text-left">{{ucfirst($service_four->service_name)}}/{{ucfirst($service_three->service_name)}}  / {{ucfirst($service_one->service_name)}}</td>                          
                            </tr>
                     

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>                     
            <!-- ./ Content -->
 @endsection