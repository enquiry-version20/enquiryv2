
@extends('layout.frontend.design')

@section('content')


			<div id="topOfPage" class="topTabsWrap">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">All Posts</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<span class="current">Handyman Personal Page</span>
					</div>
					<h3 class="pageTitle h3">Handyman Personal Page</h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
				<div class="content">
					<div class="itemscope" itemscope itemtype="http://schema.org/Article">
						<section class="post postAlter no_margin page">
							<article class="post_content">
								<div class="post_text_area" itemprop="articleBody">

									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<h5 class="sc_title sc_aligncenter sc_title_regular">About The Handyman</h5>
<!-- 													<div class="sc_subsection bg_tint_none sc_aligncenter margin_bottom_small">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<p>We will help you build strong online business by creating a professional website which best suits your needs<br/>
																and the needs of your target audience.</p>
															</div>
														</div>
													</div> -->
													<br>
													<div class="columnsWrap sc_columns no_margin sc_columns_count_3">
														<div class="columns1_3 sc_column_item sc_column_item_2 even">
															<div class="sc_team no_padding margin_right_small">
																<div class="sc_columns columnsWrap">
																	<div class="columns1_1" data-animation="animated fadeInUp">
																		<div class="sc_team_item sc_team_item_1 odd first">
																			<div class="sc_team_item_avatar">
																				<img alt="03.jpg" src="images/03-370x370.jpg">
																				<div class="sc_team_item_description"> Ased do eiusmod tempor incididunt ut labore et dolore magna&lt;br /&gt;
																				aliqua.</div>
																			</div>
																			<div class="sc_team_item_info">
																				<h3 class="sc_team_item_title">
																				<a href="#">Earl Stone</a>
																				</h3>
																				<div class="sc_team_item_position theme_accent2">Construction Worker</div>
																				<ul class="sc_team_item_socials">
																					<li>
																						<a href="#" class="social_icons icon-facebook" target="_blank">
																						</a>
																					</li>
																					<li>
																						<a href="#" class="social_icons icon-twitter" target="_blank">
																						</a>
																					</li>
																					<li>
																						<a href="#" class="social_icons icon-gplus" target="_blank">
																						</a>
																					</li>
																					<li>
																						<a href="#" class="social_icons icon-dribbble" target="_blank">
																						</a>
																					</li>
																				</ul>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>



														<div class="columns1_3 sc_column_item sc_column_item_3 odd">
															<div class="sc_accordion sc_accordion_style_1 no_margin" data-active="0">
																<div class="sc_accordion_item odd first">
																	<h4 class="sc_accordion_title">Lorem ipsum dolor sit amet</h4>
																	<div class="sc_accordion_content"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum. </div>
																</div>
																<div class="sc_accordion_item even">
																	<h4 class="sc_accordion_title">Consectetur adipisicing elit</h4>
																	<div class="sc_accordion_content"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum. </div>
																</div>
																<div class="sc_accordion_item odd">
																	<h4 class="sc_accordion_title">Sed do eiusmod tempor</h4>
																	<div class="sc_accordion_content"> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum. </div>
																</div>
															</div>
														</div>

														<div class="columns1_3 sc_column_item sc_column_item_1 odd first">
															<div class="sc_skills sc_skills_bar sc_skills_horizontal" data-type="bar" data-subtitle="Skills" data-dir="horizontal">
																<div class="sc_skills_info">Plumbing</div>
																<div class="sc_skills_info">98%</div>
																<div class="sc_skills_item sc_skills_style_1 odd first">
																	<div class="sc_skills_count">
																		<div class="sc_skills_total" data-start="0" data-stop="98" data-step="1" data-max="100" data-speed="19" data-duration="1862" data-ed="%">0%</div>
																	</div>
																</div>
																<div class="sc_skills_info">Carpentry</div>
																<div class="sc_skills_info">66%</div>
																<div class="sc_skills_item sc_skills_style_1 even">
																	<div class="sc_skills_count">
																		<div class="sc_skills_total" data-start="0" data-stop="66" data-step="1" data-max="100" data-speed="38" data-duration="2508" data-ed="%">0%</div>
																	</div>
																</div>
																<div class="sc_skills_info">Tiling</div>
																<div class="sc_skills_info">87%</div>
																<div class="sc_skills_item sc_skills_style_1 odd">
																	<div class="sc_skills_count">
																		<div class="sc_skills_total" data-start="0" data-stop="87" data-step="1" data-max="100" data-speed="40" data-duration="3480" data-ed="%">0%</div>
																	</div>
																</div>
																<div class="sc_skills_info">Electrical</div>
																<div class="sc_skills_info">82%</div>
																<div class="sc_skills_item sc_skills_style_1 even">
																	<div class="sc_skills_count">
																		<div class="sc_skills_total" data-start="0" data-stop="82" data-step="1" data-max="100" data-speed="17" data-duration="1394" data-ed="%">0%</div>
																	</div>
																</div>
																<div class="sc_skills_info">Other</div>
																<div class="sc_skills_info">76%</div>
																<div class="sc_skills_item sc_skills_style_1 odd">
																	<div class="sc_skills_count">
																		<div class="sc_skills_total" data-start="0" data-stop="76" data-step="1" data-max="100" data-speed="30" data-duration="2280" data-ed="%">0%</div>
																	</div>
																</div>
															</div>
														</div>



													</div>
												</div>
												<div class="sc_line sc_line_style_solid no_margin"></div>
											</div>
										</div>
									</div>

									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_section grey_section">
													<div class="sc_content main" data-animation="animated fadeInUp">
														<h3 class="sc_undertitle sc_title_regular">ENQUIRY UPDATE</h3>
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<p>Please fill in the form below to request a free quote and one of our experts will contact you to arrange a meeting. This visit<br/>
																will serve to evaluate your needs and give you a personalized and competitive quotation. </p>
															</div>
														</div>
														<div class="sc_contact_form sc_contact_form_contact_3 margin_top_small">
															<form data-formtype="contact_3" method="post" action="include/sendmail.php">
																<div class="columnsWrap sc_columns sc_columns_count_4 no_padding">
																	<div class="columns1_4 sc_column_item sc_column_item_1 odd first">
																		<input id="sc_contact_form_username" type="text" name="username" placeholder="Name">
																		<input id="sc_contact_form_phone" type="text" name="phone" placeholder="Phone (Ex.0123456789)">
																	</div>
																	<div class="columns1_4 sc_column_item sc_column_item_2 even">
																		<input id="sc_contact_form_email" type="text" name="email" placeholder="E-mail">
																		<input id="sc_contact_form_subj" type="text" name="subject" placeholder="Subject">
																	</div>
																	<div class="columns2_4 sc_column_item sc_column_item_3 odd span_2">
																		<textarea id="sc_contact_form_message" class="textAreaSize" name="message" placeholder="Message"></textarea>
																		<div class="sc_contact_form_button">
																			<div class="sc_button sc_button_style_dark sc_button_size_huge squareButton dark huge">
																				<a href="#" class="sc_contact_form_submit">Submit</a>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="result sc_infobox"></div>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main no_padding_bottom">
													<h1 class="sc_title sc_title_regular sc_aligncenter" data-animation="animated fadeInUp">Professional certification</h1>
													<div class="sc_section sc_undertitle_style_2 bg_tint_none sc_aligncenter" data-animation="animated fadeInUp">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna<br/>
																aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi.</p>
															</div>
														</div>
													</div>
												</div>
												<div class="sc_content main no_padding_top">
													<div id="sc_blogger_442037646" class="sc_blogger sc_blogger_horizontal style_portfolio3 portfolioWrap" data-animation="animated fadeInUp">
														<section class="portfolio isotopeNOanim folio3col" data-columns="3">
															<article class="isotopeElement post_format_standard hover_Dir odd">
																<div class="hover hoverDirShow">
																	<div class="thumb">
																		<img alt="Reed&#8217;s Certificate" src="images/certificates-350x252.jpg">
																	</div>
																	<div class="folioShowBlock">
																		<div class="folioContentAfter">
																			<h4>
																			<a href="#">Reed&#8217;s Certificate</a>
																			</h4>
																			<p>
																			Curabitur suscipit est ut orci fringilla condimentum. Praesent id pretium velit. Nunc... </p>
																		</div>
																	</div>
																</div>
															</article>
															<article class="isotopeElement post_format_standard hover_Dir even">
																<div class="hover hoverDirShow">
																	<div class="thumb">
																		<img alt="Stone&#8217;s Certificate" src="images/certificate-2-350x252.jpg">
																	</div>
																	<div class="folioShowBlock">
																		<div class="folioContentAfter">
																			<h4>
																			<a href="#">Stone&#8217;s Certificate</a>
																			</h4>
																			<p>
																			Curabitur suscipit est ut orci fringilla condimentum. Praesent id pretium velit. Nunc... </p>
																		</div>
																	</div>
																</div>
															</article>
															<article class="isotopeElement post_format_standard hover_Dir odd">
																<div class="hover hoverDirShow">
																	<div class="thumb">
																		<img alt="Day&#8217;s Certificate" src="images/Certificate-Guilloche-security-horizontal-350x252.jpg">
																	</div>
																	<div class="folioShowBlock">
																		<div class="folioContentAfter">
																			<h4>
																			<a href="#">Day&#8217;s Certificate</a>
																			</h4>
																			<p>
																			Curabitur suscipit est ut orci fringilla condimentum. Praesent id pretium velit. Nunc... </p>
																		</div>
																	</div>
																</div>
															</article>
															<article class="isotopeElement post_format_standard hover_Dir even">
																<div class="hover hoverDirShow">
																	<div class="thumb">
																		<img alt="Doe&#8217;s Certificate" src="images/Certificate-Set-350x252.jpg">
																	</div>
																	<div class="folioShowBlock">
																		<div class="folioContentAfter">
																			<h4>
																			<a href="#">Doe&#8217;s Certificate</a>
																			</h4>
																			<p>
																			Curabitur suscipit est ut orci fringilla condimentum. Praesent id pretium velit. Nunc... </p>
																		</div>
																	</div>
																</div>
															</article>
															<article class="isotopeElement post_format_standard hover_Dir odd">
																<div class="hover hoverDirShow">
																	<div class="thumb">
																		<img alt="Smith&#8217;s Certificate" src="images/certificate-350x252.jpg">
																	</div>
																	<div class="folioShowBlock">
																		<div class="folioContentAfter">
																			<h4>
																			<a href="#">Smith&#8217;s Certificate</a>
																			</h4>
																			<p>
																			Curabitur suscipit est ut orci fringilla condimentum. Praesent id pretium velit. Nunc... </p>
																		</div>
																	</div>
																</div>
															</article>
														</section>
													</div>
													<div class="columnsWrap sc_columns sc_columns_count_1">
														<div class="columns1_1 sc_column_item sc_column_item_1 odd first sc_center">
															<div class="sc_button sc_button_style_global sc_button_size_big squareButton global big">
																<a href="#" class="">More Projects</a>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tagsWrap"></div>
								</div>  
							</article>  
						</section>  
					</div>    
				 
				</div>  
			</div>


@endsection			