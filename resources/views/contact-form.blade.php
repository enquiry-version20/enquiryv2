

@extends('layout.frontend.design')
@section('content')


			<div id="topOfPage" class="topTabsWrap" style="border:4px solid white;">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="{{ url('/')}}">{{$subcat_sname2}}</a>
						<span class="breadcrumbs_delimiter"> /</span>
						<a class="all" href="{{ url('/main-services/'.$subcat_sparent1)}}">{{$subcat_sname1}}</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="{{ url('/main-sub-services-flag/'.$service_id)}}">{{$subcat_sname3}}</a>
					</div>
					<h3 class="pageTitle h3"><a class="home" href="{{ url('/')}}">{{$subcat_sname2}}</a> / <a class="all" href="{{ url('/main-services/'.$subcat_sparent1)}}">{{$subcat_sname1}}</a>/<a class="all" href="{{ url('/main-sub-services-flag/'.$service_id)}}">{{$subcat_sname3}}</a></h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar" style="border:4px solid white;">
				<div class="content">
					<div class="itemscope" itemscope itemtype="http://schema.org/Article">
						<section class="post post_format_standard postAlter no_margin page type-page status-publish hentry">
							<article class="post_content">
								<div class="post_text_area" itemprop="articleBody">

									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_section grey_section">
													<div class="sc_content main" data-animation="animated fadeInUp">
														<h3 class="sc_undertitle sc_title_regular" style="padding: 0">Where is your project located?</h3><hr>

														<div class="sc_contact_form sc_contact_form_contact_3 margin_top_small" id="first_contact">
															<form  method="post" action="{{ url('/contact-form-submit')}}" id="frmcontact">
																{{ csrf_field() }}
																<div class="columnsWrap sc_columns sc_columns_count_3 no_padding" 
																style="margin-bottom: 15px;">

																	<div class="columns1_2 sc_column_item sc_column_item_1 odd first">
																		<input id="full_name" type="text" name="full_name" placeholder="Name" required>
																	</div>	

																</div>	
																<div class="columnsWrap sc_columns sc_columns_count_2 no_padding" 
																style="margin-bottom: 15px;">
																	<div class="columns1_4 sc_column_item sc_column_item_1 odd first">
																		<input id="email" type="text" name="email" placeholder="Email" required>
																		
																	</div>

																	<div class="columns1_4 sc_column_item sc_column_item_1 odd first">	
																		<input id="phone_no" type="text" name="phone_no" placeholder="Phone" required>
																		
																	</div>
																	
																</div>
																<div class="columnsWrap sc_columns sc_columns_count_3 no_padding" 
																style="margin-bottom: 15px;">	
																	<div class="columns1_4 sc_column_item sc_column_item_1 odd first">	
																		<input id="pin_code" type="text" name="pin_code" placeholder="Pin Code" required>	

																	</div>
																	<div class="columns1_4 sc_column_item sc_column_item_2 even">
																		<input id="city" type="text" name="city" placeholder="City" required>
																	</div>	
																	
																</div>	
																<div class="columnsWrap sc_columns sc_columns_count_3 no_padding" 
																style="margin-bottom: 4px;">	
																	<div class="columns1_4 sc_column_item sc_column_item_2 even">
																		<input id="district" type="text" name="district" placeholder="District" required>
																		
																	</div>
																	<div class="columns1_4 sc_column_item sc_column_item_2 even">
																		<input id="state" type="text" name="state" placeholder="State" required>
																	</div>	

																</div>
																<input  type="hidden" id="service_id" name="service_id" value="{{$service_id}}">
																
																<div class="result sc_infobox"></div>
																<a href="{{ url('/main-sub-services-flag/'.$service_id)}}">
																		<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium" style="margin: 0;padding-top: 15px;">
																		<button type="button" >Previous</button>	
																		</div>
																</a>		
																		<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium" style="margin: 0;padding-top: 15px;">
																		<button type="submit" id="btnNext" name="btnNext">Submit</button>	
																		</div>

															</form>
														</div>
														
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="tagsWrap"></div>
								</div>  
							</article>  
						</section>  
					</div>    
				 
				</div>  
			</div>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript">

// jQuery(document).ready(function(){

// 	$('#frmcontact').on('submit', function(e){
// 		e.preventDefault();
			
//            $.ajaxSetup({
//               headers: {
//                   'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
//               }
//           });
//            jQuery.ajax({
//               url: "{{ url('/contact-form-submit') }}",
//               method: 'POST',
			 
           
//               data: {
//                  _token: '{{csrf_token()}}', 
//                  city: jQuery('#city').val(),
// 				 state: jQuery('#state').val(),
// 				 district: jQuery('#district').val(),
// 				 pin_code: jQuery('#pin_code').val(),
// 				 service_id: jQuery('#service_id').val(),
// 				 full_name: jQuery('#full_name').val(),
// 				 email: jQuery('#email').val(),
// 				 phone_no: jQuery('#phone_no').val(),

//               },
//               success: function(result){
                 
// 				window.location.href="/enquiry-finish"
// 				// window.location.href =result.url;
				
//               }});
//            });
//         });

</script>
@endsection			