			<div class="footerContentWrap">
				<footer class="footerWrap footerStyleDark">
					<div class="main footerWidget widget_area">
						<div class="columnsWrap">
							<aside class="columns1_4 widgetWrap widget widget_nav_menu">
								<h3 class="title">About</h3>
								<div class="menu-services-container">
									<ul id="menu-services" class="menu">
										<li id="menu-item-3526" class="menu-item menu-item-3526">
											<a href="#">BUILD ADVISOR</a>
										</li>
										<li class="menu-item">
											<a href="#">Kerala</a>
										</li>
										<li class="menu-item">
											<a href="#">+91 0484 2356586</a>
										</li>

									</ul>
								</div>
							</aside>

							<aside class="columns1_4 widgetWrap widget widget_nav_menu">
								<h3 class="title">Services</h3>
								<div class="menu-services-container">
									<ul id="menu-services" class="menu">
										<li id="menu-item-3526" class="menu-item menu-item-3526">
											<a href="#">Carpentry</a>
										</li>
										<li class="menu-item">
											<a href="#">Electrical</a>
										</li>
<!-- 										<li class="menu-item">
											<a href="#">Cabinet Repairs</a>
										</li> -->
<!-- 										<li class="menu-item">
											<a href="#">Flat Panel TV Installations</a>
										</li> -->
										<li class="menu-item">
											<a href="#">Plumbing Repairs</a>
										</li>
<!-- 										<li class="menu-item">
											<a href="#">Ceiling Fan Installation</a>
										</li> -->
<!-- 										<li class="menu-item">
											<a href="#">Garage Door Repair</a>
										</li> -->
										<li class="menu-item">
											<a href="#">Painting</a>
										</li>
										<li class="menu-item">
											<a href="#">Drywall Repair</a>
										</li>
										<li class="menu-item">
											<a href="#">Termite Damage Repairs</a>
										</li>
									</ul>
								</div>
							</aside>
							<aside class="columns1_4 widgetWrap widget widget_nav_menu">
								<h3 class="title">Pages</h3>
								<div class="menu-pages-container">
									<ul id="menu-pages" class="menu">
										<li class="menu-item">
											<a href="{{ url('/')}}">Home</a>
										</li>
										<li class="menu-item">
											<a href="{{ url('/aboutus')}}">About Us</a>
										</li>
										<li class="menu-item">
											<a href="#">Portfolio</a>
										</li>
<!-- 										<li class="menu-item">
											<a href="appointment.html">Make an Appointment</a>
										</li>
										<li class="menu-item">
											<a href="features_pages_faq.html">FAQ</a>
										</li>
										<li class="menu-item">
											<a href="pricing.html">Pricing</a>
										</li>
										<li class="menu-item">
											<a href="features_pages_about_us.html">Team</a>
										</li> -->
										<li class="menu-item">
											<a href="{{ url('/contactus')}}">Contacts</a>
										</li>
									</ul>
								</div>
							</aside>
							<aside class="columns1_4 widgetWrap widget widget_text">
								<h3 class="title">Build Advisor</h3>
								<div class="textwidget">
									<p>Our company does its work with the customer in mind. That is why our customers are getting only the best services and products to fulfill their most sophisticated demands.</p>
<!-- 									<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
										<a href="#" class="">Buy Now</a>
									</div> -->
								</div>
							</aside>
						</div>
					</div>
				</footer> 
				<div class="copyWrap">
					<div class="copy main">
						<div class="copyright">
							<a href="#">Inovace</a>&copy; 2020 All Rights Reserved
							<a href="#">Terms of Use</a>
							and <a href="#">Privacy Policy</a>
						</div>
						<div class="copy_socials socPage">
							<ul>
								<li>
									<a class="social_icons icon-facebook" target="_blank" href="http://facebook.com"> </a>
								</li>
								<li>
									<a class="social_icons icon-tumblr" target="_blank" href="http://twitter.com"> </a>
								</li>
								<li>
									<a class="social_icons icon-pinterest" target="_blank" href="http://pinterest.com"> </a>
								</li>
								<li>
									<a class="social_icons icon-dribbble" target="_blank" href="http://dribbble.com"> </a>
								</li>
								<li>
									<a class="social_icons icon-rss" target="_blank" href="#"> </a>
								</li>
								<li>
									<a class="social_icons icon-gplus" target="_blank" href="http://gplus.com"> </a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div> 
		</div> 
	</div> 

<!-- 	<div id="preloader" class="preloader">
	    <div class="preloader_image"></div>
	</div> -->


	<script data-cfasync="false" src="{{ url('cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js')}}"></script>
	<script type='text/javascript' src="{{ url('/js/vendor/jquery.js')}}"></script>
	<script type='text/javascript' src="{{ url('/js/vendor/jquery-migrate.min.js')}}"></script>
	<script type='text/javascript' src="{{ url('/js/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
	<script type='text/javascript' src="{{ url('/js/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>

	<script type='text/javascript' src="{{ url('/js/custom/_main.js?v=2')}}" ></script>
	<script type='text/javascript' src="{{ url('/js/vendor/__packed.js')}}" ></script>
	<script type='text/javascript' src="{{ url('/js/custom/shortcodes_init.js')}}" ></script>
	<script type='text/javascript' src="{{ url('/js/custom/_utils.js')}}" ></script>
	<script type='text/javascript' src="{{ url('/js/custom/_front.js')}}" ></script>
	<script type='text/javascript' src="{{ url('/js/vendor/flipclock/flipclock.custom.js')}}"> </script>
<!-- 	<script type='text/javascript' src="{{ url('/custom_tools/js/_customizer.js')}}" ></script> -->
	<script type='text/javascript' src="{{ url('/js/vendor/jquery.hoverdir.js')}}" ></script>
	<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false'></script>
	<script type='text/javascript' src="{{ url('/js/vendor/accordion.min.js')}}"></script>
</body>
</html>