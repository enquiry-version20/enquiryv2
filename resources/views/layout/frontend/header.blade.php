
<!DOCTYPE html>
<html lang="en-US">
<head>
	<meta charset="UTF-8"/>
	<meta http-equiv="cache-control" content="no-cache">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="format-detection" content="telephone=no">
	<title>Build Advisor</title>
	<link rel="icon" type="image/x-icon" href="{{ url('/images/favicon.ico')}}"/>
	<link rel='stylesheet' id='rs-plugin-settings-css' href="{{ url('/js/vendor/rs-plugin/css/settings.css?v=1')}}" type='text/css' media='all'/>
	<style id='rs-plugin-settings-inline-css' type='text/css'></style>
	<link rel='stylesheet' id='fontello-css' href="{{ url('/fontello-css/fontello.css?v=1')}}"  type='text/css' media='all'/>
<!-- 	<link rel='stylesheet' id='fontello-css' href="{{ url('/fontello-css/fontello-1.css?v=1')}}"  type='text/css' media='all'/> -->
	<link rel='stylesheet' id='animation-css' href="{{ url('/css/fontello/css/animation.css?v=1')}}" type='text/css' media='all'/>
	<link rel='stylesheet' id='flipclock-style-css'  href="{{ url('/js/vendor/flipclock/flipclock.css?v=1')}}" type='text/css' media='all'/>
	<link rel='stylesheet' id='woocommerce-layout-css' href="{{ url('/js/vendor/woocommerce/assets/css/woocommerce-layout.css?v=1')}}"  type='text/css' media='all'/>
	<link rel='stylesheet' id='woocommerce-smallscreen-css' href="{{ url('/js/vendor/woocommerce/assets/css/woocommerce-smallscreen.css?v=1')}}" type='text/css' media='only screen and (max-width: 768px)'/>
	<link rel='stylesheet' id='woocommerce-general-css' href="{{ url('/js/vendor/woocommerce/assets/css/woocommerce.css?v=1')}}"  type='text/css' media='all'/>
	<link rel='stylesheet' id='woo-style-css' href="{{ url('/css/woo-style.css?v=1')}}" type='text/css' media='all'/>
	<link rel='stylesheet' href="{{ url('/css/__packed.css?v=2')}}" type='text/css' media='all'/>
	<link rel='stylesheet' id='main-style-css' href="{{ url('/css/style.css?v=3')}}" type='text/css' media='all'/>
	<link rel='stylesheet' id='shortcodes-css' href="{{ url('/css/shortcodes.css?v=1')}}" type='text/css' media='all'/>
	<link rel='stylesheet' id='theme-skin-css' href="{{ url('/css/default.css?v=1')}}" type='text/css' media='all'/>
	<style id='theme-skin-inline-css' type='text/css'></style>
	<link rel='stylesheet' id='responsive-css' href="{{ url('/css/responsive.css?v=3')}}" type='text/css' media='all'/>


</head>
<body class="home page fullscreen top_panel_above sidemenu_left usermenu_show">


	<div class="main_content">
		<div class="boxedWrap fixedTopMenu">

			<header class="fixedTopMenu menu_right with_user_menu" >

				<div class="topWrap" >

					<div class="mainmenu_area">
						<div class="main">

							<div class="search" title="Open/close search form" style="padding: 0;margin-left:14px;">
								<div class="searchForm">
<!-- 									<form role="search" method="get" class="search-form" action="index.html">
										<button type="submit" class="searchSubmit" title="Start search">
										<span class="icoSearch"></span>
										</button>
										<input type="text" class="searchField" placeholder="Search &hellip;" value="" name="s" title="Search for:"/>
									</form> -->
								</div>
								<div class="ajaxSearchResults"></div>
							</div>



							<div class="logo logo_left">
								<a href="{{ url('/')}}">
								<h3 style="padding: 0;margin-top:57px;font-size: 26px;
    							padding-left: 6px;-webkit-text-stroke: 1px #5d4545;
    							-webkit-text-fill-color:#ffffff;"><b>Build Advisor</b></h3>
								</a>
							</div>
							<style type="text/css">
							.menu_right .logo h3{ padding:0;margin-top: 14px!important; }			
							</style>

							<nav role="navigation" class="menuTopWrap topMenuStyleLine">
								<ul id="" class="">
									<li class="menu-item current-menu-item current-menu-ancestor current-menu-parent menu-item-has-children">

									</li>
									<li class="menu-item menu-item-has-children">

									</li>
									<li class="menu-item menu-item-has-children columns custom_view_item">

									</li>
									<li class="menu-item menu-item-has-children">

									</li>
									<li class="menu-item">
									</li>

									<li class="menu-item">
									</li>

									<li class="menu-item phone" style="padding-top: 18px;">
										<a href="#" style="font-size: 17px;-webkit-text-stroke: 1px #000;
    									-webkit-text-fill-color: #fff;">+1(888)966-9284</a>
									</li>
									<li class="highlight menu-item" style="padding-top: 18px;">
										<a href="{{ url('/expert-signup')}}" style="font-size: 15px;">Are You an Expert!</a>
									</li>
								</ul>
							</nav>
						</div>
					</div>
				</div>
			</header>	
			<style type="text/css">


			</style>