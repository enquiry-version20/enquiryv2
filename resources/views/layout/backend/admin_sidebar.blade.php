 <!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm">

            <div class="navbar-header" style="background-color: #4a2b4e !important;">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
               
               <!--  <a  href="{{url('/admin/home')}}"><img src="{{ url('public/assets/adminlayout/images/logo.png')}}" width="130" height="50" style="margin-top: 7px;" alt=""/></a> -->
                <a class="navbar-brand" href="{{url('/admin/home')}}">MGNREGA</a>
                <a class="navbar-brand hidden" href="{{url('/admin/home')}}">MGNREGA</a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse" style="background-color: #4a2b4e !important;">
                <ul class="nav navbar-nav" 
                style="margin-top: 51px;">
    

                    
                      <li>
                        <a href="{{url('/admin/home')}}"> <i class="menu-icon ti-dashboard"></i>Dashboard </a>
                    </li>
                   
                    
                   
                  
                  <li>
                        
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon ti-user"></i>Users</a>
                        <ul class="sub-menu children dropdown-menu" style="background-color: #4a2b4e !important;">
                            <li><i class="menu-icon ti-support"></i><a href="{{url('/admin/add-user')}}" style="color:white !important;">Add user</a></li>
                            <li><i class="menu-icon ti-align-justify"></i><a href="{{url('/admin/users')}}" style="color:white !important;">List Users</a></li>
                           
                        </ul>
                    </li>
                   <li>
                        
                    </li>
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon ti-magnet"></i>Survey data</a>
                        <ul class="sub-menu children dropdown-menu" style="background-color: #4a2b4e !important;">
                           
                            <li><i class="menu-icon ti-list"></i><a href="{{url('/admin/survey')}}" style="color:white !important;">List Survey data</a></li>
                           
                        </ul>
                    </li>
                
                    
                   
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside><!-- /#left-panel -->

    <!-- Left Panel -->