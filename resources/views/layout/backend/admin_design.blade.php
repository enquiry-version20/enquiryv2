<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Build Advisor - Admin</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{url('img/favicon.jpg')}}"/>

    <!-- Main css -->
    <link rel="stylesheet" href="{{url('backend/vendors/bundle.css')}}" type="text/css">

    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap" rel="stylesheet">

    <!-- Daterangepicker -->
    <link rel="stylesheet" href="{{url('backend/vendors/datepicker/daterangepicker.css')}}" type="text/css">

    <!-- DataTable -->
    <link rel="stylesheet" href="{{url('backend/vendors/dataTable/datatables.min.css')}}" type="text/css">

<!-- App css -->
    <link rel="stylesheet" href="{{url('backend/assets/css/app.min.css')}}" type="text/css">



    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="scrollable-layout">
<!-- Preloader -->
<div class="preloader">
    <div class="preloader-icon"></div>
    <span>Loading...</span>
</div>
<!-- ./ Preloader -->
<div class="layout-wrapper">


    <!-- Header -->
    <div class="header d-print-none">
        <div class="header-container">
            <div class="header-left">
                <div class="navigation-toggler">
                    <a href="#" data-action="navigation-toggler">
                        <i data-feather="menu"></i>
                    </a>
                </div>

                <div class="header-logo">
                    <a href="{{url('/admin/home')}}">
                        <img class="logo" src="{{url('images/logo/logo.png')}}" alt="logo" width="41">
                        <span style="margin-left: 10px;color: white;font-size: 16px;">HOME ADVISOR</span>
                    </a> 
                </div>
            </div>

            <div class="header-body">
                <div class="header-body-left">
                    <ul class="navbar-nav">

                    </ul>
                </div>

                <div class="header-body-right">
                    <ul class="navbar-nav">






                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link dropdown-toggle" title="User menu" data-toggle="dropdown">

                                <span class="ml-2 d-sm-inline d-none">Admin</span>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-big">

                                <div class="list-group">
                                    <a href="{{ url('/logout') }}" class="list-group-item">Logout</a>

                                </div>

                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <ul class="navbar-nav ml-auto">
                <li class="nav-item header-toggler">
                    <a href="#" class="nav-link">
                        <i data-feather="arrow-down"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <!-- ./ Header -->

    <!-- Content wrapper -->
    <div class="content-wrapper">
        <!-- begin::navigation -->
        <div class="navigation">
            <div class="navigation-header">
                <span>Navigation</span>
                <a href="#">
                    <i class="ti-close"></i>
                </a>
            </div>
            <div class="navigation-menu-body">
                <ul>
                    <li>
                        <a class="{{ Request::is('admin/home') ? 'active' : '' }}"  href="{{url('/admin/home')}}">
                    <span class="nav-link-icon">
                        <i data-feather="pie-chart"></i>
                    </span>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a class="{{ Request::is('admin/banners') ? 'active' : '' }}"
                            href="{{url('/admin/banners')}}">
                    <span class="nav-link-icon">
                        <i data-feather="message-circle"></i>
                    </span>
                            <span>Banners</span>
                        </a>
                    </li>
<!--                     <li>
                        <a class="{{ Request::is('admin/material') ? 'active' : '' }}"
                            href="{{url('/admin/material')}}">
                    <span class="nav-link-icon">
                        <i data-feather="message-circle"></i>
                    </span>
                            <span>Meterial</span>
                        </a>
                    </li> -->
                    <li>
                        <a class="{{ Request::is('admin/service') ? 'active' : '' }}"
                            href="{{url('/admin/service')}}">
                    <span class="nav-link-icon">
                        <i data-feather="message-circle"></i>
                    </span>
                            <span>Services</span>
                        </a>
                    </li>



                    <li>
                        <a class="{{ Request::is('admin/enquiry') ? 'active' : '' }}" href="{{url('/admin/enquiry')}}">
                    <span class="nav-link-icon">
                        <i data-feather="file-plus"></i>
                    </span>
                            <span>Enquiry</span>
                        </a>
                    </li>


<!--                     <li>
                        <a href="#">
                    <span class="nav-link-icon">
                        <i data-feather="shopping-cart"></i>
                    </span>
                            <span>Logs</span>
                        </a>
                        <ul>
                            <li>
                                <a class="{{ Request::is('admin/logs') ? 'active' : '' }}" href="{{url('/admin/logs')}}">logs</a>
                            </li>
                            <li>
                                <a class="{{ Request::is('admin/userlogs') ? 'active' : '' }}" href="{{url('/admin/userlogs')}}">User Logs</a>
                            </li>  

                        </ul>
                    </li>

                    <li>
                        <a href="#">
                    <span class="nav-link-icon">
                        <i data-feather="shopping-cart"></i>
                    </span>
                            <span>Admin Users</span>
                        </a>
                        <ul>
                            <li>
                                <a class="{{ Request::is('admin/admin-roles') ? 'active' : '' }}" href="{{url('/admin/admin-roles')}}">Roles</a>
                            </li>
                            <li>
                                <a class="{{ Request::is('admin/admin-users') ? 'active' : '' }}" href="{{url('/admin/admin-users')}}">Users</a>
                            </li>
                            <li>
                                <a class="{{ Request::is('admin/admin-permissions') ? 'active' : '' }}" href="{{url('/admin/admin-permissions')}}">Permissions</a>
                            </li>
                        </ul>
                    </li> -->



                </ul>
            </div>
        </div>   

    @yield('content')
    

            <!-- Footer -->
            <footer class="content-footer">
                <div>© 2020 <a href="http://inovice.in" target="_blank">Inovace</a></div>
                <div>
<!--                     <nav class="nav">
                        <a href="https://themeforest.net/licenses/standard" class="nav-link">Licenses</a>
                        <a href="#" class="nav-link">Change Log</a>
                        <a href="#" class="nav-link">Get Help</a>
                    </nav> -->
                </div>
            </footer>
            <!-- ./ Footer -->
        </div>
        <!-- ./ Content body -->
    </div>
    <!-- ./ Content wrapper -->
</div>
<!-- ./ Layout wrapper -->

<!-- Main scripts -->

<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script> -->



    <script src="{{url('backend/vendors/bundle.js')}}"></script>

    <!-- Apex chart -->
    <script src="{{url('backend/vendors/charts/apex/apexcharts.min.js')}}"></script>

    <!-- Daterangepicker -->
    <script src="{{url('backend/vendors/datepicker/daterangepicker.js')}}"></script>

    <!-- DataTable -->
    <script src="{{url('backend/vendors/dataTable/datatables.min.js')}}"></script>

    <script src="{{url('backend/assets/js/examples/pages/user-list.js')}}"></script>

 <!-- repeater -->
    <script src="{{url('backend/vendors/jquery.repeater.min.js')}}"></script>  
<!-- Prism -->
    <script src="{{url('backend/vendors/prism/prism.js')}}"></script>

    <!-- Dashboard scripts -->
    <script src="{{url('backend/assets/js/examples/pages/dashboard.js')}}"></script>

<!-- App scripts -->
    <script src="{{url('backend/assets/js/app.min.js')}}"></script>

    <script src="{{url('backend/assets/js/examples/form-validation.js')}}"></script>

</body>
</html>
      