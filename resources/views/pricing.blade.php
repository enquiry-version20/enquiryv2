@extends('layout.frontend.design')
@section('content')



			<div id="topOfPage" class="topTabsWrap">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">All Posts</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<span class="current">Pricing Tables</span>
					</div>
					<h3 class="pageTitle h3">Pricing Tables</h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
				<div class="content">
					<div class="itemscope" itemscope itemtype="http://schema.org/Article">
						<section class="post post_format_standard postAlter no_margin page type-page status-publish hentry">
							<article class="post_content">
								<div class="post_text_area" itemprop="articleBody">

									<div class="vc_row wpb_row vc_row-fluid light_section">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<h2 class="sc_title sc_aligncenter sc_title_regular">Pricing tables</h2>
													<div class="sc_pricing_table columns_4 alignCenter">
														<div class="sc_pricing_columns sc_pricing_column_1">
															<ul class="columnsAnimate">
																<li class="sc_pricing_data sc_pricing_title">
																	<div>Basic</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<div class="sc_price_currency">$</div>
																			<div class="sc_price_money">29.</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">99</div>
																				<div class="sc_price_period">monthly</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>1 Website</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Unlimited Bandwidth</div>
																</li>
																<li class="sc_pricing_data">
																	<div>100 GB Disk Space</div>
																</li>
																<li class="sc_pricing_data">
																	<div>10 MySQL Databases</div>
																</li>
																<li class="sc_pricing_data">
																	<div>100 Email Addresses</div>
																</li>
															</ul>
														</div>
														<div class="sc_pricing_columns sc_pricing_column_2">
															<ul class="columnsAnimate">
																<li class="sc_pricing_data sc_pricing_title">
																	<div>Standard</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<div class="sc_price_currency">$</div>
																			<div class="sc_price_money">39.</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">99</div>
																				<div class="sc_price_period">monthly</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>10 Websites</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Unlimited Bandwidth</div>
																</li>
																<li class="sc_pricing_data">
																	<div>1 TB Disk Space</div>
																</li>
																<li class="sc_pricing_data">
																	<div>100 MySQL databases</div>
																</li>
																<li class="sc_pricing_data">
																	<div>1000 Email Addresses</div>
																</li>
															</ul>
														</div>
														<div class="sc_pricing_columns sc_pricing_column_3">
															<ul class="columnsAnimate">
																<li class="sc_pricing_data sc_pricing_title">
																	<div>Premium</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<div class="sc_price_currency">$</div>
																			<div class="sc_price_money">49.</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">99</div>
																				<div class="sc_price_period">monthly</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>50 Websites</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Unlimited Bandwidth</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Unlimited Disk Space</div>
																</li>
																<li class="sc_pricing_data">
																	<div>100 MySQL Databases</div>
																</li>
																<li class="sc_pricing_data">
																	<div>5000 Email Addresses</div>
																</li>
															</ul>
														</div>
														<div class="sc_pricing_columns sc_pricing_column_4">
															<ul class="columnsAnimate">
																<li class="sc_pricing_data sc_pricing_title">
																	<div>Gold</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<div class="sc_price_currency">$</div>
																			<div class="sc_price_money">59.</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">99</div>
																				<div class="sc_price_period">monthly</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Unlimited Websites</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Unlimited Bandwidth</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Unlimited Disk Space</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Unlimited MySQL Databases</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Unlimited Email Addresses</div>
																</li>
															</ul>
														</div>
													</div>
													<div class="sc_line sc_line_style_solid margin_top_large margin_bottom_none"></div>
												</div>
											</div>
										</div>
									</div>
									<div class="vc_row wpb_row vc_row-fluid light_section">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<div class="sc_pricing_table columns_5 alignCenter">
														<div class="sc_pricing_columns sc_pricing_column_1">
															<ul>
																<li class="sc_pricing_data sc_pricing_united">
																	<div>Choose Your Plan</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Amount of space</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Bandwidth per month</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Free domain names</div>
																</li>
																<li class="sc_pricing_data">
																	<div>phpMyAdmin</div>
																</li>
																<li class="sc_pricing_data">
																	<div>24h Support</div>
																</li>
																<li class="sc_pricing_data sc_pricing_footer">
																	<div>
																	</div>
																</li>
															</ul>
														</div>
														<div class="sc_pricing_columns sc_pricing_column_2">
															<ul class="columnsAnimate">
																<li class="sc_pricing_data sc_pricing_title">
																	<div>Basic</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<div class="sc_price_currency">$</div>
																			<div class="sc_price_money">19.</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">99</div>
																				<div class="sc_price_period">Monthly</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>UNLIMITED</div>
																</li>
																<li class="sc_pricing_data">
																	<div>100 GB</div>
																</li>
																<li class="sc_pricing_data">
																	<div>3 Free</div>
																</li>
																<li class="sc_pricing_data">
																	<div>
																		<span class="sc_icon icon-cancel">
																		</span>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>
																		<span class="sc_icon icon-cancel">
																		</span>
																	</div>
																</li>
																<li class="sc_pricing_data sc_pricing_footer">
																	<div>
																		<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
																			<a href="#" class="">Sign Up</a>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
														<div class="sc_pricing_columns sc_pricing_column_3">
															<ul class="columnsAnimate">
																<li class="sc_pricing_data sc_pricing_title">
																	<div>Standard</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<div class="sc_price_currency">$</div>
																			<div class="sc_price_money">59.</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">99</div>
																				<div class="sc_price_period">Monthly</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>UNLIMITED</div>
																</li>
																<li class="sc_pricing_data">
																	<div>500 GB</div>
																</li>
																<li class="sc_pricing_data">
																	<div>6 Free</div>
																</li>
																<li class="sc_pricing_data">
																	<div>
																		<span class="sc_icon icon-ok">
																		</span>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>
																		<span class="sc_icon icon-cancel">
																		</span>
																	</div>
																</li>
																<li class="sc_pricing_data sc_pricing_footer">
																	<div>
																		<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
																			<a href="#" class="">Sign Up</a>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
														<div class="sc_pricing_columns sc_pricing_column_4">
															<ul class="columnsAnimate">
																<li class="sc_pricing_data sc_pricing_title">
																	<div>Premium</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<div class="sc_price_currency">$</div>
																			<div class="sc_price_money">79.</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">99</div>
																				<div class="sc_price_period">Monthly</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>UNLIMITED</div>
																</li>
																<li class="sc_pricing_data">
																	<div>1 TB</div>
																</li>
																<li class="sc_pricing_data">
																	<div>10 Free</div>
																</li>
																<li class="sc_pricing_data">
																	<div>
																		<span class="sc_icon icon-ok">
																		</span>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>
																		<span class="sc_icon icon-ok">
																		</span>
																	</div>
																</li>
																<li class="sc_pricing_data sc_pricing_footer">
																	<div>
																		<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
																			<a href="#" class="">Sign Up</a>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
														<div class="sc_pricing_columns sc_pricing_column_5">
															<ul class="columnsAnimate">
																<li class="sc_pricing_data sc_pricing_title">
																	<div>Gold</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<div class="sc_price_currency">$</div>
																			<div class="sc_price_money">99.</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">99</div>
																				<div class="sc_price_period">Monthly</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>UNLIMITED</div>
																</li>
																<li class="sc_pricing_data">
																	<div>UNLIMITED</div>
																</li>
																<li class="sc_pricing_data">
																	<div>UNLIMITED</div>
																</li>
																<li class="sc_pricing_data">
																	<div>
																		<span class="sc_icon icon-ok">
																		</span>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>
																		<span class="sc_icon icon-ok">
																		</span>
																	</div>
																</li>
																<li class="sc_pricing_data sc_pricing_footer">
																	<div>
																		<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
																			<a href="#" class="">Sign Up</a>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
													</div>
													<div class="sc_line sc_line_style_solid margin_top_large margin_bottom_none">
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="vc_row wpb_row vc_row-fluid light_section">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<div class="sc_pricing_table columns_4 alignCenter">
														<div class="sc_pricing_columns sc_pricing_column_1">
															<ul>
																<li class="sc_pricing_data sc_pricing_title_img">
																	<div>
																		<img src="images/Depositphotos_2136700_original.jpg" alt="" />
																	</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<div class="sc_price_currency">$</div>
																			<div class="sc_price_money">29.</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">99</div>
																				<div class="sc_price_period">Monthly</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>10 Splendide philosophia et</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Cum at probo / minimum</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Falli libris has id facer</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Pertinax vel eum ne molestie</div>
																</li>
																<li class="sc_pricing_data sc_pricing_footer">
																	<div>
																		<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
																			<a href="#" class="">Sign Up</a>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
														<div class="sc_pricing_columns sc_pricing_column_2">
															<ul>
																<li class="sc_pricing_data sc_pricing_title_img">
																	<div>
																		<img src="images/Depositphotos_10321051_original.jpg" alt="" />
																	</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<div class="sc_price_currency">$</div>
																			<div class="sc_price_money">39.</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">99</div>
																				<div class="sc_price_period">Monthly</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>50 Splendide philosophia et</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Cum at probo / minimum</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Falli libris has id facer</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Pertinax vel eum ne molestie</div>
																</li>
																<li class="sc_pricing_data sc_pricing_footer">
																	<div>
																		<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
																			<a href="#" class="">Sign Up</a>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
														<div class="sc_pricing_columns sc_pricing_column_3">
															<ul>
																<li class="sc_pricing_data sc_pricing_title_img">
																	<div>
																		<img src="images/bigstock-Kitchen-in-luxury-home-with-wh-16568375.jpg" alt="" />
																	</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<div class="sc_price_currency">$</div>
																			<div class="sc_price_money">49.</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">99</div>
																				<div class="sc_price_period">Monthly</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>100 Splendide philosophia et</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Cum at probo / minimum</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Falli libris has id facer</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Pertinax vel eum ne molestie</div>
																</li>
																<li class="sc_pricing_data sc_pricing_footer">
																	<div>
																		<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
																			<a href="#" class="">Sign Up</a>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
														<div class="sc_pricing_columns sc_pricing_column_4">
															<ul>
																<li class="sc_pricing_data sc_pricing_title_img">
																	<div>
																		<img src="images/kaboompics.com_Old-brick-building.jpg" alt="" />
																	</div>
																</li>
																<li class="sc_pricing_data sc_pricing_price">
																	<div>
																		<div class="sc_price_item">
																			<div class="sc_price_currency">$</div>
																			<div class="sc_price_money">59.</div>
																			<div class="sc_price_info">
																				<div class="sc_price_penny">99</div>
																				<div class="sc_price_period">Monthly</div>
																			</div>
																		</div>
																	</div>
																</li>
																<li class="sc_pricing_data">
																	<div>200 Splendide philosophia et</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Cum at probo / minimum</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Falli libris has id facer</div>
																</li>
																<li class="sc_pricing_data">
																	<div>Pertinax vel eum ne molestie</div>
																</li>
																<li class="sc_pricing_data sc_pricing_footer">
																	<div>
																		<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
																			<a href="#" class="">Sign Up</a>
																		</div>
																	</div>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="tagsWrap"></div>
								</div>  
							</article>  
						</section>  
					</div>    
				 
				</div>  
			</div>
@endsection