
@extends('layout.frontend.design')
@section('content')

			<div id="topOfPage" class="topTabsWrap">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">All Posts</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<span class="current">Page 404</span>
					</div>
					<h3 class="pageTitle h3">Page 404</h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
				<div class="main" role="main">
					<div class="content">
						<section class="post no_margin">
							<article class="post_content">
								<div class="page404">
									<div class="titleError">404</div>
									<div class="h2">The requested page cannot be found</div>
									<p>
										Go back, or return to 
										<a href="index.html">House Repair</a> 
										home page to choose a new page. 
										<br>Please report any broken links to our team.
									</p>
									<div class="inputSubmitAnimation radCircle">
										<form class="search_form" action="#" method="get">
											<input type="text" class="sInput" name="s" value="" placeholder="What are you searching for?">
										</form>
										<a href="#" class="searchIcon aIco search" title="Search"></a>
									</div>
								</div>
							</article>
						</section>
					</div>
				</div>
			</div>
@endsection	