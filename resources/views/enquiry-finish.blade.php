

@extends('layout.frontend.design')
@section('content')


			<div id="topOfPage" class="topTabsWrap" style="border:4px solid white;">
				<div class="main">
					<div class="speedBar">
						<a class="" href="{{ url('/')}}">Back to Home</a>
					</div>
					<h3 class="pageTitle h3"><a class="home" href="{{ url('/')}}">BUILD ADVISER</a></h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar" style="border:4px solid white;">
				<div class="content">
					<div class="itemscope" itemscope itemtype="http://schema.org/Article">
						<section class="post post_format_standard postAlter no_margin page type-page status-publish hentry">
							<article class="post_content">
								<div class="post_text_area" itemprop="articleBody">

									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_section grey_section">
													<div class="sc_content main" data-animation="animated fadeInUp">
														<h3 class="sc_undertitle sc_title_regular" style="padding: 0;font-weight: 500;color: crimson;">Thank you for contacting us !</h3><hr>
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<p><br/>
															 </p>
															</div>
														</div>
														<div class="sc_contact_form sc_contact_form_contact_3 margin_top_small">
														<h3 class="sc_undertitle sc_title_regular" style="padding: 0;font-weight: 400;">Your Enquiry is Recorded,We will contact you soon.</h3>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="tagsWrap">
										
									</div>
								</div>  
							</article>  
						</section>  
					</div>    
				 
				</div>  
			</div>


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript">

jQuery(document).ready(function(){

	$('#frmcontact').on('submit', function(e){
		e.preventDefault();
			
           $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
              }
          });
           jQuery.ajax({
              url: "{{ url('/contact-form-submit') }}",
              method: 'POST',
			  dataType:'JSON',
           
              data: {
                 _token: '{{csrf_token()}}', 
                 city: jQuery('#city').val(),
				 state: jQuery('#state').val(),
				 district: jQuery('#district').val(),
				 pincode: jQuery('#pincode').val(),
				 service_id: jQuery('#service_id').val(),

              },
              success: function(result){
                 
				 alert(result.url)
				 window.location.href =result.url;
              }});
           });
        });

</script>
@endsection			