
@extends('layout.frontend.design')
@section('content')


			<div id="topOfPage" class="topTabsWrap">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">All Posts</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<span class="current">Under Construction</span>
					</div>
					<h3 class="pageTitle h3">Under Construction</h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
				<div class="main" role="main">
					<div class="content">
						<div class="itemscope" itemscope itemtype="http://schema.org/Article">
							<section class="post postAlter no_margin page">
								<article class="post_content">
									<div class="post_text_area" itemprop="articleBody">
										<div class="vc_row wpb_row vc_row-fluid">
											<div class="wpb_column vc_column_container vc_col-sm-12">
												<div class="wpb_wrapper">
													<div class="wpb_text_column wpb_content_element ">
														<div class="wpb_wrapper sc_center">
															<h1>Our Website is Under Construction</h1>
															<p>Estimated time remaining before launching the site </p>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="vc_row wpb_row vc_row-fluid">
											<div class="wpb_column vc_column_container vc_col-sm-12">
												<div class="wpb_wrapper">
													<div class="sc_section sc_aligncenter">
														<div class="sc_countdown  sc_countdown_flip margin_top_mini margin_bottom_mini">
															<div class="sc_countdown_counter" data-style="flip" data-date="2016-02-11" data-time="">
															</div>
														</div>
													</div>
													<div class="sc_emailer inputSubmitAnimation sc_aligncenter sFocus rad4 opened">
														<form>
															<input type="text" class="sInput" name="email" value="" placeholder="Please, enter you email address.">
														</form>
														<a href="#" class="sc_emailer_button searchIcon aIco mail" title="Submit" data-group="Under Constraction"> </a>
													</div>
												</div>
											</div>
										</div>
										<div class="tagsWrap"></div>
									</div>
								</article>
							</section>
						</div>
					</div>
				</div>
			</div>

@endsection			