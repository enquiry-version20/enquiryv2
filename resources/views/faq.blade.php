
@extends('layout.frontend.design')
@section('content')


			<div id="topOfPage" class="topTabsWrap">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="index.html">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="all" href="#">All Posts</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<span class="current">FAQ</span>
					</div>
					<h3 class="pageTitle h3">FAQ</h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
				<div class="main" role="main">
					<div class="content">
						<div class="itemscope" itemscope itemtype="http://schema.org/Article">
							<section class="post postAlter no_margin page">
								<article class="post_content">
									<div class="post_text_area" itemprop="articleBody">
										<div class="vc_row wpb_row vc_row-fluid">
											<div class="wpb_column vc_column_container vc_col-sm-12">
												<div class="wpb_wrapper">
													<div class="columnsWrap sc_columns sc_columns_count_3">
														<div class="columns2_3 sc_column_item sc_column_item_1 odd first span_2">
															<h2 class="sc_title sc_title_regular">Got Questions?</h2>
															<div class="wpb_text_column wpb_content_element ">
																<div class="wpb_wrapper">
																	<p>Equidem laboramus voluptaria vix no, vel nulla disputationi delicatissimi at, solet dignissim nam in. Simul efficiendi interpretaris eu nec. An graeco offendit eos, sea an graece platonem democritum. Has nobis eirmod in, voluptua definiebas eu sea. Possit docendi comprehensam quo ea, quo no epicurei intellegebat. Essent iriure nec eu.</p>
																</div>
															</div>
															<div class="sc_toggles sc_toggles_style_2 sc_show_counter sc_toggles_large">
																<div class="sc_toggles_item sc_active sc_toggles_item_large odd first">
																	<h3 class="sc_toggles_title">
																	<span class="sc_items_counter">1</span>Cras sodales malesuada ultrices</h3>
																	<div class="sc_toggles_content" style="display:block;">
																		<div class="wpb_text_column wpb_content_element ">
																			<div class="wpb_wrapper">
																				<p>Sed ut ornare leo. Cras sed porttitor lacus. Donec porta diam nunc, vel mollis enim tempus sed. Sed commodo tincidunt justo dictum ornare. Vivamus varius malesuada diam, congue congue sem suscipit eleifend. Vestibulum sodales porta sapien vitae ultrices.</p>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="sc_toggles_item sc_toggles_item_large even">
																	<h3 class="sc_toggles_title">
																	<span class="sc_items_counter">2</span>Praesent adipiscing velit et dolor egestas</h3>
																	<div class="sc_toggles_content">
																		<div class="wpb_text_column wpb_content_element ">
																			<div class="wpb_wrapper">
																				<p>Nunc hendrerit ullamcorper lectus, quis mollis nisl tincidunt non. Duis aliquet dictum lobortis. Cras sodales orci a lectus molestie, sed semper eros laoreet. Sed facilisis, libero at pellentesque molestie, quam tortor auctor tortor, in varius mauris massa nec dui. Proin convallis magna at mollis ultricies. Mauris ultrices mi et elit consectetur pharetra.</p>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="sc_toggles_item sc_toggles_item_large odd">
																	<h3 class="sc_toggles_title">
																	<span class="sc_items_counter">3</span>Maecenas semper porta molestie</h3>
																	<div class="sc_toggles_content">
																		<div class="wpb_text_column wpb_content_element ">
																			<div class="wpb_wrapper">
																				<p>Vestibulum non felis ut sem blandit porta a eget felis. Duis adipiscing felis enim, sit amet fermentum turpis vulputate at. Duis tincidunt odio elit, sit amet placerat mi aliquam dapibus.</p>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="sc_toggles_item sc_toggles_item_large even">
																	<h3 class="sc_toggles_title">
																	<span class="sc_items_counter">4</span>Phasellus ultricies consectetur erat</h3>
																	<div class="sc_toggles_content">
																		<div class="wpb_text_column wpb_content_element ">
																			<div class="wpb_wrapper">
																				<p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer blandit posuere ultrices. Sed ut purus ut nunc pretium fermentum sit amet a erat. Sed sed gravida nisi, ut blandit mi. Aliquam quis vehicula tellus.</p>
																			</div>
																		</div>
																	</div>
																</div>
																<div class="sc_toggles_item sc_toggles_item_large odd">
																	<h3 class="sc_toggles_title">
																	<span class="sc_items_counter">5</span>Sed semper sem quis ligula</h3>
																	<div class="sc_toggles_content">
																		<div class="wpb_text_column wpb_content_element ">
																			<div class="wpb_wrapper">
																				<p>Proin quis commodo ligula, vitae vulputate sapien. Nam fermentum nisi eget est mattis, vitae interdum leo suscipit. Pellentesque at adipiscing ipsum. Cras venenatis commodo magna ac ornare.</p>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
															<h2 class="sc_title sc_title_regular margin_top_large">Didn&#8217;t find the answer?</h2>
															<div class="sc_contact_form sc_contact_form_contact_1 margin_bottom_small">
																<form data-formtype="contact_1" method="post" action="#">
																	<div class="">
																		<div class="">
																			<input id="sc_contact_form_username" type="text" name="username" placeholder="Name">
																		</div>
																		<div class="">
																			<input id="sc_contact_form_email" type="text" name="email" placeholder="E-mail">
																		</div>
																		<div class="">
																			<input id="sc_contact_form_subj" type="text" name="subject" placeholder="Subject">
																		</div>
																	</div>
																	<div class="message">
																		<textarea id="sc_contact_form_message" class="textAreaSize" name="message" placeholder="Message"></textarea>
																	</div>
																	<div class="sc_contact_form_button">
																		<div class="squareButton sc_button_size sc_button_style_global global">
																			<a href="#" class="sc_contact_form_submit">Send Message</a>
																		</div>
																	</div>
																	<div class="result sc_infobox">
																	</div>
																</form>
															</div>
														</div>
														<div class="columns1_3 sc_column_item sc_column_item_3 odd after_span_2">
															<h3 class="sc_title sc_title_regular">Category</h3>
															<ul class="sc_list sc_list_style_arrows">
																<li class="sc_list_item icon-right-open-big odd first">
																	<a href="#">All</a>
																</li>
																<li class="sc_list_item icon-right-open-big even">
																	<a href="#">Basic Maintenance</a>
																</li>
																<li class="sc_list_item icon-right-open-big odd">
																	<a href="#">Carpentry</a>
																</li>
																<li class="sc_list_item icon-right-open-big even">
																	<a href="#">Installation</a>
																</li>
																<li class="sc_list_item icon-right-open-big odd">
																	<a href="#">Tiling</a>
																</li>
																<li class="sc_list_item icon-right-open-big even">
																	<a href="#">Electrical</a>
																</li>
																<li class="sc_list_item icon-right-open-big odd">
																	<a href="#">Bathroom Repair</a>
																</li>
															</ul>
															<div class="sc_line sc_line_style_solid">
															</div>
															<h3 class="sc_title sc_title_regular">Video tutorials</h3>
															<div class="sc_video_player" data-width="100%" data-height="197">
																<div class="sc_video_player_title"></div>
																<div class="sc_video_frame sc_video_play_button" data-video="&lt;iframe class=&quot;video_frame&quot; src=&quot;https://youtube.com/embed/NykRXMw1N3Y?autoplay=1&quot; width=&quot;100%&quot; height=&quot;197&quot; frameborder=&quot;0&quot; webkitAllowFullScreen=&quot;webkitAllowFullScreen&quot; mozallowfullscreen=&quot;mozallowfullscreen&quot; allowFullScreen=&quot;allowFullScreen&quot;&gt;&lt;/iframe&gt;">
																	<img alt="" src="images/Depositphotos_10321051_original-295x197.jpg">
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="tagsWrap"></div>
									</div>
								</article>
							</section>
						</div>
					</div>
				</div>
			</div>

@endsection			