

@extends('layout.frontend.design')
@section('content')


			<div id="topOfPage" class="topTabsWrap" style="border:4px solid white;">
				<div class="main">
					<div class="speedBar">
						<a class="allhome" href="{{ url('/')}}">Home</a>
						<span class="breadcrumbs_delimiter"> / </span>
						<a class="home" href="#">Expert Sign Up</a>
					</div>
					<h3 class="pageTitle h3">Expert Sign Up</h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar" style="border:4px solid white;">
				<div class="content">
					<div class="itemscope" itemscope itemtype="http://schema.org/Article">
						<section class="post post_format_standard postAlter no_margin page type-page status-publish hentry">
							<article class="post_content">
								<div class="post_text_area" itemprop="articleBody">

									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_section grey_section">
													<div class="sc_content main" data-animation="animated fadeInUp">
														<h3 class="sc_undertitle sc_title_regular">We Get You the Right Customers for Your Business</h3>
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<p>Use the form below to reach new customers to win more business.<br/>
															 </p>
															</div>
														</div>
														@if (session()->has('success'))
														<div class="alert alert-success">
														    @if(is_array(session('success')))
														        <ul>
														            @foreach (session('success') as $message)
														                <li>{{ $message }}</li>
														            @endforeach
														        </ul>
														    @else
														        {{ session('success') }}
														    @endif
														</div>
														@endif

														<div class="sc_contact_form sc_contact_form_contact_3 margin_top_small">
															<form data-formtype="contact_3" method="post" action="{{ url('/expert-update')}}">
																{{ csrf_field() }}
																<div class="columnsWrap sc_columns sc_columns_count_4 no_padding">
																	<div class="columns1_4 sc_column_item sc_column_item_1 odd first">
																		<input id="sc_contact_form_username" type="text" name="firstname" placeholder="First Name" required>
																		<input id="sc_contact_form_phone" type="text" name="lastname" placeholder="Last Name" required>
																	</div>
																	<div class="columns1_4 sc_column_item sc_column_item_2 even">
																		<input id="sc_contact_form_email" type="text" name="email" placeholder="E-mail" required>
																		<input id="sc_contact_form_subj" type="text" name="phone" placeholder="Phone No" required>
																	</div>
																	<div class="columns1_4 sc_column_item sc_column_item_2 even">
																		<input id="sc_contact_form_email" type="text" name="location" placeholder="Location" required>
																		<input id="sc_contact_form_subj" type="text" name="expertise" placeholder="Expertise" required>
																	</div>


																		<div class="sc_contact_form_button" style="float:none;">
																			<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
																		<button type="submit" style="height: 85px;">Submit</button>	
																		</div>
																		</div>

																</div>
																<div class="result sc_infobox"></div>
															</form>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>


									<div class="tagsWrap"></div>
								</div>  
							</article>  
						</section>  
					</div>    
				 
				</div>  
			</div>
			<style type="text/css">
				.mainmenu_area{
					display: none!important;
				}
			</style>
@endsection			