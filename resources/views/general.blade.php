@extends('layout.frontend.design')

@section('content')



			<div id="topOfPage" class="topTabsWrap">
				<div class="main">
					<div class="speedBar">
						<a class="home" href="{{ url('/')}}"></a>
						<span class="breadcrumbs_delimiter"> </span>
						<a class="all" href="{{url('/')}}">{{$service_name}}</a>
						<span class="breadcrumbs_delimiter"> </span>
					</div>
					<h3 class="pageTitle h3"><a class="all" href="{{url('/')}}">{{$service_name}}</a></h3>
				</div>
			</div>

			<div class="mainWrap without_sidebar">
									<div class="vc_row wpb_row vc_row-fluid">
										<div class="wpb_column vc_column_container vc_col-sm-12">
											<div class="wpb_wrapper">
												<div class="sc_content main">
													<div class="sc_section sc_aligncenter" data-animation="animated fadeInUp">
														<h3>What type of property do you have?</h3>
														<div class="sc_blogger sc_blogger_horizontal style_portfolio3 portfolioWrap">				
															<section class="portfolio isotope folio3col" data-columns="4">
																@if(count($child_services)>0)
																	@foreach($child_services as $key=>$child_service)
																	<article class="isotopeElement hover_Shift odd flt_252" style="height: auto;">
																		<div class="ih-item colored square effect6 scale_up">
																			<a href="{{ url('/main-sub-services/'.$child_service->id)}}">
																				<h3 style="padding: 0;font-size:16px;font-weight: bold;line-height:15px;min-height:30px;">{{ $child_service->service_name}}</h3 >
																				<div class="img">
																					<img alt="Living Room Staircases" src="{{ url('/'.$child_service->service_image)}}" style="height:187px; width: 250px;">
																				</div>
																				<div class="info">
																					<div class="info-back" 
																					style="    padding-left: 19px;padding-right: 19px;">
																					<div class="link-wrapper">{{$child_service->service_name}}</div>
																					</div>
																				</div>
																			</a>
																		</div>
																	</article>
																	@endforeach
																@endif


															</section>

													</div>
													<div class="sc_button sc_button_style_global sc_button_size_medium squareButton global medium">
														<a href="{{ url('/')}}" class="">Previous</a>
													</div>
												</div>
											</div>
										</div>
									</div>															
			</div>
<style type="text/css">
.mainWrap .ih-item .info{
	padding-top: 30px!important;
}
.mainWrap .ih-item .info .link-wrapper{
	min-height: 55px!important;
	    padding-top: 10px!important;
}
</style>
@endsection			