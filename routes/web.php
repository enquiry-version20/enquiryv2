<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'IndexController@index')->name('index');
Route::get('/index2', 'IndexController@index2');
Route::get('/aboutus', 'IndexController@aboutus');
Route::get('/service', 'IndexController@service');
Route::get('/handyman', 'IndexController@profile');
Route::get('/protected-page', 'IndexController@protect');
Route::get('/under-construction', 'IndexController@under');
Route::get('/FAQ', 'IndexController@faq');
Route::get('/404', 'IndexController@nopage');
Route::get('/contactus', 'IndexController@contactus');
Route::get('/blog', 'IndexController@blog');
Route::get('/pricing', 'IndexController@pricing');
Route::get('/project-1', 'IndexController@project1');
Route::get('/shop', 'IndexController@shop');
Route::get('/expert-signup', 'IndexController@expertSignup');
Route::get('/painting', 'IndexController@paint');
Route::get('/painting/home', 'IndexController@home');
Route::get('/painting/type', 'IndexController@type');
Route::get('/painting/duration', 'IndexController@duration');
Route::get('/painting/enquiry', 'IndexController@enquiry');
Route::get('/painting/location', 'IndexController@location');
Route::get('/painting/contact', 'IndexController@contact');

Route::get('/general', 'GeneralController@general');
Route::get('/general/{type}', 'GeneralController@home');
//Modified on 02-09-2021
Route::get('/main-services/{id}', 'GeneralController@mainServices');
Route::get('/main-sub-services/{id}', 'GeneralController@mainSubServices');
Route::get('/main-sub-services-flag/{id}', 'GeneralController@mainSubServicesFlag');
Route::get('/contact-form/{id}', 'GeneralController@ContactForm');
Route::post('/contact-form-submit', 'GeneralController@contactFromSubmit');
Route::get('/user-contact-form/{id}', 'GeneralController@contactFromSubmit');
Route::get('/enquiry-finish', 'GeneralController@enquiryfinish');


// Route::match(array('GET','POST'),'/general/location', 'GeneralController@firststep');
Route::get('/general/location', 'GeneralController@location');
Route::post('/general/thirdstep', 'GeneralController@thirdstep');
Route::post('/general/firststep', 'GeneralController@firststep');
Route::get('/general/firststep', 'GeneralController@getfirststep');
Route::post('/general/secondstep', 'GeneralController@secondstep');
Route::get('/general/contact', 'GeneralController@contact');
Route::get('/general/store', 'GeneralController@store');

Route::post('/searching', 'SearchController@index');	

Route::get('/flooring', 'FloorController@flooring');
Route::get('/floor/installation', 'FloorController@install');
Route::get('/floor/type', 'FloorController@type');
Route::get('/floor/home', 'FloorController@home');
Route::get('/floor/location', 'FloorController@location');
Route::get('/floor/contact', 'FloorController@contact');

Route::get('/cleaning', 'CleanController@clean');
Route::get('/clean/general', 'CleanController@general');
Route::get('/clean/type', 'CleanController@type');
Route::get('/clean/duration', 'CleanController@duration');
Route::get('/clean/location', 'CleanController@location');
Route::get('/clean/contact', 'CleanController@contact');

Route::get('/roofing', 'RoofController@roof');
Route::get('/roof/type', 'RoofController@type');
Route::get('/roof/duration', 'RoofController@duration');
Route::get('/roof/location', 'RoofController@location');
Route::get('/roof/contact', 'RoofController@contact');

Route::post('/contact/email', 'ContactController@sendmail');
Route::post('/expert-update', 'ExpertController@update');

Route::get('/admin/login','Admin\AdminController@index');
Route::post('/admin/authenticate','Admin\AdminController@authenticate');


Route::group(['middleware'=>'admin'],function(){



Route::get('/logout','Admin\AdminController@logout');
Route::get('/admin/home','Admin\HomeController@index');

Route::get('/admin/banners','Admin\BannerController@index');
Route::get('/admin/create-banner','Admin\BannerController@create');
Route::post('/admin/store-banner','Admin\BannerController@store');
Route::get('/admin/delete-banner/{id}','Admin\BannerController@delete');

Route::get('/admin/service','Admin\ServiceController@index');
Route::get('/admin/create-service','Admin\ServiceController@create');
Route::post('/admin/store-service','Admin\ServiceController@store');
Route::get('/admin/service-category/{id}','Admin\ServiceController@category');
Route::post('/admin/update-category/{id}','Admin\ServiceController@updateCategory');
Route::get('/admin/delete-service/{id}','Admin\ServiceController@delete');
Route::get('/admin/view-service/{id}','Admin\ServiceController@view');
Route::get('/admin/service-edit/{id}','Admin\ServiceController@edit');
Route::post('/admin/update-service/{id}','Admin\ServiceController@update');

Route::get('/admin/enquiry','Admin\ContactController@index');
Route::get('/admin/view-enquiry/{id}','Admin\ContactController@view');
Route::get('/admin/delete-enquiry/{id}','Admin\ContactController@delete');



Route::get('/admin/material','Admin\MeterialController@index');
Route::get('/admin/create-material','Admin\MeterialController@create');
Route::post('/admin/store-material','Admin\MeterialController@store');
Route::get('/admin/view-material/{id}','Admin\MeterialController@view');
Route::get('/admin/edit-material/{id}','Admin\MeterialController@edit');
Route::post('/admin/update-material/{id}','Admin\MeterialController@update');


});





Route::get('locale/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    echo '<h1>cache is  cleared</h1>';
});
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:clear');
    echo '<h1>configuration is cleared</h1>';
});