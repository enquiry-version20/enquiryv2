<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;

class SearchController extends Controller
{
    // public function index(Request $request)
    // {
    	
    // 	$service=Service::where('service_name','like','%' . $request->get('searchQuest') . '%' )->get();
    // 	return json_encode($service);
    // }
    public function index(Request $request)
    {
       
        $query=$request->get('searchQuest');
        $data=Service::where('service_name', 'LIKE', "{$query}%")->where('search_id',1)->get();
        
        if(count($data)>0){
            $output = '<ul class="dropdown-menu">';
       
            foreach($data as $row)
            {
                if($row->parent_id==0){
                  
                    $url=url('/main-services')."/".$row->id;
                    $output .= '
                    <li id="'.$url.'"><a href="#" >'.$row->service_name.'</a></li>
                    ';
                }else{
                    $url=url('/main-sub-services')."/".$row->id;
                    
                    $output .= '
                    <li id="'.$url.'"><a href="#" >'.$row->service_name.'</a></li>
                    ';
                }
            
            }
            $output .= '</ul>';
        }else{
            $output = '1';
        }
        
       return $output;
    }
}
