<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CleanController extends Controller
{
    public function clean(){
        return view('clean');
    } 
    public function general(){
        return view('clean-general');
    } 
    public function type(){
        return view('clean-type');
    }
    public function duration(){
        return view('clean-duration');
    }
    public function location(){
        return view('clean-location');
    }
    public function contact(){
        return view('clean-contact');
    }

}
