<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Expert;
use Mail;

class ExpertController extends Controller
{
   public function update(Request $request)
   {
               

        if($request->isMethod('post'))
        {
                $expert = Expert::create([
            		'firstname'				=> $request->get('firstname'),
                    'lastname'				=> $request->get('lastname'),
                    'email'					=> $request->get('email'),
                    'phone'          		=> $request->get('phone'),
                    'location'      		=> $request->get('location'),
                    'expertise'      		=> $request->get('expertise'),
            
                ]);

                    $message                =   '';
                    $content = '<html><body>';
                    $content .= '<table  style="border-color: #666;" cellpadding="10" border="1">';
                
                
                        $content .= "<tr><td><strong> Name:</strong> </td><td>" .  strip_tags($expert['firstname']). "</td></tr>";
                        $content .= "<tr><td><strong> Email :</strong> </td><td>" .  ($expert['email']). "</td></tr>";
                        $content .= "<tr><td><strong> Phone:</strong> </td><td>" .  strip_tags($expert['phone']) . "</td></tr>";
                        $content .= "<tr><td><strong> Subject:</strong> </td><td>" .  strip_tags($expert['location']) . "</td></tr>";
                        $content .= "<tr><td><strong> Message:</strong> </td><td>" .  strip_tags($expert['expertise']) . "</td></tr>";
                                
                    $content .= "</table>";
                    $content .= "</body></html>";

                	$to_mail                =   'jagadeesh.inovace@gmail.com';
                    Mail::send('myTestMail', ['data' => $content], function($message) use ($to_mail) 
                    {
                    $message->from('info@buildadvisor.com', 'BUILD ADVISOR - EXPERT INFORMATION');
                    $message->to($to_mail)->subject('Expert Contact BUILD ADVISOR');
                    });  
                    //endlog
                return redirect()->back()->with('success', 'Thanks for contacting us! We will contact you soon');
        }
   }
}
