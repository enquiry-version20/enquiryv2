<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FloorController extends Controller
{
    public function flooring(){
        return view('floor');
    } 
    public function install(){
        return view('floor-install');
    }
    public function type(){
        return view('floor-type');
    }
    public function home(){
        return view('floor-home');
    }
    public function location(){
        return view('floor-location');
    }
    public function contact(){
        return view('floor-contact');
    }



}
