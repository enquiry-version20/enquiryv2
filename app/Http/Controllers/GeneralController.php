<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Temporary;
use App\Models\Service;
use App\Models\UserContact;

class GeneralController extends Controller
{
    public function general(){
        return view('general');
    } 


    public function home(Request $request,$type){

            $type = $type;

        // print_r($type);exit();
            $temporary              = new Temporary;
            $temporary->type        = $type;
            $temporary->save();

            return view('general-home')->with(compact('type','temporary'));

    }

    public function getfirststep(Request $request){
        
        $temporary = $request->session()->get('temporary');
        // print_r($temporary);exit();
        return view('general-location')->with(compact('temporary'));
    }
    public function firststep(Request $request){
        // print_r($request->all());exit();
        if($request->isMethod('post')){
            $data                   = $request->all();
            $id                     = $request->id;
            $temporary              = Temporary::where('id',$id)->first();
            $temporary->duration    = $data['starting'];
            $temporary->save();

            return view('general-location')->with(compact('temporary'));
        // print_r($type);exit();
        }


    } 


    public function location(Request $request){
        $temporary = Temporary::first();
        // print_r($temporary);exit();
        return view('general-location')->with(compact('temporary'));
    }

    public function secondstep(Request $request){
        
        if($request->isMethod('post')){
            $data                   = $request->all();
            $id                   = $request->id;
            $temporary              = Temporary::where('id',$id)->first();
            $temporary->pin         = $data['pin'];
            $temporary->city        = $data['city'];
            $temporary->district    = $data['district'];
            $temporary->state       = $data['state'];

            $temporary->save();

            return redirect('/general/contact');
        }
        // print_r($type);exit();
        return view('general-contact');

    } 




    public function contact(){
        // print_r($request->all());exit();
        $temporary = Temporary::first();
        return view('general-contact')->with(compact('temporary'));
    }

    public function thirdstep(Request $request){
        
        if($request->isMethod('post')){
               $this->validate($request, [
                    'email'          => 'required|email',
                ]);

            // print_r($request->all());exit();
            $data                   = $request->all();
            $id                     = $request->id;
            $temporary              = Temporary::where('id',$id)->first();
            $temporary->firstname   = $data['firstname'];
            $temporary->lastname    = $data['lastname'];
            $temporary->phone       = $data['phone'];
            $temporary->email       = $data['email'];

            $temporary->save();

            return redirect('/general/store');
        }
        // print_r($type);exit();
        return view('general-contact');

    }



    public function store(){
        $temporary = Temporary::first();
        print_r($temporary);exit();
        return view('general-location');
    }

    /*-----Modified on 09-02-2021-----*/
    public function mainServices($id)
    {
        $main_service=Service::where('id',$id)->first();
        $service_name=$main_service->service_name;
        $child_services=Service::where('parent_id',$id)->get();

        return view('general',compact('child_services','service_name'));
    } 
    public function mainSubServices($id)
    {

        $parentRw=Service::where('id',$id)->first();
        $service_id=$parentRw->parent_id;
        $service_name=$parentRw->service_name;
        //findout Main category name
        $mainParentRw=Service::where('id',$service_id)->first();
        $main_service_parent_name=$mainParentRw->service_name;
        //ends
        $parent_id=$id;
        $main_sub_services=Service::where('parent_id',$id)->get();
        $service_parent_id=$service_id;
        return view('main-sub-service',compact('main_sub_services','parent_id','service_id','service_parent_id','service_name','main_service_parent_name'));
    }
    
    public function mainSubServicesFlag($id)
    {
        $service_id=$id;
        $service=Service::where('id',$id)->first();
        $parent_id=$service->parent_id;
       
        $service_parent=Service::where('id',$parent_id)->first();
       
        $service_parent_id=$service_parent->parent_id;
        $service_name=$service_parent->service_name;

        $main_sub_services=Service::where('parent_id',$parent_id)->get();
        //findout Main category name
        $mainParentRw=Service::where('id',$service_parent_id)->first();
        $main_service_parent_name=$mainParentRw->service_name;
        //ends

        return view('main-sub-service',compact('main_sub_services','parent_id','service_id','service_parent_id','service_name','main_service_parent_name'));
    }
    public function ContactForm($id)
    {
        $service_id=$id;
        $parent=Service::where('id',$id)->first();
        $parent_id=$parent->parent_id;
        $subcat_sname3=$parent->service_name;
         //subcat1
         $subcat1=Service::where('id',$parent_id)->first();
         $subcat_sname1=$subcat1->service_name;
         $subcat_sparent1=$subcat1->parent_id;
        //nextsubcat2
        $subcat2=Service::where('id',$subcat_sparent1)->first();
        $subcat_sname2=$subcat2->service_name;
         $subcat_sparent2=$subcat2->parent_id;
         //ends
        return view('contact-form',compact('service_id','parent_id','subcat_sname1','subcat_sname2','subcat_sname3','subcat_sparent1'));
    }
    
    public function contactFromSubmit(Request $request)
    {
       
        $service_id     =   $request->get('service_id');
        $user_contacts = UserContact::create([
            'service_id' => $request->get('service_id'),
            'full_name' => $request->get('full_name'),
            'email' => $request->get('email'),
            'phone_no' => $request->get('phone_no'),
            'pincode' => $request->get('pin_code'),
            'city' => $request->get('city'),
            'district' => $request->get('district'),
            'state' => $request->get('state'),
        ]);
        $user_id=$user_contacts->id;
         $url="/user-contact-form/$user_id";
       
            // return response()->json(['url'=>$url,'flash_message_success'=>'Successfully added']);
         return redirect('/enquiry-finish');
    }
    public function enquiryfinish()
    {
        
        return view('enquiry-finish');
    }
    public function userContactForm($id)
    {
        $user_contact_id=$id;
       
        return view('user_contact-form',compact('service_id','user_contact_id'));
    }

}
