<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Service;

class IndexController extends Controller
{
    public function index()
    {
        $main_services=Service::where('parent_id','0')->where('child_id','0')->get();
        return view('index',compact('main_services'));

    }
    public function index2(){
        return view('index2');
    } 
    public function aboutus(){
        return view('aboutus');
    } 
    public function service(){
        return view('service');
    } 
    public function profile(){
        return view('handyman');
    } 
    public function protect(){
        return view('protect');
    } 
    public function under(){
        return view('under');
    } 
    public function faq(){
        return view('faq');
    } 
    public function nopage(){
        return view('404');
    } 
    public function contactus(){
        return view('contactus');
    } 
    public function blog(){
        return view('blog');
    } 
    public function pricing(){
        return view('pricing');
    } 
    public function project1(){
        return view('project-1');
    }
    public function shop(){
        return view('shop');
    }
    public function expertSignup(){
        return view('expert-signup');
    }
    public function paint(){
        return view('paint');
    }
    public function home(){
        return view('paint-home');
    }
    public function type(){
        return view('paintingtype');
    }
    public function duration(){
        return view('paintingduration');
    }
    public function enquiry(){
        return view('enquiry');
    }

    public function location(){
        return view('paint-location');
    }
    public function contact(){
        return view('paint-contact');
    }


}
