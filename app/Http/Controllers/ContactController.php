<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Mail;

class ContactController extends Controller
{
    public function sendmail(Request $request){
		$this->validate($request, [
				'name' 		=> 'required',
				'email' 	=> 'required|email',
				'phone' 	=> 'required',
				'message'	=> 'required',
				// 'captcha' => 'required|captcha',
				]);
        if($request->isMethod('post'))
        {
    


                    $data                   =   $request->all();
                    $to_mail                =   'jagadeesh.inovace@gmail.com';
                   
                    $message                =   '';
                    $content = '<html><body>';
                    $content .= '<table  style="border-color: #666;" cellpadding="10" border="1">';
                
                
                        $content .= "<tr><td><strong> Name:</strong> </td><td>" .  strip_tags($data['name']). "</td></tr>";
                        $content .= "<tr><td><strong> Email :</strong> </td><td>" .  ($data['email']). "</td></tr>";
                        $content .= "<tr><td><strong> Phone:</strong> </td><td>" .  strip_tags($data['phone']) . "</td></tr>";
                        $content .= "<tr><td><strong> Subject:</strong> </td><td>" .  strip_tags($data['subject']) . "</td></tr>";
                        $content .= "<tr><td><strong> Message:</strong> </td><td>" .  strip_tags($data['message']) . "</td></tr>";
                                
                    $content .= "</table>";
                    $content .= "</body></html>";
            




                    Mail::send('myTestMail', ['data' => $content], function($message) use ($to_mail) 
                    {
                    $message->from('info@buildadvisor.com', 'BUILD ADVISOR - CONTACT INFORMATION');
                    $message->to($to_mail)->subject('Contact Form BUILD ADVISOR');
                    });                  

                    $message="Mail sent successfully";
                    return redirect()->back()->with('success', 'Thanks for contacting us!');
        }
    }
}
