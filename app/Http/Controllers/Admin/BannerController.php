<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use Illuminate\Support\Facades\File;

class BannerController extends Controller
{
    public function index()
    { 
    $banners            = Banner::orderBy('id', 'DESC')->get();
    // print_r($banners);exit();
    return view('admin.banner.index')->with(compact('banners'));
    }
    public function create()
    {
    return view('admin.banner.create');
    }
    public function store(Request $request)
               {
               if($request->isMethod('post')){

                $data                   = $request->all();          
                $banner                 = new banner;
                $banner->banner_title   = $data['banner_title'];
              
                if ($request->hasFile('banner_image')) {
                $image1             = $request->file('banner_image');
                $imagename1         = time().'.'.$image1->getClientOriginalExtension();
                $destination1       = public_path('/uploads/banner');
                $image1->move($destination1, $imagename1);
                $banner->banner_image   = $imagename1;
                
                }
                
                $banner->save();
                    //log
                    // $Logs = new LogController();
                    // $Logs->store('banner creation','banner creation '); 

                    //endlog
                return redirect('/admin/banners')->with('flash_message_success','Banner added successfully');
                }

                return view('admin.banner.create');  
                }

    public function delete($id=null)
    			{
                if(!empty($id))
                {
                Banner::where(['id'=>$id])->delete();
                 //log
                 // $Logs = new LogController();
                 // $Logs->store('banner deletion','banner deletion '); 
                 //endlog
                return redirect()->back()->with('flash_message_success','Banner deleted successfully');
                }
    			}
                
}
