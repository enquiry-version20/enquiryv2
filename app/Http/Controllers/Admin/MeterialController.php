<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Meterial;
use Illuminate\Support\Facades\File;

class MeterialController extends Controller
{

    public function index()
    {
        $material         =  Meterial::orderBy('id','Desc')->get(); 
        return view('admin.material.index',compact('material'));
        
    }	
    public function create()
    {
        
        return view('admin.material.create')->with('msg','');
    }

   public function store(Request $request)
   {
               
                $material_name    = Meterial::where('material_name', 
                                            $request->get('material_name'))->first();
                //Meterial image
                if(!empty($request->hasFile('meterial_image'))){
                     
                    $uplod_folder = public_path('uploads');
                    //check if uplod folder is exists or not
                    if(!File::exists($uplod_folder)) {

                    File::makeDirectory($uplod_folder, $mode = 0755, true, true);
                    }
                    $path=public_path('uploads/materialimage');
                    $image = time() . '.' . $request->meterial_image->getClientOriginalExtension();
                     $request->meterial_image->move($path, $image);
                    

                }
                else
                {
                    $image =null;
                }
                $mainimagepath='uploads/materialimage/'.$image;


                //machine thumb
                if(!empty($request->hasFile('thumpnail'))){
                    
                    $uplod_folder = public_path('uploads');
                
                    //check if uplod folder is exists or not

                    if(!File::exists($uplod_folder)) {

                    File::makeDirectory($uplod_folder, $mode = 0755, true, true);
                    }
                    $path=public_path('uploads/thumpnail');
                    $image = time() . '.' . $request->thumpnail->getClientOriginalExtension();
                    $request->thumpnail->move($path, $image);
                   

                }
                else
                {
                    $image =null;
                }
                $thumbimagepath='uploads/thumpnail/'.$image;

                $material = Meterial::create([
            
                    'material_name'          => $request->get('material_name'),
                    'material_category'      => $request->get('material_category'),
                    'material_type'          => $request->get('material_type'),
                    'prize'          		 => $request->get('prize'),
                    'date'                   => $request->get('date'),
                    'details'                => $request->get('details'),
                    'description'            => $request->get('description'),
                    'date'      			 => $request->get('date'),
                    'stock'      			 => $request->get('stock'),
                    'meterial_image' 		 => $mainimagepath,
                    'thumpnail'				 => $thumbimagepath,
            
                ]);

                    //endlog
                return redirect('/admin/material')->with('flash_message_success','Material added successfully');
   }

   public function view($id)
   {
        $material=Meterial::where('id',$id)->first();
        // print_r($material);exit();
        return view('admin.material.view')->with(compact('material'));
   }

   public function edit($id)
    {
    	// print_r($request->all());exit();
		$material  = Meterial::where('id',$id)->first();
        return view('admin.material.edit')->with(compact('material'));
    }
   public function update(Request $request,$id)
   {

        $material=Meterial::where('id',$id)->first();
        // print_r($material);exit();
        //machine image
        if(!empty($request->hasFile('meterial_image'))){
                    
            $uplod_folder = public_path('uploads');
        
            //check if uplod folder is exists or not

            if(!File::exists($uplod_folder)) {

                File::makeDirectory($uplod_folder, $mode = 0755, true, true);
            }
            $path=public_path('uploads/materialimage');
            $image = date('YmdHis') . '.' . $request->meterial_image->getClientOriginalExtension();
            $request->meterial_image->move($path, $image);
            $mainimagepath='uploads/materialimage/'.$image;
        }
        else
        {
          $mainimagepath =$material->meterial_image;
        } 
          //Thumbnail image
          if(!empty($request->hasFile('thumpnail'))){
                    
            $uplod_folder = public_path('uploads');
        
            //check if uplod folder is exists or not

            if(!File::exists($uplod_folder)) {

            File::makeDirectory($uplod_folder, $mode = 0755, true, true);
            }
            $path=public_path('uploads/thumpnail');
            $thumbimage = date('YmdHis') . '.' . $request->thumpnail->getClientOriginalExtension();
            $request->thumpnail->move($path, $thumbimage);
            $thumbimagepath='uploads/thumpnail/'.$image;
        }
        else
        {
          $thumbimagepath =$material->thumpnail;
        } 

        $material->material_name     	=   $request->get('material_name');
        $material->material_category    = 	$request->get('material_category');
        $material->material_type        =   $request->get('material_type');
        $material->details     			=   $request->get('details');
        $material->prize     			=   $request->get('prize');
        $material->description         	=   $request->get('description');
        $material->stock         		=   $request->get('stock');
        $material->date         		=   $request->get('date');
        $material->date 				= 	$mainimagepath;
        $material->date 				= 	$thumbimagepath;
        $material->save();


        return redirect('/admin/material/')->with('flash_message_success','Material updated successfully');
   }
}
