<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function index()
    {
       return view('admin.login.index');
    }

    public function authenticate(Request $request) 
    {
        
        $user = User::where('email', $request->get('email'))
                ->where('is_admin_user','1')
                ->first();

        if(!$user) {

            Session::flash('flash_message_success', 'Invalid Username or Password'); 
            Session::flash('alert-class', 'alert-danger');
             return redirect()->back();
        }

        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password'),'is_admin_user'=>1])) {

            session(['name' => 'admin','userid'=>$user->id]);
            // Authentication passed...
            return redirect()->intended('admin/home');
         }

         //return redirect()->back();
         Session::flash('flash_message_success', 'Invalid Username or Password'); 
            Session::flash('alert-class', 'alert-danger');
         return redirect()->back();

    }
    public function logout() {
        Auth::logout();
        return redirect('/');
    
    }
}
