<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Service;
use Illuminate\Support\Facades\File;

class ServiceController extends Controller
{
    public function index()
    {
        $service         =  Service::get(); 
        return view('admin.service.index',compact('service'));       
    }	

    public function create()
    {        
        return view('admin.service.create')->with('msg','');
    }
   public function store(Request $request)
   {
               
                $service_name    = Service::where('service_name', 
                                            $request->get('service_name'))->first();
                //Meterial image
                if(!empty($request->hasFile('service_image'))){
                     
                    $uplod_folder = public_path('uploads');
                    //check if uplod folder is exists or not
                    if(!File::exists($uplod_folder)) {

                    File::makeDirectory($uplod_folder, $mode = 0755, true, true);
                    }
                    $path=public_path('uploads/serviceimage');
                    $image = time() . '.' . $request->service_image->getClientOriginalExtension();
                    $request->service_image->move($path, $image);
                    

                }
                else
                {
                    $image =null;
                }
                $serviceimage='uploads/serviceimage/'.$image;


                $service = Service::create([
            		'parent_id'				=> '0',
                    'child_id'				=> '0',
                    'search_id'				=> '1',
                    'service_name'          => $request->get('service_name'),
                    'description'      		=> $request->get('description'),
                    'service_image' 		=> $serviceimage,
            
                ]);

                    //endlog
                return redirect('/admin/service')->with('flash_message_success','Service added successfully');
   }

   public function category($id)
    {
    	// print_r($id);exit();
        $id  = $id;
        $service    = Service::where('id',$id)->first();
        $service_name=$service->service_name;
        return view('admin.service.category')->with(compact('id','service_name','service'));
    }

   public function updateCategory(Request $request,$id)
   {
              
                   $parent_id 		= $id;
                   $defaultvar='';
                   $previousService1=Service::where('id',$id)->first();
                   if($previousService1->parent_id!=0){
                        $parent=$previousService1->parent_id;
                        $previousService2=Service::where('id',$parent)->first();
                        $defaultvar=$previousService2->service_name;

                   }
                   if($defaultvar!=''){
                    $url=$defaultvar.' -> '.$previousService1->service_name.' -> '.$request->get('service_name');
                   }else{
                    $url=$previousService1->service_name.' -> '.$request->get('service_name');
                   }
                //service image
                if(!empty($request->hasFile('service_image'))){
                     
                    $uplod_folder = public_path('uploads');
                    //check if uplod folder is exists or not
                    if(!File::exists($uplod_folder)) {

                    File::makeDirectory($uplod_folder, $mode = 0755, true, true);
                    }
                    $path=public_path('uploads/serviceimage');
                    $image = time() . '.' . $request->service_image->getClientOriginalExtension();
                    $request->service_image->move($path, $image);
                    

                }
                else
                {
                    $image =null;
                }
                $serviceimage='uploads/serviceimage/'.$image;

                    if($request->get('search_id')=="yes"){
                         $search_id=1;
                    }else{
                        $search_id=0;
                    }
                $service = Service::create([
            		'parent_id'				=> $parent_id,
                    'child_id'				=> '0',
                    'search_id'				=> $search_id,
                    'service_name'          => $request->get('service_name'),
                    // 'service_category'      => $request->get('service_category'),
                    'description'      		=> $request->get('description'),
                    'details'      		=> $url,
                    'service_image' 		=> $serviceimage,
            
                ]);

                return redirect('/admin/service')->with('flash_message_success','Service added successfully');
   }

   public function view($id)
   {
        $service=Service::where('id',$id)->first();
        // print_r($material);exit();
        $parent_id = $service->parent_id;
        $parent_service = Service::where('id',$parent_id)->first();
        return view('admin.service.view')->with(compact('service','parent_service'));
   }

   public function edit($id)
    {
    	// print_r($request->all());exit();
		$service  = Service::where('id',$id)->first();
        return view('admin.service.edit')->with(compact('service'));
    }


    public function delete($id=null)
    {
             if(!empty($id))
             {
             Service::where(['id'=>$id])->orWhere('parent_id', $id)->delete();
             return redirect()->back()->with('flash_message_success','Service deleted successfully');
             }
    }

   public function update(Request $request,$id)
   {

        $service=Service::where('id',$id)->first();

        if($service->parent_id!=0){
            $parentService= Service::where('id',$service->parent_id)->first();
            $service_name2=$parentService->service_name;
            if($parentService->parent_id!=0){
                $parentService1= Service::where('id',$parentService->parent_id)->first();
                $service_name1=$parentService1->service_name;
                $url=$service_name1.' -> '. $service_name2.' -> '.$service->service_name;
            }else{
                $url= $service_name2.' -> '.$service->service_name;
            }
           // $url=$parentService->service_name.'/'.$service->service_name;

        }else{

            $url='';
        }
        // print_r($material);exit();
        //machine image
        if(!empty($request->hasFile('service_image'))){
                    
            $uplod_folder = public_path('uploads');
        
            //check if uplod folder is exists or not

            if(!File::exists($uplod_folder)) {

                File::makeDirectory($uplod_folder, $mode = 0755, true, true);
            }
            $path=public_path('uploads/serviceimage');
            $image = date('YmdHis') . '.' . $request->service_image->getClientOriginalExtension();
            $request->service_image->move($path, $image);
            $serviceimage='uploads/serviceimage/'.$image;
        }
        else
        {
          $serviceimage =$service->service_image;
        } 
 

        $service->service_name     	=   $request->get('service_name');
     //    if(!empty($request->get('service_category'))){
     //    $service->service_category  = 	$request->get('service_category');
        // }
       if($request->get('search_id')){
                if($request->get('search_id')=="yes"){
                    $search_id=1;
            }else{
                $search_id=0;
            }
       } 
       $service->search_id 	= 	$search_id;
        $service->description       =   $request->get('description');
        $service->details       =   $url;
        $service->service_image 	= 	$serviceimage;
        $service->save();


        return redirect('/admin/service/')->with('flash_message_success','Service updated successfully');
   }
}
