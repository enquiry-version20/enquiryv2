<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserContact;
use DB;
use App\Models\Service;

class ContactController extends Controller
{
    public function index()
    {
        $enquiry         =  UserContact::orderBy('id', 'DESC')->get(); 
        return view('admin.enquiry.index',compact('enquiry'));       
    }

   public function view($id)
   {

        $service_one=DB::table('user_contacts')
        ->leftJoin('services', 'services.id', '=', 'user_contacts.service_id')
        ->select('user_contacts.*','services.service_name','services.parent_id')
        ->where('user_contacts.id',$id)
        ->first();
        

        $service_id 		= $service_one->service_id;

        $service_two 	= Service::where('id',$service_id)->first();
      
        $parent_id1 	= $service_two->parent_id;
        $service_three  = Service::where('id',$parent_id1)->first();
        $parent_id2 	= $service_three->parent_id;
        $service_four  = Service::where('id',$parent_id2)->first();
        
        // print_r($service_three);exit();

        return view('admin.enquiry.view')->with(compact('service_one','service_two','service_three','service_four'));
   }

    public function delete($id=null)
                {
                if(!empty($id))
                {
                UserContact::where(['id'=>$id])->delete();

                return redirect()->back()->with('flash_message_success','Enquiry deleted successfully');
                }
                }
}
