<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RoofController extends Controller
{
    public function roof(){
        return view('roof');
    } 
    public function type(){
        return view('roof-type');
    } 
    public function duration(){
        return view('roof-duration');
    } 
    public function location(){
        return view('roof-location');
    } 
 
    public function contact(){
        return view('roof-contact');
    }     
}
