<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expert extends Model
{
    protected $table = 'experts';
	   protected $fillable = [
	    'firstname','lastname','phone','email','location','expertise'
	             
	    ];
}
