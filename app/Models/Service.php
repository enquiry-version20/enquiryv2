<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';
	   protected $fillable = [
	    'parent_id','child_id','search_id','service_name','service_category','details','prize','description','service_image','thumpnail','date'
	             
	    ];
}
