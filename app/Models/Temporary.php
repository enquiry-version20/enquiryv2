<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Temporary extends Model
{
    protected $table = 'Temporary';
	   protected $fillable = [
	    'service','type','duration','pin','city','district','state','firstname','lastname','phone','email'
	             
	    ];
}
