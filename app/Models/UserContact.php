<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserContact extends Model
{
    use HasFactory;
    protected $table = 'user_contacts';
    

    protected $date = [
        'date' => 'date'
    ];

    protected $fillable = [
        'service_id',
        'full_name',
        'email',
        'phone_no',
        'state',
        'district',
        'city',
        'pincode',
        'address',
        'date',
    ];

}
